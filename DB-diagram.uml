@startuml

map Ingredient {
    name => string
    id => string
    calories => number
    fat => number
    protein => number
    price => number
    carbs => number
    photo => img URL
    vegan => bool
    badges => arr[bool]
    description => string
}

map Restaurant {
    name => string
    id => string
    culture => string (many to 1)
    description => string
    reviews => html
    cost => number
    rating => number
    mapInfo => longitude, latitude
    phoneNumber => string
    address => string
    city => string
    deliverable => bool
    photo => img
    similarRecipes => arr[string] (1 to many)
    website => url
}

map Culture {
    name => string
    id => string
    restaurantList => arr[string] (1 to many)
    description => string
    recipeList => arr[string] (1 to many)
    flag => img URL
    mapData => longitude, latitude
    language => string
    currency => string
    sub-region => string
    wiki => url
}

map Recipe {
    name => string
    id => string
    culture => string (Many to 1)
    ingredientList => arr[string] (1 to many)
    description => string
    photo => img URL
    cookTime => number (minutes)
    instructions => arr[string]
    dishType => string
    source => url
}

Recipe::culture --> Culture::id
Recipe::ingredientList --> Ingredient::id

Restaurant::culture --> Culture::id
Restaurant::similarRecipes --> Recipe::id

Culture::restaurantList --> Restaurant::id
Culture::recipeList --> Recipe::id

@enduml