SHELL := /bin/bash

# docker for front end: runs website on local 3001 port
frontend-docker:
	docker run --rm -i -t -p 3001:3000 -v $(PWD):/usr/frontend-docker -w /usr/frontend-docker zaunitekoopa/selenium-chrome

# Running the website on your local machine
frontend-localbuild:
	cd Frontend && npm run start

# Running frontend unit tests seen in jest
frontend-unit-tests:
	cd Frontend && npm run test

# Running frontend acceptence tests (Selenium)
frontend-selenium-tests:
	cd Frontend && python3 guitests.py

# Command to automatically lint for the frontend
frontend-lint:
	cd Frontend && npm run lint:fix

# Command to automatically install the node modules for the react application
frontend-node-modules:
	cd Frontend && npm install

# Docker for running the backend as a developer on local machine
backend-dev-docker:
	docker run --rm -i -t -p 5000:5000 -v $(PWD):/usr/backend-docker -w /usr/backend-docker zaunitekoopa/fff-backend

# Docker for running the production enviornment for backend
backend-prod-docker:
	cd Backend && docker run -it -v `pwd`:/usr/backend -w /usr/backend -p 80:80 zaunitekoopa/flask-docker

# Command to automatically format the backend
backend-lint:
	black Backend/

# Command to deploy backend to prod
backend-deploy-api-prod:
	cd Backend && eb deploy feast-for-friends-api-prod

