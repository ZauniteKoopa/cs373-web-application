#!/usr/bin/env python3

from sqlalchemy import func, or_
from Models import Recipe


recipe_defaults = {
    "dishType": "",
    "culture": "",
    "time": 0,
    "rating": 0,
    "sort_attribute": "name",
    "sort_desc": 0,
    "page_num": 1,
    "per_page": 10,
    "search_term": "",
}


def get_recipe_request_params(request_args):
    attributes = {}
    for arg in recipe_defaults:
        attributes[arg] = request_args.get(
            arg,
            default=recipe_defaults[arg],
            type=type(recipe_defaults[arg]),
        )

    return attributes


def search_recipes(query, search_term):
    if search_term == "":
        return query

    search_terms = search_term.strip().lower().split()
    for term in search_terms:
        query = query.filter(
            or_(
                func.lower(Recipe.name).contains(term),
                func.lower(Recipe.cuisine).contains(term),
                func.lower(Recipe.dishType).contains(term),
                func.lower(str(Recipe.cookTime)).contains(term),
                func.lower(str(Recipe.rating)).contains(term),
            )
        )

    return query


# params should be a query object and a list of filters
def filter_recipes(query, attributes):
    queries = []

    if attributes["dishType"] != recipe_defaults["dishType"]:
        queries.append(Recipe.dishType == attributes["dishType"])

    if attributes["culture"] != recipe_defaults["culture"]:
        queries.append(Recipe.cuisine == attributes["culture"])

    if attributes["time"] != recipe_defaults["time"]:
        queries.append(Recipe.cookTime <= attributes["time"])

    if attributes["rating"] != recipe_defaults["rating"]:
        queries.append(Recipe.rating >= attributes["rating"])

    return query.filter(*queries)


def sort_recipes(query, attributes):
    sort_attr = attributes["sort_attribute"]
    if attributes["sort_desc"]:
        return query.order_by(Recipe.__getattribute__(Recipe, sort_attr).desc())

    return query.order_by(Recipe.__getattribute__(Recipe, sort_attr).asc())


def paginate_recipes(query, attributes):
    return query.paginate(
        page=attributes["page_num"],
        per_page=attributes["per_page"],
    )


def build_recipe_result(query, num_results):
    result = {
        "results_total": num_results,
        "data": [],
        "num_pages": query.pages,
    }

    for item in query.items:
        result["data"].append(
            {
                "id": item.id,
                "name": item.name,
                "category": item.dishType,
                "culture": item.cuisine,
                "time": item.cookTime,
                "rating": item.rating,
                "image": item.image,
                "description": item.description,
            }
        )

    return result
