#!/usr/bin/env python3

from sqlalchemy import func, or_
from Models import Ingredient


ingredient_defaults = {
    "price": 0,
    "calories": 0,
    "fat": 0,
    "protein": 0,
    "sort_attribute": "name",
    "sort_desc": 0,
    "page_num": 1,
    "per_page": 10,
    "search_term": "",
}


def get_ingredient_request_params(request_args):
    attributes = {}
    for arg in ingredient_defaults:
        attributes[arg] = request_args.get(
            arg,
            default=ingredient_defaults[arg],
            type=type(ingredient_defaults[arg]),
        )

    return attributes


def search_ingredients(query, search_term):
    if search_term == "":
        return query

    terms = search_term.strip().lower().split()
    for term in terms:
        query = query.filter(
            or_(
                func.lower(Ingredient.title).contains(term),
                func.lower(str(Ingredient.price)).contains(term),
                func.lower(str(Ingredient.calories)).contains(term),
                func.lower(str(Ingredient.fat)).contains(term),
                func.lower(str(Ingredient.protein)).contains(term),
            )
        )

    return query


# params should be a query object and a list of filters
def filter_ingredients(query, attributes):
    queries = []

    if attributes["price"] != ingredient_defaults["price"]:
        queries.append(Ingredient.price <= attributes["price"])

    if attributes["calories"] != ingredient_defaults["calories"]:
        queries.append(Ingredient.calories <= attributes["calories"])

    if attributes["fat"] != ingredient_defaults["fat"]:
        queries.append(Ingredient.fat <= attributes["fat"])

    if attributes["protein"] != ingredient_defaults["protein"]:
        queries.append(Ingredient.protein <= attributes["protein"])

    return query.filter(*queries)


def sort_ingredients(query, attributes):
    sort_attr = attributes["sort_attribute"]
    if attributes["sort_desc"]:
        return query.order_by(Ingredient.__getattribute__(Ingredient, sort_attr).desc())

    return query.order_by(Ingredient.__getattribute__(Ingredient, sort_attr).asc())


def paginate_ingredients(query, attributes):
    return query.paginate(
        page=attributes["page_num"],
        per_page=attributes["per_page"],
    )


def build_ingredient_result(query, num_results):
    result = {
        "results_total": num_results,
        "data": [],
        "num_pages": query.pages,
    }

    for item in query.items:
        result["data"].append(
            {
                "id": item.id,
                "name": item.name,
                "title": item.title,
                "price": item.price,
                "calories": item.calories,
                "fat": item.fat,
                "protein": item.protein,
                "image": item.image,
                "description": item.description,
                "num_recipe_connections": item.num_recipe_connections,
            }
        )

    return result
