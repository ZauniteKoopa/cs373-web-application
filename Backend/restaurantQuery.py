#!/usr/bin/env python3

from sqlalchemy import func, or_
from Models import Restaurant


restaurant_defaults = {
    "culture": "",
    "city": "",
    "cost": 0,
    "rating": 0,
    "sort_attribute": "name",
    "sort_desc": 0,
    "page_num": 1,
    "per_page": 10,
    "search_term": "",
}


def get_restaurant_request_params(request_args):
    attributes = {}
    for arg in restaurant_defaults:
        attributes[arg] = request_args.get(
            arg,
            default=restaurant_defaults[arg],
            type=type(restaurant_defaults[arg]),
        )

    return attributes


def search_restaurants(query, search_term):
    if search_term == "":
        return query

    terms = search_term.strip().lower().split()
    for term in terms:
        query = query.filter(
            or_(
                func.lower(Restaurant.name).contains(term),
                func.lower(Restaurant.culture).contains(term),
                func.lower(Restaurant.city).contains(term),
                func.lower(str(Restaurant.cost)).contains(term),
                func.lower(str(Restaurant.rating)).contains(term),
            )
        )

    return query


# params should be a query object and a list of filters
def filter_restaurants(query, attributes):
    queries = []

    if attributes["culture"] != restaurant_defaults["culture"]:
        queries.append(Restaurant.culture == attributes["culture"])

    if attributes["city"] != restaurant_defaults["city"]:
        queries.append(Restaurant.city == attributes["city"])

    if attributes["cost"] != restaurant_defaults["cost"]:
        queries.append(Restaurant.cost <= attributes["cost"])

    if attributes["rating"] != restaurant_defaults["rating"]:
        queries.append(Restaurant.rating >= attributes["rating"])

    return query.filter(*queries)


def sort_restaurants(query, attributes):
    sort_attr = attributes["sort_attribute"]
    if attributes["sort_desc"]:
        return query.order_by(Restaurant.__getattribute__(Restaurant, sort_attr).desc())

    return query.order_by(Restaurant.__getattribute__(Restaurant, sort_attr).asc())


def paginate_restaurants(query, attributes):
    return query.paginate(
        page=attributes["page_num"],
        per_page=attributes["per_page"],
    )


def build_restaurant_result(query, num_results):
    result = {
        "results_total": num_results,
        "data": [],
        "num_pages": query.pages,
    }

    for item in query.items:
        result["data"].append(
            {
                "id": item.id,
                "name": item.name,
                "culture": item.culture,
                "location": item.city,
                "cost": item.cost,
                "rating": item.rating,
                "image": item.image,
                "num_recipe_connections": item.num_recipe_connections,
                "description": item.description,
            }
        )

    return result
