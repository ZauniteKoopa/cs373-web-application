import json
import os
from threading import local
import requests

# UPDATE RESTAURANTS
param = {
    "per_page": 1000,
}

cuisine_types = json.loads(
    open(
        "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/cuisine-types.json"
    ).read()
)

culture_directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files"
culture_list = json.loads(
    json.dumps(
        requests.get(
            "http://api.feastforfriends.me/api/cultures", params=param
        ).json()
    )
)

culture_recipes_lists = {}
for culture in culture_list["data"]:
    culture_recipes_lists[culture["name"]] = json.loads(json.dumps(requests.get("http://api.feastforfriends.me/api/cultures/" + str(culture["id"])).json()))["recipe_connections"] 

directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/"
cities = ["austin", "dallas", "houston", "san-antonio"]

for city in cities:
    city_dir = directory + city
    for filename in os.listdir(city_dir):
        file = os.path.join(city_dir, filename)
        if os.path.isfile(file):
            f = open(file, "r+")

            local_data = json.load(f)
            local_data["recipe_connections"] = culture_recipes_lists[cuisine_types[local_data["culture"]]]
            local_data["num_recipe_connections"] = len(local_data["recipe_connections"])

            f.seek(0)
            f.write(json.dumps(local_data, indent = 4))
            f.truncate()
            f.close()