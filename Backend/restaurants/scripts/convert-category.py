#!/usr/bin/env python3

import os, json

culture_count = dict()
directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/"
cities = ["austin", "dallas", "houston", "san-antonio"]

for city in cities:
    # print("in " + city)
    city_dir = directory + city
    for filename in os.listdir(city_dir):
        file = os.path.join(city_dir, filename)
        if os.path.isfile(file):
            f = open(file, "r+")

            print("checking " + filename)
            restuarant_data = json.load(f)
            assert restuarant_data["description"] != ""

            # f.seek(0)
            # f.write(json.dumps(restuarant_data, indent = 4))
            # f.truncate()
            f.close()
