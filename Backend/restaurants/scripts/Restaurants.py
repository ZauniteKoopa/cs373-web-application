#!/usr/bin/python3

from yelpapi import YelpAPI
import os.path
import json

client_id = "CLvH_oejmW4isGEg-hCd2w"
api_key = "IerTD0husgLIP0C8r4s5F0i1qPDWgTOSA--RDg66doP8Awd8EHE1hYvcpVEHb8xb-Fy2UnNr3o9Hr_h9X81CcxSfhQZKPGe13f9MZBaMo3OFMacMycz8EAKTnYUZYnYx"
yelp_api = YelpAPI(api_key)

CITY_RESTAURANT_LIMIT = 2

cities = ["austin, tx", "houston, tx", "dallas, tx", "san antonio, tx"]
cuisines = json.loads(open("cuisine-types.json").read())

instance_counter = 0

for city in cities:
    print(city)
    file_path = (
        "restaurant-data-files/" + city.split(",")[0] + "/"
        if city != cities[3]
        else "restaurant-data-files/san-antonio/"
    )

    for cuisine in cuisines:
        print(cuisine)
        search_term = cuisine.lower() + " restaurant"

        try:
            search_response = yelp_api.search_query(
                term=search_term, location=city, categories=cuisine, limit=1
            )
        except YelpAPI.YelpAPIError as e:
            print(e)
            exit(1)

        restaurant_id = search_response["businesses"][0]["id"]
        restaurant_name = search_response["businesses"][0]["alias"]

        data_file_path = file_path + restaurant_name + ".json"
        data_file = ""

        if os.path.exists(data_file_path):
            print("already processed", restaurant_name)
            continue

        data_file = open(
            data_file_path,
            "w",
        )

        try:
            restaurant_details = yelp_api.business_query(id=restaurant_id)
        except YelpAPI.YelpAPIError as e:
            print(e)
            exit(1)

        try:
            restaurant_details["reviews"] = yelp_api.reviews_query(id=restaurant_id)[
                "reviews"
            ]
        except YelpAPI.YelpAPIError as e:
            print(e)
            exit(1)

        data_file.write(json.dumps(restaurant_details, indent=4))
        data_file.close()

        instance_counter += 1

print(instance_counter)
