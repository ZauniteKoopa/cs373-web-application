#!/usr/bin/env python3

from flask import Flask, jsonify, request
from flask.helpers import make_response
from flask_restful import Api
from flask_cors import CORS
from Models import db, Ingredient, Recipe, Culture, Restaurant

from recipeQuery import *
from ingredientQuery import *
from restaurantQuery import *
from cultureQuery import *

# Initialize flask app and sqlalchemy instance
app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://AnthonyHoang750:Zephyr3074!@feast-for-friends-db.cs3t2qwf688y.us-east-1.rds.amazonaws.com:5432/postgres"
CORS(app)
api = Api(app)
db.init_app(app)

# -- RECIPES -- #
@app.route("/api/recipes")
def get_recipe_list():
    query = Recipe.query

    # load query params from URL, set to default if not specified
    attributes = get_recipe_request_params(request.args)

    # SEARCHING
    query = search_recipes(query, attributes["search_term"])

    # FILTERING
    query = filter_recipes(query, attributes)

    # SORTING
    query = sort_recipes(query, attributes)

    # PAGINATION
    num_results = query.count()
    query = paginate_recipes(query, attributes)

    return jsonify(build_recipe_result(query, num_results))


# -- RECIPE BY ID -- #
@app.route("/api/recipes/<recipe_id>")
def get_recipe_by_id(recipe_id):
    recipe_with_id = Recipe.query.filter_by(id=recipe_id).first()

    if recipe_with_id:
        return jsonify(Recipe.into_dict(recipe_with_id))

    return make_response(
        jsonify({"ERROR": "recipe with ID " + str(recipe_id) + " not found"}), 404
    )


# -- LIST OF RECIPE FILTERS -- #
@app.route("/api/recipes/filters")
def get_recipe_filters():
    dish_type_filters = []
    cuisine_filters = []

    for recipe in Recipe.query:
        new_filter = recipe.dishType
        if new_filter not in dish_type_filters:
            dish_type_filters.append(new_filter)

        new_filter = recipe.cuisine
        if new_filter not in cuisine_filters:
            cuisine_filters.append(new_filter)

    return jsonify(
        {"dishType": sorted(dish_type_filters), "cuisine": sorted(cuisine_filters)}
    )


# -- INGREDIENTS -- #
@app.route("/api/ingredients")
def get_ingredient_list():
    query = Ingredient.query

    # load query params from URL, set to default if not specified
    attributes = get_ingredient_request_params(request.args)

    # SEARCHING
    query = search_ingredients(query, attributes["search_term"])

    # FILTERING
    query = filter_ingredients(query, attributes)

    # SORTING
    query = sort_ingredients(query, attributes)

    # PAGINATION
    num_results = query.count()
    query = paginate_ingredients(query, attributes)

    return jsonify(build_ingredient_result(query, num_results))


# -- INGREDIENT BY ID -- #
@app.route("/api/ingredients/<ingredient_id>")
def get_ingredient_by_id(ingredient_id):
    ingredient_with_id = Ingredient.query.filter_by(id=ingredient_id).first()

    if ingredient_with_id:
        return jsonify(Ingredient.into_dict(ingredient_with_id))

    return make_response(
        jsonify({"ERROR": "ingredient with ID " + str(ingredient_id) + " not found"}),
        404,
    )


# -- RESTAURANTS -- #
@app.route("/api/restaurants")
def get_restaurant_list():
    query = Restaurant.query

    # load query params from URL, set to default if not specified
    attributes = get_restaurant_request_params(request.args)

    # SEARCHING
    query = search_restaurants(query, attributes["search_term"])

    # FILTERING
    query = filter_restaurants(query, attributes)

    # SORTING
    query = sort_restaurants(query, attributes)

    # PAGINATION
    num_results = query.count()
    query = paginate_restaurants(query, attributes)

    return jsonify(build_restaurant_result(query, num_results))


# -- RESTAURANT BY ID -- #
@app.route("/api/restaurants/<restaurant_id>")
def get_restaurant_by_id(restaurant_id):
    restaurant_with_id = Restaurant.query.filter_by(id=restaurant_id).first()

    if restaurant_with_id:
        return jsonify(Restaurant.into_dict(restaurant_with_id))

    return make_response(
        jsonify({"ERROR": "restaurant with ID " + str(restaurant_id) + " not found"}),
        404,
    )


# -- LIST OF RESTAURANTS FILTERS -- #
@app.route("/api/restaurants/filters")
def get_restaurant_filters():
    culture_filters = []
    location_filters = []

    for restaurant in Restaurant.query:
        new_filter = restaurant.culture
        if new_filter not in culture_filters:
            culture_filters.append(new_filter)

        new_filter = restaurant.city
        if new_filter not in location_filters:
            location_filters.append(new_filter)

    return jsonify(
        {"culture": sorted(culture_filters), "location": sorted(location_filters)}
    )


# -- CULTURES -- #
@app.route("/api/cultures")
def get_culture_list():
    query = Culture.query

    # load query params from URL, set to default if not specified
    attributes = get_culture_request_params(request.args)

    # SEARCHING
    query = search_cultures(query, attributes["search_term"])

    # FILTERING
    query = filter_cultures(query, attributes)

    # SORTING
    query = sort_cultures(query, attributes)

    # PAGINATION
    num_results = query.count()
    query = paginate_cultures(query, attributes)

    return jsonify(build_culture_result(query, num_results))


# -- CULTURE BY ID -- #
@app.route("/api/cultures/<culture_id>")
def get_culture_by_id(culture_id):
    culture_with_id = Culture.query.filter_by(id=culture_id).first()

    if culture_with_id:
        return jsonify(Culture.into_dict(culture_with_id))

    return make_response(
        jsonify({"ERROR": "culture with ID " + str(culture_id) + " not found"}), 404
    )


# -- LIST OF CULTURE FILTERS -- #
@app.route("/api/cultures/filters")
def get_culture_filters():

    subregion_filters = []
    language_filters = []

    for culture in Culture.query:
        new_filter = culture.subRegion
        if new_filter not in subregion_filters:
            subregion_filters.append(new_filter)

        new_filter = culture.language
        if new_filter not in language_filters:
            language_filters.append(new_filter)

    return jsonify(
        {
            "subRegion": sorted(subregion_filters),
            "language": sorted(language_filters),
        }
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
