#!/usr/bin/env python3

import json
import requests
import os

def update_culture_connections():

    # UPDATE RECIPES
    param = {
        "per_page": 1000,
    }

    recipes_data = json.loads(
        json.dumps(
            requests.get("http://api.feastforfriends.me/api/recipes", params=param).json()
        )
    )

    directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files"
    for filename in os.listdir(directory):
        file = os.path.join(directory, filename)
        if os.path.isfile(file) and ".json" in file:

            culture_data_file = open(file, "r+")
            culture_data = json.load(culture_data_file)

            culture_data["recipe_connections"] = []
            culture_spoonacular_tag = culture_data["spoonacular_tag"]

            for recipe in recipes_data["data"]:
                if recipe["culture"] == culture_spoonacular_tag:
                    culture_data["recipe_connections"].append(recipe["id"])

            culture_data["num_recipe_connections"] = len(culture_data["recipe_connections"])

            culture_data_file.seek(0)
            culture_data_file.write(json.dumps(culture_data, indent=4))
            culture_data_file.truncate()
            culture_data_file.close()

    # UPDATE RESTAURANTS
    directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/"

    param = {
        "per_page": 1000,
    }
    restaurant_list = json.loads(
        json.dumps(
            requests.get(
                "http://api.feastforfriends.me/api/restaurants", params=param
            ).json()
        )
    )

    cuisine_types = json.loads(
        open(
            "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/cuisine-types.json"
        ).read()
    )

    culture_restaurant_counts = {culture: [] for culture in list(cuisine_types.values())}


    for restaurant_data in restaurant_list["data"]:
        restaurant_culture = restaurant_data["culture"]
        culture_restaurant_counts[cuisine_types[restaurant_culture]].append(
            restaurant_data["id"]
        )

    countries = json.loads(
        (
            open(
                "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/old-countries-list.json"
            ).read()
        )
    )["countries"]

    countries_with_restaurants = []

    for country_code in countries:

        if countries[country_code] in list(culture_restaurant_counts.keys()):
            countries_with_restaurants.append(country_code)

    for country in culture_restaurant_counts.keys():
        for country_code in countries_with_restaurants:
            if countries[country_code] == country:

                filename_base = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files/"
                filename = filename_base + country_code + "-data.json"
                country_data_file = open(
                    filename_base + country_code + "-data.json",
                    "r+",
                )
                country_data = json.load(country_data_file)

                country_data["restaurant_connections"] = culture_restaurant_counts[country]
                country_data["num_restaurant_connections"] = len(
                    culture_restaurant_counts[country]
                )

                country_data_file.seek(0)
                country_data_file.write(json.dumps(country_data, indent=4))
                country_data_file.truncate()
                country_data_file.close()
