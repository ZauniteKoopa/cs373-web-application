#!/usr/bin/env python3

from sqlalchemy import func, or_
from Models import Culture


culture_defaults = {
    "subregion": "",
    "language": "",
    "num_recipe_connections": 0,
    "num_restaurant_connections": 0,
    "sort_attribute": "name",
    "sort_desc": 0,
    "page_num": 1,
    "per_page": 10,
    "search_term": "",
}


def get_culture_request_params(request_args):
    attributes = {}
    for arg in culture_defaults:
        attributes[arg] = request_args.get(
            arg,
            default=culture_defaults[arg],
            type=type(culture_defaults[arg]),
        )

    return attributes


def search_cultures(query, search_term):
    if search_term == "":
        return query

    terms = search_term.strip().lower().split()
    for term in terms:
        query = query.filter(
            or_(
                func.lower(Culture.name).contains(term),
                func.lower(Culture.subRegion).contains(term),
                func.lower(Culture.language).contains(term),
                func.lower(str(Culture.num_restaurant_connections)).contains(term),
                func.lower(str(Culture.num_recipe_connections)).contains(term),
            )
        )

    return query


# params should be a query object and a list of filters
def filter_cultures(query, attributes):
    queries = []

    if attributes["subregion"] != culture_defaults["subregion"]:
        queries.append(Culture.subRegion == attributes["subregion"])

    if attributes["language"] != culture_defaults["language"]:
        queries.append(Culture.language == attributes["language"])

    if (
        attributes["num_recipe_connections"]
        != culture_defaults["num_recipe_connections"]
    ):
        queries.append(
            Culture.num_recipe_connections >= attributes["num_recipe_connections"]
        )

    if (
        attributes["num_restaurant_connections"]
        != culture_defaults["num_restaurant_connections"]
    ):
        queries.append(
            Culture.num_restaurant_connections
            >= attributes["num_restaurant_connections"]
        )

    return query.filter(*queries)


def sort_cultures(query, attributes):
    sort_attr = attributes["sort_attribute"]
    if attributes["sort_desc"]:
        return query.order_by(Culture.__getattribute__(Culture, sort_attr).desc())

    return query.order_by(Culture.__getattribute__(Culture, sort_attr).asc())


def paginate_cultures(query, attributes):
    return query.paginate(
        page=attributes["page_num"],
        per_page=attributes["per_page"],
    )


def build_culture_result(query, num_results):
    result = {
        "results_total": num_results,
        "data": [],
        "num_pages": query.pages,
    }

    for item in query.items:
        result["data"].append(
            {
                "id": item.id,
                "name": item.name,
                "subregion": item.subRegion,
                "language": item.language,
                "num_restaurant_connections": item.num_restaurant_connections,
                "num_recipe_connections": item.num_recipe_connections,
                "image": item.image,
                "description": item.description,
            }
        )

    return result
