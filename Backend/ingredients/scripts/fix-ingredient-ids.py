#!/usr/bin/env python3

import json
import os





directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/ingredients/ingredients-list/"
ingredient = dict()
id = 1
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    f = open(file, "r+")
    ingredient = json.load(f)

    

    ingredient["id"] = id

    # if "costs" in ingredient["description"]:
    #     print("cost still in " + filename)
    id = id + 1

    f.seek(0)
    f.write(json.dumps(ingredient, indent=4))
    f.truncate()
    f.close()
