#!/usr/bin/python

from re import I
from typing import Dict
import requests
import json
import os
import time


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.
ingredient_ids = []
ingredient_names = []

directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        f = open(file)
        recipe = json.load(f)

    ingredient_name = recipe["recipes"][0]["extendedIngredients"][2]["name"]
    ingredient_names.append(ingredient_name)

i = 0
while i < len(ingredient_names):

    url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/products/search"

    querystring = {"query": ingredient_names[i], "number": "1"}

    headers = {
        "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
        "x-rapidapi-key": "2482fe0b08msh07505b451e765e8p16f687jsn66ee9f28c624",
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    json_result = response.json()
    ingredient_name = json_result["products"]
    if len(ingredient_name) > 0:
        ingredient_ids.append(json_result["products"][0]["id"])
    else:
        ingredient_names.__delitem__(i)
        i = i - 1
    i = i + 1
    time.sleep(1)

for j in range(len(ingredient_ids)):

    url = (
        "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/products/"
        + str(ingredient_ids[j])
    )

    headers = {
        "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
        "x-rapidapi-key": "2482fe0b08msh07505b451e765e8p16f687jsn66ee9f28c624",
    }

    response = requests.request("GET", url, headers=headers).json()

    data_file = open(ingredient_names[j].replace(" ", "-") + "-data.json", "w")
    data_file.write(json.dumps(response))
    time.sleep(1)


# url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/products/search"

# querystring = {"query":"ginger", "number":"1"}

# headers = {
#     'x-rapidapi-host': "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
#     'x-rapidapi-key': "d5509c5927msha2bb4e40f916893p159c13jsnf38c8ae74e99"
#     }

# response = requests.request("GET", url, headers=headers, params=querystring)

# json_result = response.json()

# ingredient_id = json_result['products'][0]["id"]

# url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/food/products/" + str(ingredient_id)

# headers = {
#    'x-rapidapi-host': "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
#    'x-rapidapi-key': "d5509c5927msha2bb4e40f916893p159c13jsnf38c8ae74e99"
#     }

# response = requests.request("GET", url, headers=headers)

# print(response.text)
