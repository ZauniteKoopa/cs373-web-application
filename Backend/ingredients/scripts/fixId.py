#!/usr/bin/python

from re import I
from typing import Dict
import requests
import json
import os
import time


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.


directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/ingredients/ingredients-list"
recipe = dict()
for filename in os.listdir(directory):
    # print(filename)
    title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        f = open(file, "r+")
        recipe = json.load(f)

    recipe["title"] = title
    f.seek(0)
    f.write(json.dumps(recipe))
    f.truncate()
