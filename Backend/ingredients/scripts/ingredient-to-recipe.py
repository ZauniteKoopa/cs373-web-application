#!/usr/bin/python


from re import I
import requests
import json
import os
import time
from random import randrange


ingredientdirectory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/ingredients/ingredients-list/"
recipedirectory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list/"

def addIds(idArray, recipeIngredients, ingredient, recipeID):
    for i in recipeIngredients:
        thisRecipeIngredient = i["nameClean"]
        if thisRecipeIngredient is None:
            thisRecipeIngredient = i["name"]
        if ingredient in thisRecipeIngredient:
            idArray.append(recipeID)
            break


for filename in os.listdir(ingredientdirectory):
    # print(filename)
    # title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(ingredientdirectory, filename)
    print(filename)
    if os.path.isfile(file):
        f = open(file, "r+")
        thisIngredient = json.load(f)
        recipeids = []
        ingredientName = thisIngredient["name"]
        for filename2 in os.listdir(recipedirectory):
            file2 = os.path.join(recipedirectory, filename2)
            if os.path.isfile(file2):
                f2 = open(file2, "r+")
                thisRecipe = json.load(f2)
                if "recipes" in thisRecipe:
                    tempId = thisRecipe["recipes"][0]["id"]
                    tempIngredients = thisRecipe["recipes"][0]["extendedIngredients"]
                    addIds(recipeids, tempIngredients, ingredientName, tempId)
                else:
                    tempId = thisRecipe["id"]
                    tempIngredients = thisRecipe["extendedIngredients"]
                    addIds(recipeids, tempIngredients, ingredientName, tempId)
                f2.seek(0)
                f2.close()
        thisIngredient["recipeConnections"] = recipeids
        f.seek(0)
        f.write(json.dumps(thisIngredient, indent=4))
        f.truncate()
        f.close()
        




