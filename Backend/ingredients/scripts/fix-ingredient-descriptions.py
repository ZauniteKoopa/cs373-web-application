#!/usr/bin/env python3

import json
import os
import html
import re
from bs4 import BeautifulSoup


def clean_string(description):
    return (
        BeautifulSoup(html.unescape(description), features="html5lib")
        .get_text()
        .replace("\n", "")
        .strip()
        .encode("ascii", "ignore")
        .decode()
    )


directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/ingredients/ingredients-list/"
ingredient = dict()
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    f = open(file, "r+")
    ingredient = json.load(f)
    description = clean_string(ingredient["description"])

    print(filename)

    if ingredient["generatedText"] is not None:
        description = clean_string(ingredient["generatedText"])

    ingredient["description"] = description

    # if "costs" in ingredient["description"]:
    #     print("cost still in " + filename)
    print(ingredient["description"])
    print()

    f.seek(0)
    f.write(json.dumps(ingredient, indent=4))
    f.truncate()
    f.close()
