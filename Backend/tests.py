#!/usr/bin/env python3

import unittest
import json
import jsonschema
import app
from Models import Culture, Ingredient, Recipe, Restaurant

TEST_CLIENT = app.app.test_client()


def validate_json(data_instance, model_schema):
    try:
        jsonschema.validate(instance=data_instance, schema=model_schema)
    except:
        return False
    return True


#  ~~~  >>> RECIPES <<<  ~~~  #
RECIPE_LIST_ITEM_ATTRIBUTES = [
    "category",
    "culture",
    "description",
    "id",
    "image",
    "name",
    "rating",
    "time",
]
RECIPE_INSTANCE_ATTRIBUTES = [
    attribute for attribute in vars(Recipe) if "__" not in attribute
]
RECIPE_LIST_ITEM_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "category": {"type": "string"},
                    "culture": {"type": "string"},
                    "description": {"type": "string"},
                    "id": {"type": "number"},
                    "image": {"type": "string"},
                    "name": {"type": "string"},
                    "rating": {"type": "number"},
                    "time": {"type": "number"},
                },
            },
        },
        "num_pages": {"type": "number"},
        "results_total": {"type": "number"},
    },
}
RECIPE_FILTER_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "cuisine": {
            "type": "array",
            "items": {"type": "string"},
        },
        "dishType": {
            "type": "array",
            "items": {"type": "string"},
        },
    },
}


class TestRecipeEndpoints(unittest.TestCase):

    # - GET RECIPE LIST ALL - #
    def test_get_recipe_list(self):
        response_data = json.loads(TEST_CLIENT.get("/api/recipes").data)

        self.assertTrue(
            validate_json(response_data, RECIPE_LIST_ITEM_JSON_SCHEMA),
            "Recipe list item doesn't match recipe list schema.",
        )

        for recipe in response_data["data"]:
            for attribute in recipe.keys():
                self.assertTrue(
                    attribute in RECIPE_LIST_ITEM_ATTRIBUTES,
                    attribute
                    + " is not sent in the recipe list response for "
                    + recipe["name"]
                    + ".",
                )
                self.assertTrue(
                    recipe[attribute] is not None,
                    attribute + " has a null value in " + recipe["name"] + ".",
                )
                self.assertTrue(
                    recipe[attribute] != "",
                    attribute + " is empty in " + recipe["name"] + ".",
                )

    # - GET RECIPE LIST SEARCHING - #
    def test_recipe_search(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes",
                query_string={"search_term": "chicken", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for recipe in response_data["data"]:
            for value in recipe.values():
                if type(value) is str and "chicken" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET RECIPE LIST FILTERING - #
    def test_recipe_filter(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes", query_string={"culture": "Korean", "per_page": 1000}
            ).data
        )

        for recipe in response_data["data"]:
            self.assertTrue(
                recipe["culture"] == "Korean", "Culture does not match filter"
            )

    # - GET RECIPE LIST SORTING - #
    def test_recipe_sort_asc(self):

        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes",
                query_string={"sort_attribute": "dishType", "per_page": 1000},
            ).data
        )
        for recipe in range(0, len(response_data["data"]) - 1):
            recipe_data = response_data["data"][recipe]["category"]
            next_recipe = response_data["data"][recipe + 1]["category"]
            self.assertTrue(
                min(recipe_data, next_recipe) == recipe_data,
                "dish types aren't in order",
            )

    def test_recipe_sort_desc(self):

        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes",
                query_string={
                    "sort_attribute": "dishType",
                    "sort_desc": 1,
                    "per_page": 1000,
                },
            ).data
        )
        for recipe in range(0, len(response_data["data"]) - 1):
            recipe_data = response_data["data"][recipe]["category"]
            next_recipe = response_data["data"][recipe + 1]["category"]
            self.assertTrue(
                min(recipe_data, next_recipe) == next_recipe,
                "dish types aren't in order",
            )

    # - GET RECIPE BY ID - #
    def test_get_recipe_by_id(self):

        self.assertTrue(
            TEST_CLIENT.get("/api/recipes/5000").status_code == 404,
            "Response should be 404, there isn't a recipe with ID 5000.",
        )
        self.assertTrue(
            json.loads(TEST_CLIENT.get("/api/recipes/5000").data)
            == {"ERROR": "recipe with ID 5000 not found"},
            "Error response for recipe ID not found is wrong.",
        )

        for recipe_id in range(1, 101):
            recipe_data = json.loads(app.get_recipe_by_id(recipe_id).data)
            for attribute in recipe_data.keys():
                self.assertTrue(
                    attribute in RECIPE_INSTANCE_ATTRIBUTES,
                    attribute + " is not sent in the recipe instances.",
                )
                self.assertTrue(
                    recipe_data[attribute] is not None,
                    attribute + " has a null value in recipe instance.",
                )
                self.assertTrue(
                    recipe_data[attribute] != "",
                    attribute + " is empty in " + recipe_data["name"] + ".",
                )

    def test_recipe_search2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes",
                query_string={"search_term": "African", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for recipe in response_data["data"]:
            for value in recipe.values():
                if type(value) is str and "African" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET RECIPE LIST FILTERING - #
    def test_recipe_filter2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/recipes", query_string={"time": 90, "per_page": 1000}
            ).data
        )

        for recipe in response_data["data"]:

            self.assertTrue(recipe["time"] <= 90, "cook time does not match filter")

    # -- LIST OF RECIPE FILTERS -- #
    def test_get_recipe_filters(self):
        filters = json.loads(TEST_CLIENT.get("/api/recipes/filters").data)
        self.assertTrue(
            validate_json(filters, RECIPE_FILTER_JSON_SCHEMA),
            "Filter response doesn't match schema.",
        )
        self.assertTrue(
            len(filters["cuisine"]) == 24,
            str(24 - len(filters["cuisine"])) + " are missing.",
        )
        self.assertTrue(
            len(filters["dishType"]) == 9,
            str(9 - len(filters["dishType"])) + " are missing.",
        )


#  ~~~  >>> INGREDIENTS <<<  ~~~  #
INGREDIENT_LIST_ITEM_ATTRIBUTES = [
    "calories",
    "description",
    "fat",
    "id",
    "image",
    "name",
    "num_recipe_connections",
    "price",
    "protein",
    "title",
]
INGREDIENT_INSTANCE_ATTRIBUTES = [
    attribute for attribute in vars(Ingredient) if "__" not in attribute
]
INGREDIENT_LIST_ITEM_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "calories": {"type": "number"},
                    "description": {"type": "string"},
                    "fat": {"type": "number"},
                    "id": {"type": "number"},
                    "image": {"type": "string"},
                    "name": {"type": "string"},
                    "price": {"type": "number"},
                    "protein": {"type": "number"},
                    "title": {"type": "string"},
                },
            },
        },
        "num_pages": {"type": "number"},
        "results_total": {"type": "number"},
    },
}


class TestIngredientEndpoints(unittest.TestCase):

    # - GET INGREDIENT LIST - #
    def test_get_ingredient_list(self):
        response_data = json.loads(TEST_CLIENT.get("/api/ingredients").data)

        self.assertTrue(
            validate_json(response_data, INGREDIENT_LIST_ITEM_JSON_SCHEMA),
            "Ingredient list item doesn't match ingredient list schema.",
        )

        for ingredient in response_data["data"]:
            for attribute in ingredient.keys():
                self.assertTrue(
                    attribute in INGREDIENT_LIST_ITEM_ATTRIBUTES,
                    attribute
                    + " is not sent in the list response for "
                    + ingredient["name"]
                    + ".",
                )
                self.assertTrue(
                    ingredient[attribute] is not None,
                    attribute
                    + " has a null value in the list respone for "
                    + ingredient["name"]
                    + ".",
                )
                self.assertTrue(
                    ingredient[attribute] != "",
                    attribute
                    + " is empty in list respone for "
                    + ingredient["name"]
                    + ".",
                )

    def test_ingredient_search1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients",
                query_string={"search_term": "chicken", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for ingredient in response_data["data"]:
            for value in ingredient.values():
                if type(value) is str and "chicken" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET RECIPE LIST FILTERING - #
    def test_ingredient_filter1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients", query_string={"calories": 120, "per_page": 1000}
            ).data
        )

        for ingredient in response_data["data"]:
            self.assertTrue(
                ingredient["calories"] <= 120, "calories does not match filter"
            )

    # - GET RECIPE LIST SORTING - #
    def test_ingredient_sort_asc(self):

        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients",
                query_string={"sort_attribute": "price", "per_page": 1000},
            ).data
        )
        for ingredient in range(0, len(response_data["data"]) - 1):
            ingredient_data = response_data["data"][ingredient]["price"]
            next_ingredient = response_data["data"][ingredient + 1]["price"]
            self.assertTrue(
                min(ingredient_data, next_ingredient) == ingredient_data,
                "prices aren't in order",
            )

    def test_ingredient_sort_desc(self):

        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients",
                query_string={
                    "sort_attribute": "price",
                    "sort_desc": 1,
                    "per_page": 1000,
                },
            ).data
        )
        for ingredient in range(0, len(response_data["data"]) - 1):
            ingredient_data = response_data["data"][ingredient]["price"]
            next_ingredient = response_data["data"][ingredient + 1]["price"]
            self.assertTrue(
                min(ingredient_data, next_ingredient) == next_ingredient,
                "prices aren't in order",
            )

    # - GET INGREDIENT BY ID - #
    def test_get_ingredient_by_id(self):

        self.assertTrue(
            app.get_ingredient_by_id(5000).status_code == 404,
            "Response should be 404, there isn't a ingredient with ID 5000.",
        )
        self.assertTrue(
            json.loads(app.get_ingredient_by_id(5000).data)
            == {"ERROR": "ingredient with ID 5000 not found"},
            "Error response for ingredient ID not found is wrong.",
        )

        for ingredient_id in range(1, 115):
            ingredient_data = json.loads(app.get_ingredient_by_id(ingredient_id).data)
            for attribute in ingredient_data.keys():
                self.assertTrue(
                    attribute in INGREDIENT_INSTANCE_ATTRIBUTES,
                    attribute
                    + " is not sent in the ingredient instances for id "
                    + str(ingredient_id),
                )
                self.assertTrue(
                    ingredient_data[attribute] is not None,
                    attribute + " has a null value in ingredient instance.",
                )
                self.assertTrue(
                    ingredient_data[attribute] != "",
                    attribute + " is empty in " + ingredient_data["name"] + ".",
                )

    # - GET INGREDIENT LIST SEARCHING - #
    def test_ingredient_search2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients",
                query_string={"search_term": "cheese", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for ingredient in response_data["data"]:
            for value in ingredient.values():
                if type(value) is str and "cheese" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET INGREDIENT LIST FILTERING - #
    def test_ingredient_filter2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/ingredients", query_string={"price": 5, "per_page": 1000}
            ).data
        )

        for ingredient in response_data["data"]:
            self.assertTrue(ingredient["price"] <= 5, "Price does not match filter")


#  ~~~  >>> RESTAURANTS <<<  ~~~  #
RESTAURANT_LIST_ITEM_ATTRIBUTES = [
    "cost",
    "culture",
    "description",
    "id",
    "image",
    "location",
    "name",
    "num_recipe_connections",
    "rating",
]
RESTAURANT_INSTANCE_ATTRIBUTES = [
    attribute for attribute in vars(Restaurant) if "__" not in attribute
]
RESTAURANT_LIST_ITEM_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "cost": {"type": "number"},
                    "culture": {"type": "string"},
                    "description": {"type": "string"},
                    "id": {"type": "number"},
                    "image": {"type": "string"},
                    "location": {"type": "string"},
                    "name": {"type": "string"},
                    "rating": {"type": "number"},
                },
            },
        },
        "num_pages": {"type": "number"},
        "results_total": {"type": "number"},
    },
}
RESTAURANT_FILTER_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "culture": {
            "type": "array",
            "items": {"type": "string"},
        },
        "location": {
            "type": "array",
            "items": {"type": "string"},
        },
    },
}


class TestRestaurantEndpoints(unittest.TestCase):

    # - GET RESTAURANT LIST - #
    def test_get_restaurant_list(self):
        response_data = json.loads(TEST_CLIENT.get("/api/restaurants").data)

        self.assertTrue(
            validate_json(response_data, RESTAURANT_LIST_ITEM_JSON_SCHEMA),
            "restaurant list item doesn't match restaurant list schema.",
        )

        for restaurant in response_data["data"]:
            for attribute in restaurant.keys():
                self.assertTrue(
                    attribute in RESTAURANT_LIST_ITEM_ATTRIBUTES,
                    attribute
                    + " is not sent in the list response for "
                    + restaurant["name"]
                    + " response.",
                )
                self.assertTrue(
                    restaurant[attribute] is not None,
                    attribute
                    + " has a null value in the list response for "
                    + restaurant["name"]
                    + ".",
                )
                self.assertTrue(
                    restaurant[attribute] != "",
                    attribute
                    + " is empty in list response for "
                    + restaurant["name"]
                    + ".",
                )

    def test_restaurant_search1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants",
                query_string={"search_term": "steak", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for restaurant in response_data["data"]:
            for value in restaurant.values():
                if type(value) is str and "steak" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET restaurant LIST FILTERING - #
    def test_restaurant_filter1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants", query_string={"city": "Houston", "per_page": 1000}
            ).data
        )

        for restaurant in response_data["data"]:
            self.assertTrue(
                restaurant["location"] == "Houston", "Location does not match filter"
            )

    # - GET restaurant LIST SORTING - #
    def test_restaurant_sort_asc(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants",
                query_string={"sort_attribute": "cost", "per_page": 1000},
            ).data
        )
        for restaurant in range(0, len(response_data["data"]) - 1):
            restaurant_data = response_data["data"][restaurant]["cost"]
            next_restaurant = response_data["data"][restaurant + 1]["cost"]
            self.assertTrue(
                min(restaurant_data, next_restaurant) == restaurant_data,
                "costs aren't in order",
            )

    def test_restaurant_sort_desc(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants",
                query_string={
                    "sort_attribute": "cost",
                    "sort_desc": 1,
                    "per_page": 1000,
                },
            ).data
        )
        for restaurant in range(0, len(response_data["data"]) - 1):
            restaurant_data = response_data["data"][restaurant]["cost"]
            next_restaurant = response_data["data"][restaurant + 1]["cost"]
            self.assertTrue(
                min(restaurant_data, next_restaurant) == next_restaurant,
                "costs aren't in order",
            )

    # - GET RESTAURANT BY ID - #
    def test_get_restaurant_by_id(self):

        self.assertTrue(
            app.get_restaurant_by_id(5000).status_code == 404,
            "Response should be 404, there isn't a restaurant with ID 5000.",
        )
        self.assertTrue(
            json.loads(app.get_restaurant_by_id(5000).data)
            == {"ERROR": "restaurant with ID 5000 not found"},
            "Error response for restaurant ID not found is wrong.",
        )

        for restaurant_id in range(1, 138):
            restaurant_data = json.loads(app.get_restaurant_by_id(restaurant_id).data)
            for attribute in restaurant_data.keys():
                self.assertTrue(
                    attribute in RESTAURANT_INSTANCE_ATTRIBUTES,
                    attribute + " is not sent in the restaurant instances.",
                )
                self.assertTrue(
                    restaurant_data[attribute] is not None,
                    attribute + " has a null value in restaurant instance.",
                )
                self.assertTrue(
                    restaurant_data[attribute] != "",
                    attribute + " is empty in " + restaurant_data["name"] + ".",
                )

    # - GET RESTAURANT LIST SEARCHING - #
    def test_restaurant_search2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants",
                query_string={"search_term": "Asia", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for restaurant in response_data["data"]:
            for value in restaurant.values():
                if type(value) is str and "Asia" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET RESTAURANT LIST FILTERING - #
    def test_restaurant_filter2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/restaurants", query_string={"city": "Houston", "per_page": 1000}
            ).data
        )

        for restaurant in response_data["data"]:
            self.assertTrue(
                restaurant["location"] == "Houston", "City does not match filter"
            )

    # -- LIST OF RESTAURANT FILTERS -- #
    def test_get_restaurant_filters(self):
        filters = json.loads(TEST_CLIENT.get("api/restaurants/filters").data)
        self.assertTrue(
            validate_json(filters, RESTAURANT_FILTER_JSON_SCHEMA),
            "Filter response doesn't match schema.",
        )
        self.assertTrue(
            len(filters["culture"]) == 37,
            str(37 - len(filters["culture"])) + " are missing.",
        )
        self.assertTrue(
            len(filters["location"]) == 17,
            str(16 - len(filters["location"])) + " are missing.",
        )


#  ~~~  >>> CULTURES <<<  ~~~  #
CULTURE_LIST_ITEM_ATTRIBUTES = [
    "description",
    "id",
    "image",
    "language",
    "name",
    "num_restaurant_connections",
    "num_recipe_connections",
    "subregion",
]
CULTURE_INSTANCE_ATTRIBUTES = [
    attribute for attribute in vars(Culture) if "__" not in attribute
]
CULTURE_LIST_ITEM_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "description": {"type": "string"},
                    "id": {"type": "number"},
                    "image": {"type": "string"},
                    "language": {"type": "string"},
                    "name": {"type": "string"},
                    "num_restaurant_connections": {"type": "number"},
                    "num_recipe_connections": {"type": "number"},
                    "sub-region": {"type": "string"},
                },
            },
        },
        "num_pages": {"type": "number"},
        "results_total": {"type": "number"},
    },
}
CULTURE_FILTER_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "language": {
            "type": "array",
            "items": {"type": "string"},
        },
        "subRegion": {
            "type": "array",
            "items": {"type": "string"},
        },
    },
}


class TestCultureEndpoints(unittest.TestCase):

    # - GET CULTURE LIST - #
    def test_get_culture_list(self):
        response_data = json.loads(TEST_CLIENT.get("/api/cultures").data)

        self.assertTrue(
            validate_json(response_data, CULTURE_LIST_ITEM_JSON_SCHEMA),
            "culture list item doesn't match culture list schema.",
        )

        for culture in response_data["data"]:
            for attribute in culture.keys():
                self.assertTrue(
                    attribute in CULTURE_LIST_ITEM_ATTRIBUTES,
                    attribute
                    + " is not sent in the culture list for "
                    + culture["name"]
                    + ".",
                )
                self.assertTrue(
                    culture[attribute] is not None,
                    attribute + " has a null value in " + culture["name"] + ".",
                )
                self.assertTrue(
                    culture[attribute] != "",
                    attribute + " is empty in " + culture["name"] + ".",
                )

    def test_culture_search1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures", query_string={"search_term": "China", "per_page": 1000}
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for culture in response_data["data"]:
            for value in culture.values():
                if type(value) is str and "China" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET culture LIST FILTERING - #
    def test_culture_filter1(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures",
                query_string={"language": "Spanish language", "per_page": 1000},
            ).data
        )

        for culture in response_data["data"]:
            self.assertTrue(
                culture["language"] == "Spanish language",
                "languages does not match filter",
            )

    # - GET culture LIST SORTING - #
    def test_culture_sort_asc(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures",
                query_string={"sort_attribute": "subRegion", "per_page": 1000},
            ).data
        )
        for culture in range(0, len(response_data["data"]) - 1):
            culture_data = response_data["data"][culture]["subregion"]
            next_culture = response_data["data"][culture + 1]["subregion"]
            self.assertTrue(
                min(culture_data, next_culture) == culture_data,
                "sub-regions aren't in order",
            )

    def test_culture_sort_desc(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures",
                query_string={
                    "sort_attribute": "subRegion",
                    "sort_desc": 1,
                    "per_page": 1000,
                },
            ).data
        )
        for culture in range(0, len(response_data["data"]) - 1):
            culture_data = response_data["data"][culture]["subregion"]
            next_culture = response_data["data"][culture + 1]["subregion"]
            self.assertTrue(
                min(culture_data, next_culture) == next_culture,
                "sub-regions aren't in order",
            )

    # - GET CULTURE BY ID - #
    def test_get_culture_by_id(self):

        self.assertTrue(
            app.get_culture_by_id(5000).status_code == 404,
            "Response should be 404, there isn't a culture with ID 5000.",
        )
        self.assertTrue(
            json.loads(app.get_culture_by_id(5000).data)
            == {"ERROR": "culture with ID 5000 not found"},
            "Error response for culture ID not found is wrong.",
        )

        for culture_id in range(1, 38):
            culture_data = json.loads(app.get_culture_by_id(culture_id).data)
            for attribute in culture_data.keys():
                self.assertTrue(
                    attribute in CULTURE_INSTANCE_ATTRIBUTES,
                    attribute + " is not sent in the culture instances.",
                )
                self.assertTrue(
                    culture_data[attribute] is not None,
                    attribute + " has a null value in culture instance.",
                )
                self.assertTrue(
                    culture_data[attribute] != "",
                    attribute + " is empty in " + culture_data["name"] + ".",
                )

    # - GET CULTURE LIST SEARCHING - #
    def test_culture_search2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures",
                query_string={"search_term": "North America", "per_page": 1000},
            ).data
        )

        self.assertTrue(
            len(response_data["data"]) != 0, "Search term was not in response list."
        )

        search_term_in_response = False
        for culture in response_data["data"]:
            for value in culture.values():
                if type(value) is str and "North America" in value:
                    search_term_in_response = True

        self.assertTrue(
            search_term_in_response, "Search term was not in response list."
        )

    # - GET CULTURE LIST FILTERING - #
    def test_culture_filter2(self):
        response_data = json.loads(
            TEST_CLIENT.get(
                "/api/cultures", query_string={"subregion": "Europe", "per_page": 1000}
            ).data
        )

        for culture in response_data["data"]:
            self.assertTrue(
                culture["subregion"] == "Europe", "Sub-region does not match filter"
            )

    # -- LIST OF CULTURE FILTERS -- #
    def test_get_culture_filters(self):
        filters = json.loads(TEST_CLIENT.get("api/cultures/filters").data)
        self.assertTrue(
            validate_json(filters, CULTURE_FILTER_JSON_SCHEMA),
            "Filter response doesn't match schema.",
        )
        self.assertTrue(
            len(filters["language"]) == 23,
            str(22 - len(filters["language"])) + " are missing.",
        )
        self.assertTrue(
            len(filters["subRegion"]) == 5,
            str(5 - len(filters["subRegion"])) + " are missing.",
        )


if __name__ == "__main__":
    with app.app.app_context():
        unittest.main()
