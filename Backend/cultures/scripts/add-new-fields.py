import json
import os

culture = dict()
directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files"
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        culture = json.load(f)

        culture["produce"] = ["", "", "", "", "", ""]
        culture["latitude"] = 0
        culture["longitude"] = 0

        f.seek(0)
        f.write(json.dumps(culture, indent=4))
        f.truncate()
        f.close()
