#!/usr/bin/env python3

"""
1. try wikipedia food/cuisine seciton
2. go through ids from countries-list.json, api call for each
3. sort results by population, only take top 50 (maybe less)
4. combine cuisine section from wiki with other data
"""

import requests, json, time, wikipediaapi

base_url = "https://countries-cities.p.rapidapi.com/location/country/"

headers = {
    "x-rapidapi-host": "countries-cities.p.rapidapi.com",
    "x-rapidapi-key": "d5509c5927msha2bb4e40f916893p159c13jsnf38c8ae74e99",
}


def main():
    # - opens the list of country codes that we will use to scrape the info.
    with open("countries-list.json") as countries_list:
        json_result = json.load(countries_list)

    # - grabs the dictionary that actually contains the country codes.
    countries = json_result["countries"]

    # - This for loop will iterate over the codes, grab the resulting json details, and store it in a file for each code.
    for country in countries:
        country_data_response = requests.request(
            "GET", base_url + country, headers=headers
        ).json()

        data_file = open(country + "-data.json", "w")
        data_file.write(get_cuisine_wiki_info(country_data_response))

        # - this sleep will ensure that we do not go over rapid-api's limit for sending requests per second.
        time.sleep(2)


def get_cuisine_wiki_info(country_data_dict: dict) -> str:
    wiki_name = country_data_dict["name"]

    wikipedia = wikipediaapi.Wikipedia("en")
    country_wiki_page = wikipedia.page(wiki_name)
    country_data_dict["cuisine_info"] = filter_wiki_cuisine_section(
        country_wiki_page.text
    )

    return json.dumps(country_data_dict)


def filter_wiki_cuisine_section(wiki_page_text: str) -> str:
    cuisine_section = ""

    split_text = wiki_page_text.split("\n")
    for token in range(len(split_text)):
        if (
            split_text[token] == "Cuisine"
            or split_text[token] == "Food"
            or split_text[token] == "Food and drink"
        ):
            cuisine_section += split_text[token + 1]

    return cuisine_section


if __name__ == "__main__":
    main()
