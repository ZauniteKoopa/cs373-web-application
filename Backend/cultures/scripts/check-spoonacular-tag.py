#!/usr/bin/env python3

import json
from os import listdir
from os.path import isfile, join

json_data_files = []

for file in listdir("./"):
    if isfile(join("./", file)) and "-data.json" in file:
        json_data_files += (file,)

for json_data_file in json_data_files:
    country_data = json.load(open(json_data_file))
    print("checking " + json_data_file, "(" + country_data["name"] + ")")
    if country_data["spoonacular_tag"] == "":
        print("MISSING SPOONACULAR TAG SECTION FOR " + str(json_data_files))
