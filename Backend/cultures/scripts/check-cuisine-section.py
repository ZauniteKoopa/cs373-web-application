#!/usr/bin/env python3

import json
from os import listdir
from os.path import isfile, join

json_data_files = []

for file in listdir("./"):
    if isfile(join("./", file)) and "-data.json" in file:
        json_data_files += (file,)

counter = 0
for json_data_file in json_data_files:
    print(json_data_file)
    data_file = json.load(open(json_data_file))
    if data_file["cuisine_info"] == "":
        print("MISSING CUISINSE SECTION FOR " + data_file["name"])
    counter += 1

print(counter)
