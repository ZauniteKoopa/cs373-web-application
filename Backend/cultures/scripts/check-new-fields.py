import json
import os

culture = dict()
directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files"
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        culture = json.load(f)

        if culture["recipes"]:
            try:
                if len(culture[0]["cuisines"]) == 1:
                    culture["cuisines"] = culture["cuisines"][0]
            except (AttributeError, TypeError):
                continue
        else:
            try:
                if len(culture["cuisines"]) == 1:
                    culture["cuisines"] = culture["cuisines"][0]
            except (AttributeError, TypeError):
                continue

        f.seek(0)
        f.write(json.dumps(culture, indent=4))
        f.truncate()
        f.close()
