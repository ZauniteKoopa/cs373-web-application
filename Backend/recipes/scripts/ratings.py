#!/usr/bin/python

from re import I
from typing import Dict
import requests
import json
import os
import time
from random import randrange


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.


directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
for filename in os.listdir(directory):
    # print(filename)
    # title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        f = open(file, "r+")
        recipe = json.load(f)

    # handle files that don't have recipes as a key
    num = randrange(10)
    rating = 0
    if num < 7:
        rating = 4
    elif num >= 7 and num < 9:
        rating = 5
    else:
        rating = 3
    if "recipes" in recipe:

        recipe["recipes"][0]["rating"] = rating

    else:
        recipe["rating"] = rating

    f.seek(0)
    f.write(json.dumps(recipe))
    f.truncate()
