#!/usr/bin/python

from flask import Flask, jsonify
from flask_restful import Api
from flask_cors import CORS
from Models import db, Ingredient, Recipe, Culture, Restaurant
from re import I
from typing import Dict
import requests
import json
import os
import time
from random import randrange
from app import app

# from Backend.Models import Restaurant

# Initialize flask app and sqlalchemy instance
# app.debug = True  # TODO set to False when deploying to AWS
# app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# app.config[
#     "SQLALCHEMY_DATABASE_URI"
# ] = "postgresql+psycopg2://AnthonyHoang750:Zephyr3074!@feast-for-friends-db.cs3t2qwf688y.us-east-1.rds.amazonaws.com:5432/postgres"
# CORS(app)
api = Api(app)
db.init_app(app)


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.

directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"
thisCulture = dict()
with app.app_context():
    for filename in os.listdir(directory):
        # print(filename)
        # title = filename.replace("-data.json", "").replace("-", " ")
        file = os.path.join(directory, filename)
        if os.path.isfile(file):
            f = open(file, "r+")
            thisRecipe = json.load(f)
            ids = dict()

            if "recipes" in thisRecipe:
                ingredients = thisRecipe["recipes"][0]["extendedIngredients"]
            else:
                ingredients = thisRecipe["extendedIngredients"]

            for i in ingredients:
                print(i["name"])
                # thisIngredients = Ingredient.query.filter(Ingredient.name == i["name"]).first()
                thisIngredients = Ingredient.query.filter(
                    Ingredient.name.like(i["name"])
                ).all()
                print(thisIngredients)
                break
            break

        # result = dict()
        # for i in restaurants:
        #     result[str(i.id)] = i.name

        # f.seek(0)
        # f.write(json.dumps(result, indent = 4))
        # f.truncate()
        f.close()

if __name__ == "__main__":
    app.run(debug=True)
