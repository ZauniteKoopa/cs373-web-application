#!/usr/bin/env python3

import os
import json

def id_in_ingredient_list_twice(ingredient_id, ingredient_list):
    count = 0
    for ingredient in ingredient_list:
        if ingredient["id"] == ingredient_id:
            count += 1
    return count != 1

def check_no_dupes(ingredient_list, file_name):
    for ingredient in range(len(ingredient_list)):
        for other_ingredient in range(ingredient + 1, len(ingredient_list)):
            if ingredient_list[ingredient]["id"] == ingredient_list[other_ingredient]["id"]:
                print("duplicate in file " + file_name)


directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
for filename in os.listdir(directory):
    title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(directory, filename)

    f = open(file, "r+")
    recipe = json.load(f)

    # handle files that don't have recipes as a key
    try:
        recipe["recipes"]
        try:
            ingredients = recipe["recipes"][0]["extendedIngredients"]
            ingredients_no_dupes = []
            for ingredient in ingredients:
                if not id_in_ingredient_list_twice(ingredient["id"], ingredients):
                    ingredients_no_dupes.append(ingredient)
            check_no_dupes(ingredients_no_dupes, filename)
            recipe["recipes"][0]["extendedIngredients"] = ingredients_no_dupes
        except:
            continue
    except:
        try:
            ingredients = recipe["extendedIngredients"]
            ingredients_no_dupes = []
            for ingredient in ingredients:
                if not id_in_ingredient_list_twice(ingredient["id"], ingredients):
                    ingredients_no_dupes.append(ingredient)
            check_no_dupes(ingredients_no_dupes, filename)
            recipe["extendedIngredients"] = ingredients_no_dupes
        except:
            continue

    f.seek(0)
    f.write(json.dumps(recipe, indent=2))
    f.truncate()
    f.close()
