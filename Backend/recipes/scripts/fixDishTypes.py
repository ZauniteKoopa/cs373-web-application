#!/usr/bin/env python3

# TODO put all different found dishTypes into a dict and print

import json
import os

dish_types = set()
directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"

lessPreferred = ["lunch", "dinner", "breakfast"]
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        recipe = json.load(f)

        try:
            recipe["recipes"]
        except:
            # dish_types.add(dish_type for dish_type in recipe["dishTypes"])
            if recipe["dishTypes"][0] in lessPreferred and len(recipe["dishTypes"]) > 1:
                recipe["dishTypes"] = recipe["dishTypes"][1]
            else:
                recipe["dishTypes"] = recipe["dishTypes"][0]
        else:
            # dish_types.add(dish_type for dish_type in recipe["recipes"][0]["dishTypes"])
            if (
                recipe["recipes"][0]["dishTypes"][0] in lessPreferred
                and len(recipe["recipes"][0]["dishTypes"]) > 1
            ):
                recipe["recipes"][0]["dishTypes"] = recipe["recipes"][0]["dishTypes"][1]
            else:
                recipe["recipes"][0]["dishTypes"] = recipe["recipes"][0]["dishTypes"][0]

        f.seek(0)
        f.write(json.dumps(recipe))
        f.truncate()
