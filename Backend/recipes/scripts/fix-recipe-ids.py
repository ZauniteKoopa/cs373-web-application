#!/usr/bin/env python3

import json
import os





recipe = dict()
directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list/"
id = 1
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        recipe = json.load(f)
        
        if "recipes" in recipe:
            recipe["recipes"][0]["id"] = id
           
    
        else:
            recipe["id"] = id

    # if "costs" in ingredient["description"]:
    #     print("cost still in " + filename)
    id = id + 1

    f.seek(0)
    f.write(json.dumps(recipe, indent=4))
    f.truncate()
    f.close()
