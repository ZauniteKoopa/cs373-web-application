#!/usr/bin/env python3

# TODO put all different found dishTypes into a dict and print

import json
import os

dish_types = set()
directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        recipe = json.load(f)

        print("checking " + filename)
        try:
            recipe["recipes"]
        except:
            # dish_types.add(dish_type for dish_type in recipe["dishTypes"])
            for dish_type in recipe["dishTypes"]:
                dish_types.add(dish_type)
        else:
            # dish_types.add(dish_type for dish_type in recipe["recipes"][0]["dishTypes"])
            for dish_type in recipe["recipes"][0]["dishTypes"]:
                dish_types.add(dish_type)

        f.close()

print("\n-- found dish types --")
for dish_type in dish_types:
    print(dish_type)
