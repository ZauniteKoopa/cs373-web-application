#!/usr/bin/python

import requests
import json
import time

# - spoonacular allows us to randomly get a number of recipes.
url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/random"

# manually search greek, japanese, caribbean, jewish, mexican

cuisines = [
    "african",
    "american",
    "british",
    "cajun",
    "chinese",
    "eastern european",
    "european",
    "french",
    "german",
    "indian",
    "irish",
    "italian",
    "korean",
    "latin american",
    "mediterranean",
    "middle eastern",
    "southern",
    "spanish",
    "thai",
    "vietnamese",
]
# read file, fill cuisines

# - for now we will get only 1 recipe, but in the final scrape we will get 100.

headers = {
    "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    "x-rapidapi-key": "d5509c5927msha2bb4e40f916893p159c13jsnf38c8ae74e99",
}
j = 0
limit = len(cuisines)

for i in range(100):
    querystring = {"tags": cuisines[j], "number": "1"}
    response = requests.request("GET", url, headers=headers, params=querystring).json()
    name = response["recipes"][0]["title"]
    j = j + 1
    if j == limit:
        j = 0
    recipe_name = name.replace(" ", "-")
    data_file = open(recipe_name + "-data.json", "w")
    data_file.write(json.dumps(response))
    time.sleep(1)

    # print(json.dumps(response))

    # for k in response['recipes']:
    #     print(json.dumps(k))

    # print(response['recipes'][0]['title'])


# url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/search"

# querystring = {"query":"burger"}

# headers = {
#     'x-rapidapi-host': "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
#     'x-rapidapi-key': "d5509c5927msha2bb4e40f916893p159c13jsnf38c8ae74e99"
#     }

# response = requests.request("GET", url, headers=headers, params=querystring)

# print(response.text)
