import json
import os

culture = dict()
directory = "/mnt/c/Users/gummy/OneDrive/Desktop/CS373/assignment2/cs373-web-application/Backend/recipes/recipes-list"
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file) and ".json" in file:
        f = open(file, "r+")
        recipe = json.load(f)

        try:
            if recipe["dishTypes"] == []:
                print(filename)
        except:
            if recipe["recipes"][0]["dishTypes"] == []:
                print(filename)

        f.close()
