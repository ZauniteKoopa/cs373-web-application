import requests
import json

url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/search"

querystring = {"query": "Omelette", "number": "1"}

headers = {
    "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    "x-rapidapi-key": "2482fe0b08msh07505b451e765e8p16f687jsn66ee9f28c624",
}

response = requests.request("GET", url, headers=headers, params=querystring).json()
recipe_id = response["results"][0]["id"]


url = (
    "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/"
    + str(recipe_id)
    + "/information"
)

headers = {
    "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    "x-rapidapi-key": "2482fe0b08msh07505b451e765e8p16f687jsn66ee9f28c624",
}

response = requests.request("GET", url, headers=headers).json()
name = response["title"]
recipe_name = name.replace(" ", "-")
data_file = open(recipe_name + "-data.json", "w")
print(recipe_name)
data_file.write(json.dumps(response))
