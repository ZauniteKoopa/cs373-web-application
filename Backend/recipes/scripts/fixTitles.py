#!/usr/bin/python

from re import I
from typing import Dict
import requests
import json
import os
import time


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.


directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
for filename in os.listdir(directory):
    # print(filename)
    title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        f = open(file, "r+")
        recipe = json.load(f)

        # handle files that don't have recipes as a key
        try:
            recipe["recipes"]
            try:
                if len(recipe["recipes"][0]["cuisines"]) == 1:
                    recipe["cuisines"] = recipe["cuisines"][0]
            except:
                continue
        except:
            try:
                if len(recipe["cuisines"]) == 1:
                    recipe["cuisines"] = recipe["cuisines"][0]
            except:
                continue

    f.seek(0)
    f.write(json.dumps(recipe, indent=4))
    f.truncate()
    f.close()
