#!/usr/bin/python

from re import I
from typing import Dict
import requests
import json
import os
import time


# - possible outline of getting ingredients in scrape is to go over each recipe we scrape, choose one ingredient,
# - and then search for it's info in the rapid-api.
# - this will result in multiple ingredients that will have a relation to at least 1 recipe.


directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
cuisines = []
for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        f = open(file, "r+")
        recipe = json.load(f)

    # handle files that don't have recipes as a key
    if recipe["recipes"]:
        try:
            if len(recipe[0]["cuisines"]) == 1:
                recipe["cuisines"] = recipe["cuisines"][0]
        except (AttributeError, TypeError):
            continue
    else:
        try:
            if len(recipe["cuisines"]) == 1:
                recipe["cuisines"] = recipe["cuisines"][0]
        except (AttributeError, TypeError):
            continue


print(json.dumps(cuisines))
