#!/usr/bin/env python3

import os
import json
from bs4 import BeautifulSoup

directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/recipes/recipes-list"
recipe = dict()
for filename in os.listdir(directory):
    # print(filename)
    title = filename.replace("-data.json", "").replace("-", " ")
    file = os.path.join(directory, filename)

    f = open(file, "r+")
    recipe = json.load(f)

    # handle files that don't have recipes as a key
    try:
        recipe["recipes"]
        try:
            description = recipe["recipes"][0]["summary"]
            removeHTML = BeautifulSoup(description, features="html5lib")
            recipe["recipes"][0]["summary"] = removeHTML.get_text()

            description = recipe["recipes"][0]["instructions"]
            removeHTML = BeautifulSoup(description, features="html5lib")
            recipe["recipes"][0]["instructions"] = removeHTML.get_text()
        except:
            continue
    except:
        try:
            description = recipe["summary"]
            removeHTML = BeautifulSoup(description, features="html5lib")
            recipe["summary"] = removeHTML.get_text()

            description = recipe["instructions"]
            removeHTML = BeautifulSoup(description, features="html5lib")
            recipe["instructions"] = removeHTML.get_text()
        except:
            continue

    f.seek(0)
    f.write(json.dumps(recipe, indent=2))
    f.truncate()
    f.close()
