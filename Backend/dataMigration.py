import json
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from utils import update_culture_connections

# Initialize flask app and sqlalchemy instance
app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://AnthonyHoang750:Zephyr3074!@feast-for-friends-db.cs3t2qwf688y.us-east-1.rds.amazonaws.com:5432/postgres"
db = SQLAlchemy(app)


class Ingredient(db.Model):
    _tablename_ = "ingredient"
    id = db.Column("ingredient_id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    title = db.Column(db.Text)
    calories = db.Column(db.Float)
    fat = db.Column(db.Float)
    sugar = db.Column(db.Float)
    sodium = db.Column(db.Float)
    fiber = db.Column(db.Float)
    protein = db.Column(db.Float)
    price = db.Column(db.Float)
    carbs = db.Column(db.Float)
    vegan = db.Column(db.Boolean)
    description = db.Column(db.Text)
    image = db.Column(db.Text)
    recipeConnections = db.Column(db.Text)
    num_recipe_connections = db.Column(db.Text)

    def __init__(
        self,
        id,
        name,
        calories,
        fat,
        sugar,
        sodium,
        fiber,
        protein,
        price,
        carbs,
        vegan,
        description,
        image,
        title,
        recipeConnections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.calories = calories
        self.fat = fat
        self.sugar = sugar
        self.sodium = sodium
        self.fiber = fiber
        self.protein = protein
        self.price = price
        self.carbs = carbs
        self.vegan = vegan
        self.description = description
        self.image = image
        self.title = title
        self.recipeConnections = recipeConnections
        self.num_recipe_connections = num_recipe_connections


def import_ingredients():
    directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/ingredients/ingredients-list"
    ingredient_list = []
    for filename in os.listdir(directory):
        file = os.path.join(directory, filename)
        if os.path.isfile(file):
            f = open(file)
            ingredient = json.load(f)
            id = ingredient["id"]
            name = ingredient["name"]
            title = ingredient["title"]
            # calories = 0.0
            calories = (
                ingredient["nutrition"]["calories"]
                if ingredient["nutrition"]["calories"] != None
                else 0.0
            )

            nutrients = ingredient["nutrition"]["nutrients"]
            fat, carbs, protein, sugar, sodium, fiber = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            for j in range(len(nutrients)):
                if nutrients[j]["name"] == "Fat":
                    fat = nutrients[j]["amount"]
                if nutrients[j]["name"] == "Protein":
                    protein = nutrients[j]["amount"]
                if nutrients[j]["name"] == "Carbohydrates":
                    carbs = nutrients[j]["amount"]
                if nutrients[j]["name"] == "Sugar":
                    sugar = nutrients[j]["amount"]
                if nutrients[j]["name"] == "Sodium":
                    sodium = nutrients[j]["amount"]
                if nutrients[j]["name"] == "Fiber":
                    fiber = nutrients[j]["amount"]
            price = ingredient["price"] if ingredient["price"] != None else 0.0
            vegan = "vegan" in ingredient["badges"]
            description = (
                ingredient["description"] if ingredient["description"] != None else ""
            )
            image = ingredient["image"]
            recipeConnections = ""
            for i in ingredient["recipeConnections"]:
                recipeConnections = recipeConnections + str(i) + ","

            ingredient_item = Ingredient(
                id,
                name,
                calories,
                fat,
                sugar,
                sodium,
                fiber,
                protein,
                price,
                carbs,
                vegan,
                description,
                image,
                title,
                recipeConnections,
                len(ingredient["recipeConnections"]),
            )
            ingredient_list.append(ingredient_item)

    return ingredient_list


class Recipe(db.Model):
    _tablename_ = "recipe"
    id = db.Column("recipe_id", db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    cuisine = db.Column(db.String(50))
    image = db.Column(db.Text)
    # have to store ingredients as a string. Frontend will have to parse for ingredients
    ingredients = db.Column(db.Text)
    description = db.Column(db.Text)
    cookTime = db.Column(db.Integer)
    # have to combine all the steps into 1 long string frontend will parse
    instructions = db.Column(db.Text)
    dishType = db.Column(db.String(20))
    source = db.Column(db.Text)
    rating = db.Column(db.Integer)

    def __init__(
        self,
        id,
        name,
        cuisine,
        image,
        ingredients,
        description,
        cookTime,
        instructions,
        dishType,
        source,
        rating,
    ):
        self.id = id
        self.name = name
        self.cuisine = cuisine
        self.image = image
        self.ingredients = ingredients
        self.description = description
        self.cookTime = cookTime
        self.instructions = instructions
        self.dishType = dishType
        self.source = source
        self.rating = rating


def import_recipes():
    directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/recipes/recipes-list"
    recipe_list = []
    for filename in os.listdir(directory):
        file = os.path.join(directory, filename)

        if os.path.isfile(file):
            f = open(file)
            recipe = json.load(f)

            if "recipes" in recipe:
                id = recipe["recipes"][0]["id"]

                name = recipe["recipes"][0]["title"]

                cuisine = recipe["recipes"][0]["cuisines"]

                image = recipe["recipes"][0]["image"]

                ingredients = ""
                for i in recipe["recipes"][0]["extendedIngredients"]:
                    ingredient = i["original"]
                    ingredients += ingredient + "^"
                    ingredients += (
                        i["nameClean"]
                        if i["nameClean"] is not None
                        else i["name"] + "^"
                    )
                    ingredients += "^"

                description = recipe["recipes"][0]["summary"]
                cookTime = recipe["recipes"][0]["readyInMinutes"]

                dishType = recipe["recipes"][0]["dishTypes"]

                source = recipe["recipes"][0]["sourceUrl"]
                instructions = recipe["recipes"][0]["instructions"]
                rating = recipe["recipes"][0]["rating"]

            else:
                id = recipe["id"]

                name = recipe["title"]

                cuisine = recipe["cuisines"]

                image = recipe["image"]

                ingredients = ""
                for i in recipe["extendedIngredients"]:
                    ingredient = i["original"]
                    ingredients += ingredient + "^"
                    ingredients += (
                        i["nameClean"]
                        if i["nameClean"] is not None
                        else i["name"] + "^"
                    )
                    ingredients += "^"

                description = recipe["summary"]
                cookTime = recipe["readyInMinutes"]

                dishType = recipe["dishTypes"]

                source = recipe["sourceUrl"]
                instructions = recipe["instructions"]
                rating = recipe["rating"]

        recipe_item = Recipe(
            id,
            name,
            cuisine,
            image,
            ingredients,
            description,
            cookTime,
            instructions,
            dishType,
            source,
            rating,
        )
        recipe_list.append(recipe_item)

    return recipe_list


class Culture(db.Model):
    _tablename_ = "culture"
    id = db.Column("culture_id", db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    image = db.Column(db.String(100))
    description = db.Column(db.Text)
    language = db.Column(db.String(50))
    currency = db.Column(db.String(50))
    mapLatitude = db.Column(db.Float)
    mapLongitude = db.Column(db.Float)
    url = db.Column(db.String(100))
    subRegion = db.Column(db.String(100))
    cuisine = db.Column(db.String(100))
    produce = db.Column(db.String(100))
    restaurant_connections = db.Column(db.String(50))
    num_restaurant_connections = db.Column(db.Integer)
    recipe_connections = db.Column(db.String(50))
    num_recipe_connections = db.Column(db.Integer)

    def __init__(
        self,
        id,
        name,
        image,
        description,
        language,
        currency,
        mapLatitude,
        mapLongitude,
        url,
        subRegion,
        cuisine,
        produce,
        restaurant_connections,
        num_restaurant_connections,
        recipe_connections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.image = image
        self.description = description
        self.language = language
        self.currency = currency
        self.mapLatitude = mapLatitude
        self.mapLongitude = mapLongitude
        self.url = url
        self.subRegion = subRegion
        self.cuisine = cuisine
        self.produce = produce
        self.produce = produce
        self.restaurant_connections = restaurant_connections
        self.num_restaurant_connections = num_restaurant_connections
        self.recipe_connections = recipe_connections
        self.num_recipe_connections = num_recipe_connections


def import_cultures():
    directory = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/cultures/country-data-files"
    culture_list = []
    for filename in os.listdir(directory):
        file = os.path.join(directory, filename)

        if os.path.isfile(file):
            f = open(file)
            culture = json.load(f)
            id = culture["id"]
            name = culture["name"]
            image = culture["flag"]["file"]
            description = culture["cuisine_info"]
            language = culture["language"]
            currency = culture["currency"]["name"]
            # change to latitude and longitude later
            mapLatitude = culture["latitude"]
            mapLongitude = culture["longitude"]
            url = culture["wiki_url"]
            subRegion = culture["continent"]["name"]
            cuisine = culture["spoonacular_tag"]
            num_restaurant_connections = culture["num_restaurant_connections"]
            num_recipe_connections = culture["num_recipe_connections"]

            products = ""
            for product in culture["produce"]:
                products += product + ","

            restaurant_connections = ""
            for id_ in culture["restaurant_connections"]:
                restaurant_connections += str(id_) + ","

            recipe_connections = ""
            for id_ in culture["recipe_connections"]:
                recipe_connections += str(id_) + ","

        culture_item = Culture(
            id,
            name,
            image,
            description,
            language,
            currency,
            mapLatitude,
            mapLongitude,
            url,
            subRegion,
            cuisine,
            products,
            restaurant_connections,
            num_restaurant_connections,
            recipe_connections,
            num_recipe_connections,
        )
        culture_list.append(culture_item)
    return culture_list


class Restaurant(db.Model):
    _tablename_ = "restaurant"
    id = db.Column("restaurant_id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    culture = db.Column(db.Text)
    review1 = db.Column(db.Text)
    review2 = db.Column(db.Text)
    review3 = db.Column(db.Text)
    description = db.Column(db.Text)
    cost = db.Column(db.Integer)
    rating = db.Column(db.Float)
    mapLatitude = db.Column(db.Float)
    mapLongitude = db.Column(db.Float)
    phone = db.Column(db.String(50))
    address = db.Column(db.Text)
    city = db.Column(db.String(50))
    deliverable = db.Column(db.Boolean)
    image = db.Column(db.String(100))
    website = db.Column(db.Text)
    recipe_connections = db.Column(db.Text)
    num_recipe_connections = db.Column(db.Text)

    def __init__(
        self,
        id,
        name,
        culture,
        review1,
        review2,
        review3,
        description,
        cost,
        rating,
        mapLatitude,
        mapLongitude,
        phone,
        address,
        city,
        deliverable,
        image,
        website,
        recipe_connections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.culture = culture
        self.review1 = review1
        self.review2 = review2
        self.review3 = review3
        self.description = description
        self.cost = cost
        self.rating = rating
        self.mapLatitude = mapLatitude
        self.mapLongitude = mapLongitude
        self.phone = phone
        self.address = address
        self.city = city
        self.deliverable = deliverable
        self.image = image
        self.website = website
        self.recipe_connections = recipe_connections
        self.num_recipe_connections = num_recipe_connections


def get_restaurant_item(file):
    f = open(file)
    restaurant = json.load(f)
    id = restaurant["id"]
    name = restaurant["name"]

    culture = restaurant["culture"]

    review1 = restaurant["reviews"][0]["text"]
    review2 = restaurant["reviews"][1]["text"]
    review3 = restaurant["reviews"][2]["text"]

    description = restaurant["description"]

    cost = 0
    if "price" in restaurant.keys():
        cost = str.__len__(restaurant["price"])

    rating = restaurant["rating"]
    mapLatitude = restaurant["coordinates"]["latitude"]
    mapLongitude = restaurant["coordinates"]["longitude"]
    phone = restaurant["phone"]

    address = ""
    for k in restaurant["location"]["display_address"]:
        address = address + k

    city = restaurant["location"]["city"]
    deliverable = "delivery" in restaurant["transactions"]
    image = restaurant["image_url"]
    website = restaurant["url"]
    recipe_connections = ""
    for connection in restaurant["recipe_connections"]:
        recipe_connections += str(connection["id"]) + ","

    return Restaurant(
        id,
        name,
        culture,
        review1,
        review2,
        review3,
        description,
        cost,
        rating,
        mapLatitude,
        mapLongitude,
        phone,
        address,
        city,
        deliverable,
        image,
        website,
        recipe_connections,
        restaurant["num_recipe_connections"],
    )


def import_restaurants():
    austin = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/austin"
    dallas = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/dallas"
    houston = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/houston"
    sanAntonio = "/home/natew/Dropbox/Org/school/spring-22/swe/projects/cs373-web-application/Backend/restaurants/restaurant-data-files/san-antonio"
    restaurant_list = []
    for filename in os.listdir(austin):
        rest_file = os.path.join(austin, filename)
        restaurant_list.append(get_restaurant_item(rest_file))

    for filename in os.listdir(dallas):
        rest_file = os.path.join(dallas, filename)
        restaurant_list.append(get_restaurant_item(rest_file))

    for filename in os.listdir(houston):
        rest_file = os.path.join(houston, filename)
        restaurant_list.append(get_restaurant_item(rest_file))

    for filename in os.listdir(sanAntonio):
        rest_file = os.path.join(sanAntonio, filename)
        restaurant_list.append(get_restaurant_item(rest_file))

    return restaurant_list


update_culture_connections()

ingredient_list = import_ingredients()
print("done gathering ingredients")
recipe_list = import_recipes()
print("done gathering recipes")
culture_list = import_cultures()
print("done gathering cultures")
restaurant_list = import_restaurants()
print("done gathering restaurants")
print("Starting database update...")
db.session.commit()
db.drop_all()
db.create_all()
db.session.add_all(ingredient_list)
print("done exporting ingredients")
db.session.add_all(recipe_list)
print("done exporting recipes")
db.session.add_all(culture_list)
print("done exporting cultures")
db.session.add_all(restaurant_list)
print("done exporting restaurants")
db.session.commit()
print("Done")
