#!/usr/bin/env python3

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

db = SQLAlchemy()

# -- RECIPES -- #
class Recipe(db.Model):
    _tablename_ = "recipe"
    id = db.Column("recipe_id", db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    cuisine = db.Column(db.String(50))
    image = db.Column(db.Text)
    # have to store ingredients as a string. Frontend will have to parse for ingredients
    ingredients = db.Column(db.Text)
    description = db.Column(db.Text)
    cookTime = db.Column(db.Integer)
    # have to combine all the steps into 1 long string frontend will parse
    instructions = db.Column(db.Text)
    dishType = db.Column(db.String(20))
    source = db.Column(db.Text)
    rating = db.Column(db.Integer)

    def __init__(
        self,
        id,
        name,
        cuisine,
        image,
        ingredients,
        description,
        cookTime,
        instructions,
        dishType,
        source,
        rating,
    ):
        self.id = id
        self.name = name
        self.cuisine = cuisine
        self.image = image
        self.ingredients = ingredients
        self.description = description
        self.cookTime = cookTime
        self.instructions = instructions
        self.dishType = dishType
        self.source = source
        self.rating = rating

    def into_dict(self):
        result = dict()
        result["id"] = self.id
        result["name"] = self.name
        result["cuisine"] = self.get_culture_connection()
        result["image"] = self.image
        result["ingredients"] = self.get_ingredients_connections(
            self.ingredients.split("^")[:-1]
        )
        result["description"] = self.description
        result["cookTime"] = self.cookTime
        result["instructions"] = self.instructions
        result["dishType"] = self.dishType
        result["source"] = self.source
        result["rating"] = self.rating

        return result

    # Main function to get culture connection for recipe
    def get_culture_connection(self):
        cultureList = Culture.query.all()
        cultureConnection = {}

        # Search through all the cultures and find the ID
        for culture in cultureList:
            for current_id in culture.recipe_connections.split(",")[:-1]:
                current_id_num = int(current_id)
                if current_id_num is self.id:
                    cultureConnection["name"] = culture.name
                    cultureConnection["id"] = culture.id
                    return cultureConnection

        cultureConnection["name"] = "Not Known"
        cultureConnection["id"] = None
        return cultureConnection

    # Main function to get ingredient connections
    def get_ingredients_connections(self, ingredient_list):
        ingredient_connections = []
        for ingredient_index in range(1, len(ingredient_list), 2):

            ingredient = ingredient_list[ingredient_index]

            # Get ingredient connection name
            ingredient_connection = {}
            ingredient_connection["name"] = ingredient_list[ingredient_index - 1]

            # Get ingredient connection instance if it exist
            ingredient_key = "%" + ingredient.lower() + "%"
            actual_ingredient_instance = Ingredient.query.filter(
                func.lower(Ingredient.name).contains(ingredient_key)
            ).all()
            if len(actual_ingredient_instance) == 0:
                ingredient_connection["id"] = None
            else:
                ingredient_connection["id"] = actual_ingredient_instance[0].id

            ingredient_connections.append(ingredient_connection)

        return ingredient_connections


# -- INGREDIENTS -- #
class Ingredient(db.Model):
    _tablename_ = "ingredient"
    id = db.Column("ingredient_id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    title = db.Column(db.Text)
    calories = db.Column(db.Float)
    fat = db.Column(db.Float)
    sugar = db.Column(db.Float)
    sodium = db.Column(db.Float)
    fiber = db.Column(db.Float)
    protein = db.Column(db.Float)
    price = db.Column(db.Float)
    carbs = db.Column(db.Float)
    vegan = db.Column(db.Boolean)
    description = db.Column(db.Text)
    image = db.Column(db.Text)
    recipeConnections = db.Column(db.Text)
    num_recipe_connections = db.Column(db.Text)

    def __init__(
        self,
        id,
        name,
        calories,
        fat,
        sugar,
        sodium,
        fiber,
        protein,
        price,
        carbs,
        vegan,
        description,
        image,
        title,
        recipeConnections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.calories = calories
        self.fat = fat
        self.sugar = sugar
        self.sodium = sodium
        self.fiber = fiber
        self.protein = protein
        self.price = price
        self.carbs = carbs
        self.vegan = vegan
        self.description = description
        self.image = image
        self.title = title
        self.recipeConnections = recipeConnections
        self.num_recipe_connections = num_recipe_connections

    def into_dict(self):
        result = dict()
        result["id"] = self.id
        result["name"] = self.name
        result["calories"] = self.calories
        result["fat"] = self.fat
        result["sugar"] = self.sugar
        result["sodium"] = self.sodium
        result["fiber"] = self.fiber
        result["protein"] = self.protein
        result["price"] = self.price
        result["carbs"] = self.carbs
        result["vegan"] = self.vegan
        result["description"] = self.description
        result["image"] = self.image
        result["title"] = self.title
        result["recipeConnections"] = self.get_recipe_connection(
            self.recipeConnections.split(",")[:-1]
        )
        result["num_recipe_connections"] = self.num_recipe_connections
        return result

    def get_recipe_connection(self, recipe_connections):
        connections = []
        # Search through all the cultures and find the ID
        for connection in recipe_connections:
            result = Recipe.query.filter_by(id=int(connection)).first()
            connections.append({"name": result.name, "id": result.id})

        return connections


# -- RESTAURANTS -- #
class Restaurant(db.Model):
    _tablename_ = "restaurant"
    id = db.Column("restaurant_id", db.Integer, primary_key=True)
    name = db.Column(db.Text)
    culture = db.Column(db.Text)
    review1 = db.Column(db.Text)
    review2 = db.Column(db.Text)
    review3 = db.Column(db.Text)
    description = db.Column(db.Text)
    cost = db.Column(db.Integer)
    rating = db.Column(db.Float)
    mapLatitude = db.Column(db.Float)
    mapLongitude = db.Column(db.Float)
    phone = db.Column(db.String(50))
    address = db.Column(db.Text)
    city = db.Column(db.String(50))
    deliverable = db.Column(db.Boolean)
    image = db.Column(db.String(100))
    website = db.Column(db.Text)
    recipe_connections = db.Column(db.Text)
    num_recipe_connections = db.Column(db.Text)

    def __init__(
        self,
        id,
        name,
        culture,
        review1,
        review2,
        review3,
        description,
        cost,
        rating,
        mapLatitude,
        mapLongitude,
        phone,
        address,
        city,
        deliverable,
        image,
        website,
        recipe_connections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.culture = culture
        self.review1 = review1
        self.review2 = review2
        self.review3 = review3
        self.description = description
        self.cost = cost
        self.rating = rating
        self.mapLatitude = mapLatitude
        self.mapLongitude = mapLongitude
        self.phone = phone
        self.address = address
        self.city = city
        self.deliverable = deliverable
        self.image = image
        self.website = website
        self.recipe_connections = recipe_connections
        self.num_recipe_connections = num_recipe_connections

    def into_dict(self):
        result = dict()
        result["id"] = self.id
        result["name"] = self.name
        result["culture"] = self.get_culture_connection()
        result["review1"] = self.review1
        result["review2"] = self.review2
        result["review3"] = self.review3
        result["description"] = self.description
        result["cost"] = self.cost
        result["rating"] = self.rating
        result["mapLatitude"] = self.mapLatitude
        result["mapLongitude"] = self.mapLongitude
        result["phone"] = self.phone
        result["address"] = self.address
        result["city"] = self.city
        result["deliverable"] = self.deliverable
        result["image"] = self.image
        result["website"] = self.website
        result["recipe_connections"] = self.get_recipe_connections_json()
        result["num_recipe_connections"] = self.num_recipe_connections

        return result

    # Main function to get culture connection for current restaurant
    def get_culture_connection(self):
        cultureList = Culture.query.all()
        cultureConnection = {}

        # Search through all the cultures and find the ID
        for culture in cultureList:
            for current_id in culture.restaurant_connections.split(",")[:-1]:
                current_id_num = int(current_id)
                if current_id_num is self.id:
                    cultureConnection["name"] = culture.name
                    cultureConnection["id"] = culture.id
                    return cultureConnection

        cultureConnection["name"] = "Not Known"
        cultureConnection["id"] = None
        return cultureConnection

    def get_recipe_connections_json(restaurant):
        recipe_connections_json = []
        for id in restaurant.recipe_connections.split(",")[:-1]:
            recipe_connection = {}
            recipe_connection["id"] = id
            recipe_connection["name"] = Recipe.query.get(int(id)).name
            recipe_connections_json.append(recipe_connection)

        return recipe_connections_json


class Culture(db.Model):
    _tablename_ = "culture"
    id = db.Column("culture_id", db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    image = db.Column(db.String(100))
    description = db.Column(db.Text)
    language = db.Column(db.String(50))
    currency = db.Column(db.String(50))
    mapLatitude = db.Column(db.Float)
    mapLongitude = db.Column(db.Float)
    url = db.Column(db.String(100))
    subRegion = db.Column(db.String(100))
    cuisine = db.Column(db.String(100))
    produce = db.Column(db.String(100))
    restaurant_connections = db.Column(db.String(50))
    num_restaurant_connections = db.Column(db.Integer)
    recipe_connections = db.Column(db.String(50))
    num_recipe_connections = db.Column(db.Integer)

    def __init__(
        self,
        id,
        name,
        image,
        description,
        language,
        currency,
        mapLatitude,
        mapLongitude,
        url,
        subRegion,
        cuisine,
        produce,
        restaurant_connections,
        num_restaurant_connections,
        recipe_connections,
        num_recipe_connections,
    ):
        self.id = id
        self.name = name
        self.image = image
        self.description = description
        self.language = language
        self.currency = currency
        self.mapLatitude = mapLatitude
        self.mapLongitude = mapLongitude
        self.url = url
        self.subRegion = subRegion
        self.cuisine = cuisine
        self.produce = produce
        self.produce = produce
        self.restaurant_connections = restaurant_connections
        self.num_restaurant_connections = num_restaurant_connections
        self.recipe_connections = recipe_connections
        self.num_recipe_connections = num_recipe_connections

    def into_dict(culture):
        result = dict()
        result["id"] = culture.id
        result["name"] = culture.name
        result["image"] = culture.image
        result["description"] = culture.description
        result["language"] = culture.language
        result["currency"] = culture.currency
        result["mapLatitude"] = culture.mapLatitude
        result["mapLongitude"] = culture.mapLongitude
        result["url"] = culture.url
        result["subRegion"] = culture.subRegion
        result["cuisine"] = culture.cuisine
        result["produce"] = culture.produce.split(",")[:-1]
        result["restaurant_connections"] = culture.get_restaurant_connections_json()
        result["num_restaurant_connections"] = culture.num_restaurant_connections
        result["recipe_connections"] = culture.get_recipe_connections_json()
        result["num_recipe_connections"] = culture.num_recipe_connections

        return result

    def get_recipe_connections_json(culture):
        recipe_connections_json = []
        for id in culture.recipe_connections.split(",")[:-1]:
            recipe_connection = {}
            recipe_connection["id"] = id
            recipe_connection["name"] = Recipe.query.get(int(id)).name
            recipe_connections_json.append(recipe_connection)

        return recipe_connections_json

    def get_restaurant_connections_json(culture):
        restaurant_connections_json = []
        for id in culture.restaurant_connections.split(",")[:-1]:
            restaurant_connection = {}
            restaurant_connection["id"] = id
            restaurant_connection["name"] = Restaurant.query.get(int(id)).name
            restaurant_connections_json.append(restaurant_connection)

        return restaurant_connections_json
