![https://gitlab.com/ZauniteKoopa/cs373-web-application/-/pipelines](https://img.shields.io/gitlab/pipeline-status/ZauniteKoopa/cs373-web-application?branch=main&style=flat-square)
![https://gitlab.com/ZauniteKoopa/cs373-web-application/-/issues/?sort=label_priority_desc&state=all](https://flat.badgen.net/gitlab/issues/ZauniteKoopa/cs373-web-application)
![https://gitlab.com/ZauniteKoopa/cs373-web-application/-/issues](https://flat.badgen.net/gitlab/open-issues/ZauniteKoopa/cs373-web-application)
![https://gitlab.com/ZauniteKoopa/cs373-web-application/-/issues/?sort=label_priority_desc&state=closed](https://flat.badgen.net/gitlab/closed-issues/ZauniteKoopa/cs373-web-application)
![](https://flat.badgen.net/gitlab/license/gitlab-org/omnibus-gitlab)
# CS373 Web Application Project

Group Members (GitLab ID, EID)

- Anthony Hoang   (ZauniteKoopa, ath2339)
- Tanner Dreiling (tdreiling19, trd758)
- Nathan Whyte    (nathanwhyte, nmw766)
- Ali Al-Adhami   (Ali-AlAdhami, aaa7344)

GitLab Pipelines: https://gitlab.com/ZauniteKoopa/cs373-web-application/-/pipelines

Website: https://www.feastforfriends.me/

Comments: From the root folder, you can do make frontend-docker to get a bash terminal running in an enviornment with all the tools needed.
When running in this docker, the frontend application will be in port 3001. When running the backend, use the make command backend-dev-docker.
This will create a bash terminal running in an enviornment with all the tools necessary for the backend. All mounting instructions came from
Geoff's and Caitlin's instructions posted on the SWE discord and Selenium-Test structure was inspired by CitiesLoveBaseball from Fall 2021

# Phase 1

Phase 1 Git SHA: 467e4e564d5c57f07ffe7d1407c7f4f742062e7e

Phase 1 Leader: Anthony Hoang

Developer            | Estimated Completion | Actual Completion |
-------------------- | -------------------- | ----------------- |
**Anthony Hoang**    | 16 hours             | 24 hours          |
Tanner Dreiling      | 15 hours             | 17 hours          |
Nathan Whyte         | 16 hours             | 19 hours          |
Ali Al-Adhami        | 15 hours             | 28 hours          |

# Phase 2

Phase 2 Git SHA: f9c00922745ef930e58b258fe02a8b475892631

Phase 2 Leader: Nathan Whyte

Developer            | Estimated Completion | Actual Completion |
-------------------- | -------------------- | ----------------- |
Anthony Hoang        | 24 hours             | 38 hours          |
Tanner Dreiling      | 25 hours             | 35 hours          |
**Nathan Whyte**     | 32 hours             | 40 hours          |
Ali Al-Adhami        | 20 hours             | 30 hours          |

# Phase 3

Phase 3 Git SHA: cfc6fe9c9558e6220b2d42e13feefe4b143b40ae

Phase 3 Leader: Tanner Dreiling

Developer            | Estimated Completion | Actual Completion |
-------------------- | -------------------- | ----------------- |
Anthony Hoang        | 20 hours             | 30 hours          |
**Tanner Dreiling**  | 17 hours             | 20 hours          |
Nathan Whyte         | 19 hours             | 24 hours          |
Ali Al-Adhami        | 25 hours             | 18 hours          |

# Phase 4

Phase 4 Git SHA: f92aa478530e5cc552fabe03588e21ec7429a40e

Phase 4 Leader: Ali Al-Adhami

Developer            | Estimated Completion | Actual Completion |
-------------------- | -------------------- | ----------------- |
Anthony Hoang        | 10 hours             | 14 hours          |
Tanner Dreiling      | 10 hours             | 12 hours          |
Nathan Whyte         | 8 hours              | 13 hours          |
**Ali Al-Adhami**    | 25 hours             | 35 hours          |


# Proposal

Canvas / Discord Group Number: 10AM Group 9

Name of the team members:
- Anthony Hoang   (ath2339)
- Nathan Whyte    (nmw766)
- Tanner Dreiling (trd758)
- Ali Al-Adhami   (aaa7344)

Name of the project: Feast for Friends

URL of GitLab Repo: https://gitlab.com/ZauniteKoopa/cs373-web-application

Proposed Project:
- Feast for Friends is a tool for those who are looking for recipes, figuring out what they can cook with a certain ingredient, or want to find a restaurant. Our website will feature recipes, ingredients, and restaurants from a variety of cultures. 
  
  Food is an integral part of all of our lives, so Feast for Friends helps you find great recipes that fit any appetite or budget. Have an ingredient that you want to incorporate in your cooking? Feast for Friends gives you recipes that use what you have. Not in the mood for cooking? Feast for Friends will provide restaurants to fit your tastes.

Data Sources with RESTful API for the Project:
- Recipes and ingredients: https://rapidapi.com/apidojo/api/tasty/
- Nutrition: https://english.api.rakuten.net/msilverman/api/nutritionix-nutrition-database
- Restaurant location and culture: https://developers.google.com/maps/
- Restaurants, ratings, reviews: https://fusion.yelp.com/
- Culture location and info: https://rapidapi.com/natkapral/api/countries-cities/
- Culture region: https://restcountries.com/#api-endpoints-v3-all

Data models for the project:
- Recipes
- Ingredients
- Restaurants
- Cultures

Estimate number of instances for each model:
- 100 recipes
- Average 8 ingredients per recipe
- 100 restaurants
- 100 cultures

Data model attributes (filter / sort by):
- Recipes    : name, culture of origin, nutrition, rating, cost 
- Ingredients: name, macros (protein, carbs, fat), type (meat, produce, dairy, etc.), cost, availability in current region
- Restaurants: name, recipe, culture of origin, rating, distance from current location
- Cultures   : name, continent, language

Data model attributes (Searching on instance pages):
- Recipes    : ingredient list, instructions, reference to recipe source, photo of food, estimated time
- Ingredients: complete nutrition facts, photo of ingredient, places to buy, recipes containing ingredient, description of ingredient
- Restaurants: location, photo of place, reviews, link to restaurant webpage, description of restaurant
- Cultures   : location on map, flag, description, list of resources, sample recipes, sample restaurant

Data model connections:
- Recipes     ⟶ Ingredients: Recipes are made up of ingredients
- Recipes     ⟶ Restaurants: Find a restaurant that makes the recipe you're looking at if you want to try the dish before you cook it.
- Ingredients ⟶ Recipes    : If you want to use a certain ingredient, find a recipe that requires it.
- Ingredients ⟶ Restaurants: Find a restaurant that serves this ingredient.
- Restaurants ⟶ Recipes    : If you want to make a similar dish to the one that you ate a restaurant. 
- Restaurants ⟶ Culture    : If you want to know more about where the restaurant's food originates from.
- Culture     ⟶ Recipes    : Find recipes from a certain culture of origin.
- Culture     ⟶ Restaurant : Search for a restaurant that serves a certain type of food.

Media Associated with each Model:
- Recipes    : photo of finished recipe, link to source of recipe, video of the recipe being made, text instructions
- Ingredients: photo of ingredient, diagram of nutrition facts
- Restaurants: photo of restaurant, location of restaurant on a map embed, text description, reviews
- Culture    : location on map, photos of culture, text description

What 3 Questions Answered by the site?:
- What recipe can I make with this ingredient that I have?
- Is there a way for me to try another culture's food?
- What is a nutritious way to utilize this ingredient or eat this kind of food?
