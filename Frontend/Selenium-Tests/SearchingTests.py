import unittest
import sys

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

# TO RUN:
# python ./Frontend/Selenium-Tests/NavBarTests.py

# Inspired by CitiesLoveBaseball from Fall 2021

URL = "https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Recipes"

class SortTests(unittest.TestCase):

  def setUp(self):
    # Set up options for chrome
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--window-size=1280,800')
    chrome_options.add_argument('--allow-insecure-localhost')
    chrome_options.add_argument('--start-maximized')

    # Create driver
    self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    self.driver.get(URL)
    self.driver.maximize_window()
    
  # Close browser and quit after all test
  def tearDown(self) :
    self.driver.quit()

  # Test if searching works for 1 word queries
  def testSingleWordSearch (self):
    self.driver.get(URL)
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass
    
    # Add text input into search bar and press enter
    searchBar = self.driver.find_element(By.NAME, 'search-bar-input')
    searchBar.send_keys('cheese')
    searchBar.send_keys(Keys.ENTER)

    # Wait for loading screen again
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass

    # Check if number of results is equal
    num_instances = self.driver.find_element(By.NAME, 'num-instances')
    self.assertEqual(num_instances.text, '7 Recipes Found')

    # Click on the first element in the table which is Cheese Dill Scones
    self.driver.find_element(By.ID, 'search-page-table-row').click()
    
    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'recipeName')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'recipeName').text, 'Recipe: Cheese Dill Scones')


  # Test if searching works for multiple word queries
  def testMultipleWordSearch (self):
    self.driver.get(URL)
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass
    
    # Add text input into search bar and press enter
    searchBar = self.driver.find_element(By.NAME, 'search-bar-input')
    searchBar.send_keys('chicken stew')
    searchBar.send_keys(Keys.ENTER)

    # Wait for loading screen again
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass

    # Check if number of results is equal
    num_instances = self.driver.find_element(By.NAME, 'num-instances')
    self.assertEqual(num_instances.text, '1 Recipes Found')

    # Click on the first element in the table which is African Chicken Peanut Stew
    self.driver.find_element(By.ID, 'search-page-table-row').click()
    
    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'recipeName')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'recipeName').text, 'Recipe: African Chicken Peanut Stew')


    
if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'])