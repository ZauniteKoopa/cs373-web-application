import unittest
import sys

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager

# TO RUN:
# python ./Frontend/Selenium-Tests/NavBarTests.py

# Inspired by CitiesLoveBaseball from Fall 2021

URL = "https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Recipes"

class FilterTests(unittest.TestCase):

  def setUp(self):
    # Set up options for chrome
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--window-size=1280,800')
    chrome_options.add_argument('--allow-insecure-localhost')
    chrome_options.add_argument('--start-maximized')

    # Create driver
    self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    self.driver.get(URL)
    self.driver.maximize_window()
    
  # Close browser and quit after all test
  def tearDown(self) :
    self.driver.quit()

  # Test for sort ascending functionality
  def testFilter (self):
    self.driver.get(URL)
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass
    
    select_filters = self.driver.find_elements(By.CLASS_NAME, 'search-filter-select')
    
    # Check if 4 select_filters are found
    self.assertEqual(len(select_filters), 4)
    
    # Get the first select (category) and set it to "salad"
    categorySelect = Select(select_filters[0])
    categorySelect.select_by_visible_text('salad')

    # Wait for loading screen again
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass

    # Click on the first element in the table which is an Thai Salad (the only salad in the database)
    self.driver.find_element(By.ID, 'search-page-table-row').click()

    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'recipeName')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'recipeName').text, 'Recipe: Thai Grilled Beef Salad')


    
if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'])