import unittest
import sys

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

# TO RUN:
# python ./Frontend/Selenium-Tests/NavBarTests.py

# Inspired by CitiesLoveBaseball from Fall 2021

URL = "https://dev-branch.d16rrm87w18uqi.amplifyapp.com"

class SearchPageTests(unittest.TestCase):

  def setUp(self):
    # Set up options for chrome
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--window-size=1280,800')
    chrome_options.add_argument('--allow-insecure-localhost')
    chrome_options.add_argument('--start-maximized')

    # Create driver
    self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    self.driver.get(URL)
    self.driver.maximize_window()
    
  # Close browser and quit after all test
  def tearDown(self) :
    self.driver.quit()

  # Test functionality of culture search page
  def testCultureSearch (self):
    self.driver.get(URL + "/Cultures")

    # Wait for loading screen
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass
    
    # Get the first element in table and click
    self.driver.find_element(By.ID, 'search-page-table-row').click()

    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'cultureName')))
    finally:
      pass
    
    self.assertEqual(self.driver.find_element(By.ID, 'cultureName').text, 'Argentina')

  # Test functionality of recipe search page
  def testRecipeSearch (self):
    self.driver.get(URL + "/Recipes")

    # Wait for loading screen
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass
    
    # Click on the first element on the table
    self.driver.find_element(By.ID, 'search-page-table-row').click()

    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'recipeName')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'recipeName').text, 'Recipe: African Chicken Peanut Stew')

  # Test functionality of Restaurant Search
  def testRestaurantSearch (self):
    self.driver.get(URL + "/Restaurants")

    # Wait for loading screen
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass

    # Click on the first element of the table
    self.driver.find_element(By.ID, 'search-page-table-row').click()

    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'restName')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'restName').text, '12 Cuts Brazilian Steakhouse')

  # Test ingredient search
  def testIngredientSearch (self):
    self.driver.get(URL + "/Ingredients")

    # Wait for loading screen
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'num-instances')))
    finally:
      pass

    # Click on the first element in the table
    self.driver.find_element(By.ID, 'search-page-table-row').click()

    # See if it leads to the correct page
    try:
      WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'ingredient-title')))
    finally:
      pass

    self.assertEqual(self.driver.find_element(By.ID, 'ingredient-title').text, 'Ah-So Barbecue Sauce, 11 oz')

    
if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'])