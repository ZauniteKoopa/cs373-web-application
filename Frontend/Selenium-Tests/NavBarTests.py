import unittest
import sys

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

# TO RUN:
# python ./Frontend/Selenium-Tests/NavBarTests.py

# Inspired by CitiesLoveBaseball from Fall 2021

URL = "https://dev-branch.d16rrm87w18uqi.amplifyapp.com/"

class NavBarTests(unittest.TestCase):

    # Get drivers and run website before all tests
    def setUp(self):
        # Set up options for chrome
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--window-size=1280,800')
        chrome_options.add_argument('--allow-insecure-localhost')
        chrome_options.add_argument('--start-maximized')

        # Create driver
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        self.driver.get(URL)
        self.driver.maximize_window()

    
    # Close browser and quit after all test
    def tearDown(self) :
        self.driver.quit()

    
    # Testing home link on NavBar
    def testHome(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Home').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Home')

    # Testing about link on nav bar
    def testAbout(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'About').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/About')

    # Testing Cultures link on nav bar
    def testCultures(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Cultures').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Cultures')

    # Testing Recipes link on nav bar
    def testRecipes(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Recipes').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Recipes')

    # Testing Restaurant link on nav bar
    def testRestaurants(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Restaurants').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Restaurants')

    # Testing Ingredients link on nav bar
    def testIngredients(self) :
        self.driver.get(URL)
        self.driver.find_element(By.LINK_TEXT, 'Ingredients').click()
        self.assertEqual(self.driver.current_url, 'https://dev-branch.d16rrm87w18uqi.amplifyapp.com/Ingredients')

# If running this script directly
if __name__ == "__main__":
    unittest.main(argv=['first-arg-is-ignored'])