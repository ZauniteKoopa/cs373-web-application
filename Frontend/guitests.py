import sys, os

# Inspired by MyTechReview from Fall 2021

if __name__ == "__main__":

    # get correct python command for the current machine
    try:
        python = sys.argv[1]
    except:
        python = 'python3'
    
    # Run test files here as you would normally do on the command line
    os.system(python + " ./Selenium-Tests/NavBarTests.py")
    os.system(python + " ./Selenium-Tests/AboutPageTests.py")
    os.system(python + " ./Selenium-Tests/SearchPageTests.py")
    os.system(python + " ./Selenium-Tests/SortTests.py")
    os.system(python + " ./Selenium-Tests/FilterTest.py")
    os.system(python + " ./Selenium-Tests/SearchingTests.py")
