import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import RestaurantSearchPage from '../../Components/ModelSearchPages/RestaurantSearchPage';
import { getNumericalFilters } from '../../Components/ModelSearchPages/Filtering';
import { NUM_INSTANCES_PER_PAGE } from '../../Constants/ModelSearchConstants';
import { handleSortButtonClick } from '../../Components/ModelSearchPages/Sorting';
import { getSearchQuery } from '../../Components/ModelSearchPages/Searching';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import SearchPageSkeleton from '../../Components/Skeleton/SearchPageSkeleton/SearchPageSkeleton';

// Constants to help with filtering
const COST_STEP_SIZE = 1;
const COST_NUM_STEPS = 3;

const RATINGS_STEP_SIZE = 1;
const RATINGS_NUM_STEPS = 5;

// Function to change filters
//  Pre: newFilters are the filters to be changed, currentFilterReqs are the current filters, setFilterReqs is the state function to change it
//  Post: Changes the set filters accordingly
function getNewFilterState(newFilters, currentFilterReqs) {
  let newFilterState = {};

  // get new filter state: culture
  newFilterState.culture =
    newFilters.culture === 'None' ? undefined : newFilters.culture || currentFilterReqs.culture;

  // get new filter state: location
  newFilterState.location =
    newFilters.location === 'None' ? undefined : newFilters.location || currentFilterReqs.location;

  // get new filter state: cost
  let cost = newFilters.cost !== undefined ? Number(newFilters.cost) : undefined;

  if (typeof cost === 'number') {
    let cost_filter = [cost, '>='];
    newFilterState.cost = cost_filter;
  } else {
    newFilterState.cost = currentFilterReqs.cost;
  }

  // get new filter state: rating
  let rating = newFilters.rating !== undefined ? Number(newFilters.rating) : undefined;

  if (typeof rating === 'number') {
    let rating_filter = [rating, '>='];
    newFilterState.rating = rating_filter;
  } else {
    newFilterState.rating = currentFilterReqs.rating;
  }

  // Return new filters
  return newFilterState;
}

// Main function for API router:
//  Main goal for this router is to make an API call to get list of restaurants
//  and then send that list of restaurants into RestaurantSearchPage
function RestaurantSearchRouter() {
  // Setting state
  const [errorFlag, setErrorFlag] = useState(false);
  const [filterCategories, setFilterCategories] = useState(null);

  // Setting API filter state
  const [apiState, setApiState] = useState({
    content: null,
    sortSetting: {
      attribute: 'name',
      ascending: true
    },
    activeFilters: {},
    currentPage: 1
  });

  // Get search params from URL
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = getSearchQuery(searchParams);

  // Get list of filters
  useEffect(() => {
    // Function to get filters via API call
    const getFilters = async () => {
      await fetch('https://api.feastforfriends.me/api/restaurants/filters')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setFilterCategories(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Ensures that only run api calls once
    if (!errorFlag && filterCategories === null) {
      getFilters();
    }
  });

  // Get list of shallow recipes
  useEffect(() => {
    // Function to get all restaurant instances on this page
    const getRestaurants = async () => {
      // Pagination
      const currentPage = apiState.currentPage;

      // Sorting attributes
      const sortSettingAttribute = apiState.sortSetting.attribute;
      const isSortDescending = apiState.sortSetting.ascending ? '0' : '1';

      // Filtering attributes
      const locationFilter = apiState.activeFilters.location || '';
      const cultureFilter = apiState.activeFilters.culture || '';

      let costFilter = apiState.activeFilters.cost;
      costFilter = costFilter === undefined ? 0.0 : costFilter[0];

      let ratingFilter = apiState.activeFilters.rating;
      ratingFilter = ratingFilter === undefined ? 0 : ratingFilter[0];

      await fetch(
        'https://api.feastforfriends.me/api/restaurants?sort_attribute=' +
          sortSettingAttribute +
          '&sort_desc=' +
          isSortDescending +
          '&city=' +
          locationFilter +
          '&culture=' +
          cultureFilter +
          '&cost=' +
          costFilter +
          '&rating=' +
          ratingFilter +
          '&search_term=' +
          searchQuery +
          '&page_num=' +
          currentPage
      )
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          setApiState({
            content: data,
            sortSetting: apiState.sortSetting,
            activeFilters: apiState.activeFilters,
            currentPage: apiState.currentPage
          });
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Ensures that only run api calls once
    if (!errorFlag && filterCategories !== null) {
      getRestaurants();
    }
  }, [
    apiState.activeFilters,
    apiState.currentPage,
    apiState.sortSetting,
    filterCategories,
    searchParams
  ]);

  // Check if still loading categories
  if (!errorFlag && filterCategories === null) {
    return <SearchPageSkeleton />;

    // Check if loading data
  } else if (!errorFlag && apiState.content === null) {
    return <SearchPageSkeleton />;

    // Error
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // On Sort setting change function
  let onSortSettingChange = (newSortAttribute) => {
    let newSortSetting = handleSortButtonClick(apiState.sortSetting, newSortAttribute);

    setApiState({
      content: null,
      sortSetting: newSortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: apiState.currentPage
    });
  };

  // If active filters change function
  let onActiveFiltersChange = (newFilters) => {
    let newFilterState = getNewFilterState(newFilters, apiState.activeFilters);

    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: newFilterState,
      currentPage: 1
    });
  };

  // Event handler function to handle pageChange
  let onPageChange = (newPage) => {
    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: newPage
    });
  };

  // State function to apply the new quey to the filters
  const setSearchQuery = (newQuery) => {
    setSearchParams({
      'search-bar-input': newQuery
    });
  };

  // Set numperpage
  const numPerPage = NUM_INSTANCES_PER_PAGE;
  const restaurants = apiState.content.data;
  const totalPages = apiState.content.num_pages;
  const totalInstances = apiState.content.results_total;

  // Process database to get relevant filters
  let cultureFiltersList = filterCategories.culture;
  let locationFiltersList = filterCategories.location;
  let costFiltersList = getNumericalFilters(0, COST_STEP_SIZE, COST_NUM_STEPS);
  let ratingFiltersList = getNumericalFilters(0, RATINGS_STEP_SIZE, RATINGS_NUM_STEPS);

  return (
    <RestaurantSearchPage
      database={restaurants}
      numPerPage={numPerPage}
      cultureFiltersList={cultureFiltersList}
      locationFiltersList={locationFiltersList}
      costFiltersList={costFiltersList}
      ratingFiltersList={ratingFiltersList}
      totalInstances={totalInstances}
      sortSetting={apiState.sortSetting}
      currentPage={apiState.currentPage}
      activeFilters={apiState.activeFilters}
      onSortSettingChange={onSortSettingChange}
      onPageChange={onPageChange}
      onActiveFiltersChange={onActiveFiltersChange}
      searchQuery={searchQuery}
      setSearchQuery={setSearchQuery}
      totalPages={totalPages}
    />
  );
}

export default RestaurantSearchRouter;
