import React, { useState, useEffect } from 'react';
import RecipeSearchPage from '../../Components/ModelSearchPages/RecipeSearchPage';
import { useSearchParams } from 'react-router-dom';
import { getNumericalFilters } from '../../Components/ModelSearchPages/Filtering';
import { NUM_INSTANCES_PER_PAGE } from '../../Constants/ModelSearchConstants';
import { handleSortButtonClick } from '../../Components/ModelSearchPages/Sorting';
import { getSearchQuery } from '../../Components/ModelSearchPages/Searching';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import SearchPageSkeleton from '../../Components/Skeleton/SearchPageSkeleton/SearchPageSkeleton';

// Constants for filter formatting
const TIME_START = 0.25;
const TIME_STEP_SIZE = 0.25;
const TIME_NUM_STEPS = 5;

const RATINGS_STEP_SIZE = 1;
const RATING_NUM_STEPS = 5;

// Conversion from minutes to hours
const MIN_PER_HOUR = 60;

// Functional component to change filters
//  Pre: newFilters are the filters to be changed, currentFilterReqs are the current filters, setFilterReqs is the state function to change it
//  Post: Changes the set filters accordingly
function getNewFilterState(newFilters, currentFilterReqs) {
  let newFilterState = {};

  // get new filter state: category
  newFilterState.category =
    newFilters.category === 'None' ? undefined : newFilters.category || currentFilterReqs.category;

  // get new filter state: culture
  newFilterState.culture =
    newFilters.culture === 'None' ? undefined : newFilters.culture || currentFilterReqs.culture;

  // get new filter state: time
  let time = newFilters.time !== undefined ? Number(newFilters.time) * MIN_PER_HOUR : undefined;

  if (typeof time === 'number') {
    let time_filter = [time, '<='];
    newFilterState.time = time_filter;
  } else {
    newFilterState.time = currentFilterReqs.time;
  }

  // get new filter state: rating
  let rating = newFilters.rating !== undefined ? Number(newFilters.rating) : undefined;

  if (typeof rating === 'number') {
    let rating_filter = [rating, '>='];
    newFilterState.rating = rating_filter;
  } else {
    newFilterState.rating = currentFilterReqs.rating;
  }

  // Final set state
  return newFilterState;
}

// Main function for API router:
//  Main goal for this router is to make an API call to get list of restaurants
//  and then send that list of restaurants into RestaurantSearchPage
function RecipeSearchRouter() {
  // Setting state
  const [errorFlag, setErrorFlag] = useState(false);
  const [filterCategories, setFilterCategories] = useState(null);

  // Setting API filter state
  const [apiState, setApiState] = useState({
    content: null,
    sortSetting: {
      attribute: 'name',
      ascending: true
    },
    activeFilters: {},
    currentPage: 1
  });

  // Get search params from URL
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = getSearchQuery(searchParams);

  // Get filters
  useEffect(() => {
    // Function to get filters via API call
    const getFilters = async () => {
      await fetch('https://api.feastforfriends.me/api/recipes/filters')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setFilterCategories(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (!errorFlag && filterCategories === null) {
      getFilters();
    }
  });

  // Get list of shallow recipes
  useEffect(() => {
    // Get all recipe data
    const getRecipes = async () => {
      // Pagination
      const currentPage = apiState.currentPage;

      // Sorting attributes
      const sortSettingAttribute = apiState.sortSetting.attribute;
      const isSortDescending = apiState.sortSetting.ascending ? '0' : '1';

      // Filtering attributes
      const dishTypeFilter = apiState.activeFilters.category || '';
      const cultureFilter = apiState.activeFilters.culture || '';

      let timeFilter = apiState.activeFilters.time;
      timeFilter = timeFilter === undefined ? 0.0 : timeFilter[0];

      let ratingFilter = apiState.activeFilters.rating;
      ratingFilter = ratingFilter === undefined ? 0 : ratingFilter[0];

      await fetch(
        'https://api.feastforfriends.me/api/recipes?sort_attribute=' +
          sortSettingAttribute +
          '&sort_desc=' +
          isSortDescending +
          '&dishType=' +
          dishTypeFilter +
          '&culture=' +
          cultureFilter +
          '&time=' +
          timeFilter +
          '&rating=' +
          ratingFilter +
          '&search_term=' +
          searchQuery +
          '&page_num=' +
          currentPage
      )
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          setApiState({
            content: data,
            sortSetting: apiState.sortSetting,
            activeFilters: apiState.activeFilters,
            currentPage: apiState.currentPage
          });
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Ensures that only run api calls once
    if (!errorFlag && filterCategories !== null) {
      getRecipes();
    }
  }, [
    apiState.activeFilters,
    apiState.currentPage,
    apiState.sortSetting,
    filterCategories,
    searchParams
  ]);

  // Check if still loading categories
  if (!errorFlag && filterCategories === null) {
    return <SearchPageSkeleton />;

    // Check if loading data
  } else if (!errorFlag && apiState.content === null) {
    return <SearchPageSkeleton />;

    // Error
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // On Sort setting change function
  let onSortSettingChange = (newSortAttribute) => {
    let newSortSetting = handleSortButtonClick(apiState.sortSetting, newSortAttribute);
    setApiState({
      content: null,
      sortSetting: newSortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: apiState.currentPage
    });
  };

  // If active filters change function
  let onActiveFiltersChange = (newFilters) => {
    let newFilterState = getNewFilterState(newFilters, apiState.activeFilters);

    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: newFilterState,
      currentPage: 1
    });
  };

  // Event handler function to handle pageChange
  let onPageChange = (newPage) => {
    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: newPage
    });
  };

  // State function to apply the new quey to the filters
  const setSearchQuery = (newQuery) => {
    setSearchParams({
      'search-bar-input': newQuery
    });
  };

  // Get num per page
  const numPerPage = NUM_INSTANCES_PER_PAGE;
  const recipes = apiState.content.data;
  const totalPages = apiState.content.num_pages;
  const totalInstances = apiState.content.results_total;

  // Process database to get filters relevant to database
  let categoryFiltersList = filterCategories.dishType;
  let cultureFiltersList = filterCategories.cuisine;
  let timeFiltersList = getNumericalFilters(TIME_START, TIME_STEP_SIZE, TIME_NUM_STEPS);
  let ratingFiltersList = getNumericalFilters(0, RATINGS_STEP_SIZE, RATING_NUM_STEPS);

  return (
    <RecipeSearchPage
      database={recipes}
      numPerPage={numPerPage}
      categoryFiltersList={categoryFiltersList}
      cultureFiltersList={cultureFiltersList}
      timeFiltersList={timeFiltersList}
      ratingFiltersList={ratingFiltersList}
      totalInstances={totalInstances}
      sortSetting={apiState.sortSetting}
      currentPage={apiState.currentPage}
      totalPages={totalPages}
      activeFilters={apiState.activeFilters}
      onSortSettingChange={onSortSettingChange}
      onPageChange={onPageChange}
      onActiveFiltersChange={onActiveFiltersChange}
      searchQuery={searchQuery}
      setSearchQuery={setSearchQuery}
    />
  );
}

export default RecipeSearchRouter;
