import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import IngredientSearchPage from '../../Components/ModelSearchPages/IngredientSearchPage';
import { getNumericalFilters } from '../../Components/ModelSearchPages/Filtering';
import { NUM_INSTANCES_PER_PAGE } from '../../Constants/ModelSearchConstants';
import { handleSortButtonClick } from '../../Components/ModelSearchPages/Sorting';
import { getSearchQuery } from '../../Components/ModelSearchPages/Searching';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import SearchPageSkeleton from '../../Components/Skeleton/SearchPageSkeleton/SearchPageSkeleton';

// Constants to help with filters
const PRICE_START_STEP = 8;
const PRICE_STEP_SIZE = -1;
const PRICE_NUM_STEPS = 7;

const CALORIES_START_STEP = 160;
const CALORIES_STEP_SIZE = -20;
const CALORIES_NUM_STEPS = 7;

const FAT_START_STEP = 15;
const FAT_STEP_SIZE = -3;
const FAT_NUM_STEPS = 4;

const PROTEIN_START_STEP = 15;
const PROTEIN_STEP_SIZE = -3;
const PROTEIN_NUM_STEPS = 4;

// Function to change filters
//  Pre: newFilters are the filters to be changed, currentFilterReqs are the current filters, setFilterReqs is the state function to change it
//  Post: Changes the set filters accordingly
function getNewFilterState(newFilters, currentFilterReqs) {
  let newFilterState = {};

  // get new filter state: price
  let price = newFilters.price !== undefined ? Number(newFilters.price) : undefined;

  if (typeof price === 'number') {
    newFilterState.price = [price, '<='];
  } else {
    newFilterState.price = currentFilterReqs.price;
  }

  // get new filter state: calories
  let calories = newFilters.calories !== undefined ? Number(newFilters.calories) : undefined;

  if (typeof calories === 'number') {
    newFilterState.calories = [calories, '<='];
  } else {
    newFilterState.calories = currentFilterReqs.calories;
  }

  // get new filter state: fat
  let fat = newFilters.fat !== undefined ? Number(newFilters.fat) : undefined;

  if (typeof fat === 'number') {
    newFilterState.fat = [fat, '<='];
  } else {
    newFilterState.fat = currentFilterReqs.fat;
  }

  // get new filter state: protein
  let protein = newFilters.protein !== undefined ? Number(newFilters.protein) : undefined;

  if (typeof protein === 'number') {
    newFilterState.protein = [protein, '<='];
  } else {
    newFilterState.protein = currentFilterReqs.protein;
  }

  // Set state
  return newFilterState;
}

// Main function for API router:
//  Main goal for this router is to make an API call to get list of ingredients
//  and then send that list of ingredients into IngredientSearchPage
function IngredientSearchRouter() {
  // Setting state
  const [errorFlag, setErrorFlag] = useState(false);

  // Setting API filter state
  const [apiState, setApiState] = useState({
    content: null,
    sortSetting: {
      attribute: 'title',
      ascending: true
    },
    activeFilters: {},
    currentPage: 1
  });

  // Get search params from URL
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = getSearchQuery(searchParams);

  // Get list of shallow ingredients
  useEffect(() => {
    const getIngredients = async () => {
      // Pagination
      const currentPage = apiState.currentPage;

      // Sorting attributes
      const sortSettingAttribute = apiState.sortSetting.attribute;
      const isSortDescending = apiState.sortSetting.ascending ? '0' : '1';

      // Filtering attributes
      let priceFilter = apiState.activeFilters.price;
      priceFilter = priceFilter === undefined ? 0.0 : priceFilter[0];

      let calorieFilter = apiState.activeFilters.calories;
      calorieFilter = calorieFilter === undefined ? 0 : calorieFilter[0];

      let fatFilter = apiState.activeFilters.fat;
      fatFilter = fatFilter === undefined ? 0.0 : fatFilter[0];

      let proteinFilter = apiState.activeFilters.protein;
      proteinFilter = proteinFilter === undefined ? 0 : proteinFilter[0];

      await fetch(
        'https://api.feastforfriends.me/api/ingredients?sort_attribute=' +
          sortSettingAttribute +
          '&sort_desc=' +
          isSortDescending +
          '&price=' +
          priceFilter +
          '&calories=' +
          calorieFilter +
          '&fat=' +
          fatFilter +
          '&protein=' +
          proteinFilter +
          '&search_term=' +
          searchQuery +
          '&page_num=' +
          currentPage
      )
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          setApiState({
            content: data,
            sortSetting: apiState.sortSetting,
            activeFilters: apiState.activeFilters,
            currentPage: apiState.currentPage
          });
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Ensures that only run api calls once
    if (!errorFlag) {
      getIngredients();
    }
  }, [apiState.activeFilters, apiState.currentPage, apiState.sortSetting, searchParams]);

  // Check if loading or error
  if (!errorFlag && apiState.content === null) {
    return <SearchPageSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // On Sort setting change function
  let onSortSettingChange = (newSortAttribute) => {
    let newSortSetting = handleSortButtonClick(apiState.sortSetting, newSortAttribute);
    setApiState({
      content: null,
      sortSetting: newSortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: apiState.currentPage
    });
  };

  // If active filters change function
  let onActiveFiltersChange = (newFilters) => {
    let newFilterState = getNewFilterState(newFilters, apiState.activeFilters);

    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: newFilterState,
      currentPage: 1
    });
  };

  // Event handler function to handle pageChange
  let onPageChange = (newPage) => {
    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: newPage
    });
  };

  // State function to apply the new quey to the filters
  const setSearchQuery = (newQuery) => {
    setSearchParams({
      'search-bar-input': newQuery
    });
  };

  // Get database from APIonSort
  const numPerPage = NUM_INSTANCES_PER_PAGE;
  const ingredients = apiState.content.data;
  const totalPages = apiState.content.num_pages;
  const totalInstances = apiState.content.results_total;

  // Dynamically get filters relevant to database
  let priceFiltersList = getNumericalFilters(PRICE_START_STEP, PRICE_STEP_SIZE, PRICE_NUM_STEPS);
  let caloriesFiltersList = getNumericalFilters(
    CALORIES_START_STEP,
    CALORIES_STEP_SIZE,
    CALORIES_NUM_STEPS
  );
  let fatFiltersList = getNumericalFilters(FAT_START_STEP, FAT_STEP_SIZE, FAT_NUM_STEPS);
  let proteinFiltersList = getNumericalFilters(
    PROTEIN_START_STEP,
    PROTEIN_STEP_SIZE,
    PROTEIN_NUM_STEPS
  );

  return (
    <IngredientSearchPage
      database={ingredients}
      numPerPage={numPerPage}
      priceFiltersList={priceFiltersList}
      caloriesFiltersList={caloriesFiltersList}
      fatFiltersList={fatFiltersList}
      proteinFiltersList={proteinFiltersList}
      totalInstances={totalInstances}
      totalPages={totalPages}
      sortSetting={apiState.sortSetting}
      currentPage={apiState.currentPage}
      activeFilters={apiState.activeFilters}
      onSortSettingChange={onSortSettingChange}
      onPageChange={onPageChange}
      onActiveFiltersChange={onActiveFiltersChange}
      searchQuery={searchQuery}
      setSearchQuery={setSearchQuery}
    />
  );
}

export default IngredientSearchRouter;
