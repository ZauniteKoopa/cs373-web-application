import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { getNumericalFilters } from '../../Components/ModelSearchPages/Filtering';
import CultureSearchPage from '../../Components/ModelSearchPages/CultureSearchPage';
import { NUM_INSTANCES_PER_PAGE } from '../../Constants/ModelSearchConstants';
import { handleSortButtonClick } from '../../Components/ModelSearchPages/Sorting';
import { getSearchQuery } from '../../Components/ModelSearchPages/Searching';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import SearchPageSkeleton from '../../Components/Skeleton/SearchPageSkeleton/SearchPageSkeleton';

const RECIPE_STEP_SIZE = 1;
const RECIPE_NUM_STEPS = 5;

const RESTAURANT_STEP_SIZE = 1;
const RESTAURANT_NUM_STEPS = 5;

// Function to change filters
//  Pre: newFilters are the filters to be changed, currentFilterReqs are the current filters, setFilterReqs is the state function to change it
//  Post: Changes the set filters accordingly
function getNewFilterState(newFilters, currentFilterReqs) {
  let newFilterState = {};

  // Get newFilterState's subregion
  newFilterState.subregion =
    newFilters.subregion === 'None'
      ? undefined
      : newFilters.subregion || currentFilterReqs.subregion;

  // Get newFilterState's language
  newFilterState.language =
    newFilters.language === 'None' ? undefined : newFilters.language || currentFilterReqs.language;

  // Get newFilterStates's num recipe connections
  let new_num_recipes =
    newFilters.num_recipe_connections !== undefined
      ? Number(newFilters.num_recipe_connections)
      : undefined;

  if (typeof new_num_recipes === 'number') {
    let recipe_connections_filter = [new_num_recipes, '>='];
    newFilterState.num_recipe_connections = recipe_connections_filter;
  } else {
    newFilterState.num_recipe_connections = currentFilterReqs.num_recipe_connections;
  }

  // Get newFilterState's num restaurant connections
  let new_num_restaurants =
    newFilters.num_restaurant_connections !== undefined
      ? Number(newFilters.num_restaurant_connections)
      : undefined;

  if (typeof new_num_restaurants === 'number') {
    let restaurants_connections_filter = [new_num_restaurants, '>='];
    newFilterState.num_restaurant_connections = restaurants_connections_filter;
  } else {
    newFilterState.num_restaurant_connections = currentFilterReqs.num_restaurant_connections;
  }

  // Return new filter state
  return newFilterState;
}

// Main function for API router:
//  Main goal for this router is to make an API call to get list of cultures and process list
//  and then send that list of cultures into CultureSearchPage
function CultureSearchRouter() {
  // Setting API result state
  const [errorFlag, setErrorFlag] = useState(false);
  const [filterCategories, setFilterCategories] = useState(null);

  // Setting API filter state
  const [apiState, setApiState] = useState({
    content: null,
    sortSetting: {
      attribute: 'name',
      ascending: true
    },
    activeFilters: {},
    currentPage: 1
  });

  // Get search params from URL
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = getSearchQuery(searchParams);

  // Get list of filters
  useEffect(() => {
    // Function to get filters via API call
    const getFilters = async () => {
      await fetch('https://api.feastforfriends.me/api/cultures/filters')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setFilterCategories(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (!errorFlag && filterCategories === null) {
      getFilters();
    }
  });

  // Get list of shallow recipes
  useEffect(() => {
    // Fetch all of the culture data
    const getCultures = async () => {
      // Pagination
      const currentPage = apiState.currentPage;

      // Sorting attributes
      const sortSettingAttribute = apiState.sortSetting.attribute;
      const isSortDescending = apiState.sortSetting.ascending ? '0' : '1';

      // Filter attributes
      const subregionFilter = apiState.activeFilters.subregion || '';
      const languageFilter = apiState.activeFilters.language || '';

      let numRecipesFilter = apiState.activeFilters.num_recipe_connections;
      numRecipesFilter = numRecipesFilter === undefined ? 0 : numRecipesFilter[0];

      let numRestaurantsFilter = apiState.activeFilters.num_restaurant_connections;
      numRestaurantsFilter = numRestaurantsFilter === undefined ? 0 : numRestaurantsFilter[0];

      await fetch(
        'https://api.feastforfriends.me/api/cultures?sort_attribute=' +
          sortSettingAttribute +
          '&sort_desc=' +
          isSortDescending +
          '&subregion=' +
          subregionFilter +
          '&language=' +
          languageFilter +
          '&num_recipe_connections=' +
          numRecipesFilter +
          '&num_restaurant_connections=' +
          numRestaurantsFilter +
          '&search_term=' +
          searchQuery +
          '&page_num=' +
          currentPage
      )
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          setApiState({
            content: data,
            sortSetting: apiState.sortSetting,
            activeFilters: apiState.activeFilters,
            currentPage: apiState.currentPage
          });
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Only run this effect when apiState changes or searchParam changes
    if (!errorFlag && filterCategories !== null) {
      getCultures();
    }
  }, [
    filterCategories,
    apiState.sortSetting,
    apiState.currentPage,
    apiState.activeFilters,
    searchParams
  ]);

  // Check if still loading categories
  if (!errorFlag && filterCategories === null) {
    // return <div>Loading filters</div>;
    return <SearchPageSkeleton />;

    // Check if loading data
  } else if (!errorFlag && apiState.content === null) {
    // return <div>Loading</div>;
    return <SearchPageSkeleton />;

    // Check for error
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // On Sort setting change function
  let onSortSettingChange = (newSortAttribute) => {
    let newSortSetting = handleSortButtonClick(apiState.sortSetting, newSortAttribute);
    setApiState({
      content: null,
      sortSetting: newSortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: apiState.currentPage
    });
  };

  // If active filters change function
  let onActiveFiltersChange = (newFilters) => {
    let newFilterState = getNewFilterState(newFilters, apiState.activeFilters);

    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: newFilterState,
      currentPage: 1
    });
  };

  // Event handler function to handle pageChange
  let onPageChange = (newPage) => {
    setApiState({
      content: null,
      sortSetting: apiState.sortSetting,
      activeFilters: apiState.activeFilters,
      currentPage: newPage
    });
  };

  // State function to apply the new quey to the filters
  const setSearchQuery = (newQuery) => {
    setSearchParams({
      'search-bar-input': newQuery
    });
  };

  // Get num pages and culture from API state content
  const numPerPage = NUM_INSTANCES_PER_PAGE;
  const totalPages = apiState.content.num_pages;
  const totalInstances = apiState.content.results_total;
  const cultures = apiState.content.data;

  // Process API call result to get static constant filters (if state of search page changes, don't have to recalculate this again)
  let subregionFiltersList = filterCategories.subRegion;
  let languageFiltersList = filterCategories.language;
  let numRecipeFiltersList = getNumericalFilters(0, RECIPE_STEP_SIZE, RECIPE_NUM_STEPS);
  let numRestaurantFiltersList = getNumericalFilters(0, RESTAURANT_STEP_SIZE, RESTAURANT_NUM_STEPS);

  return (
    <CultureSearchPage
      database={cultures}
      numPerPage={numPerPage}
      subregionFiltersList={subregionFiltersList}
      numRecipeFiltersList={numRecipeFiltersList}
      languageFiltersList={languageFiltersList}
      numRestaurantFiltersList={numRestaurantFiltersList}
      totalInstances={totalInstances}
      sortSetting={apiState.sortSetting}
      currentPage={apiState.currentPage}
      totalPages={totalPages}
      activeFilters={apiState.activeFilters}
      onSortSettingChange={onSortSettingChange}
      onPageChange={onPageChange}
      onActiveFiltersChange={onActiveFiltersChange}
      searchQuery={searchQuery}
      setSearchQuery={setSearchQuery}
    />
  );
}

export default CultureSearchRouter;
