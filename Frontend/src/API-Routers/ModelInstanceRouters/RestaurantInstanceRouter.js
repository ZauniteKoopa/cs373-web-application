import React, { useState, useEffect } from 'react';
import RestaurantInstancePage from '../../Components/ModelInstancePages/RestaurantInstance';
import { useParams } from 'react-router-dom';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import RestaurantSkeleton from '../../Components/Skeleton/InstancePageSkeleton/RestaurantSkeleton';

function RestaurantInstanceRouter() {
  const [restaurant, setRestaurant] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);
  let { id } = useParams();

  useEffect(() => {
    const getRestaurant = async () => {
      await fetch('https://api.feastforfriends.me/api/restaurants/' + id)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setRestaurant(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (!errorFlag && restaurant === null) {
      getRestaurant();
    }
  }, []);

  if (!errorFlag && restaurant === null) {
    return <RestaurantSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  let fieldCount = Object.keys(restaurant).length;
  if (fieldCount === 1) {
    return <ErrorPage />;
  }

  // Insert data into instance page
  return <RestaurantInstancePage restaurant={restaurant} />;
}

export default RestaurantInstanceRouter;
