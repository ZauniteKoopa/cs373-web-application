import React, { useState, useEffect } from 'react';
import RecipeInsatncePage from '../../Components/ModelInstancePages/RecipeInstance';
import { useParams } from 'react-router-dom';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import RecipeSkeleton from '../../Components/Skeleton/InstancePageSkeleton/RecipeSkeleton';

function RecipeInstanceRouter() {
  const [recipe, setRecipe] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);
  let { id } = useParams();

  useEffect(() => {
    const getRecipe = async () => {
      await fetch('https://api.feastforfriends.me/api/recipes/' + id)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setRecipe(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (!errorFlag && recipe === null) {
      getRecipe();
    }
  }, []);

  // Check state of the API call
  if (!errorFlag && recipe === null) {
    return <RecipeSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  let fieldCount = Object.keys(recipe).length;
  if (fieldCount === 1) {
    return <ErrorPage />;
  }

  return <RecipeInsatncePage recipe={recipe} />;
}

export default RecipeInstanceRouter;
