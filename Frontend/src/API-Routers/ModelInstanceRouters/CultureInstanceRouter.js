import React, { useState, useEffect } from 'react';
import CultureInstancePage from '../../Components/ModelInstancePages/CultureInstance';
import { useParams } from 'react-router-dom';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import CultureSkeleton from '../../Components/Skeleton/InstancePageSkeleton/CultureSkeleton';

function CultureInstanceRouter() {
  const [culture, setCulture] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);
  let { id } = useParams();

  useEffect(() => {
    const getCulture = async () => {
      await fetch('https://api.feastforfriends.me/api/cultures/' + id)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setCulture(data);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (!errorFlag && culture === null) {
      getCulture();
    }
  }, []);

  if (!errorFlag && culture === null) {
    return <CultureSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  let fieldCount = Object.keys(culture).length;
  if (fieldCount === 1) {
    return <ErrorPage />;
  }

  return <CultureInstancePage culture={culture} />;
}

export default CultureInstanceRouter;
