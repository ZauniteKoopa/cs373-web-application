import React, { useEffect, useState } from 'react';
import IngredientInsatncePage from '../../Components/ModelInstancePages/IngredientInstance';
import { useParams } from 'react-router-dom';
import ErrorPage from '../../Components/NotFoundError/NotFound';
import IngredientSkeleton from '../../Components/Skeleton/InstancePageSkeleton/IngredientSkeleton';

function IngredientInstanceRouter() {
  // Set the state of the API call
  const [ingredient, setIngredient] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);
  let { id } = useParams();

  // Get ingredient using API call
  useEffect(() => {
    const getIngredient = async () => {
      await fetch('https://api.feastforfriends.me/api/ingredients/' + id)
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          console.log(data);
          setIngredient(data);
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };
    // getIngredient();
    if (!errorFlag && ingredient === null) {
      getIngredient();
    }
  }, []);

  if (!errorFlag && ingredient === null) {
    return <IngredientSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  let fieldCount = Object.keys(ingredient).length;
  if (fieldCount === 1) {
    return <ErrorPage />;
  }

  return <IngredientInsatncePage ingredient={ingredient} />;
}

export default IngredientInstanceRouter;
