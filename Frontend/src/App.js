import './App.css';
import React, { Component, useEffect } from 'react';
import { Routes, Route, Outlet, Link } from 'react-router-dom';
import Nav from './Components/Nav/Nav';
import { Col, Row, Container, Button } from 'react-bootstrap';
import AboutPage from './Components/AboutPage/AboutPage';
import Footer from './Components/Footer/Footer';
import NotFoundError from './Components/NotFoundError/NotFound';

// The 4 model instance pages routers
import CultureInstanceRouter from './API-Routers/ModelInstanceRouters/CultureInstanceRouter';
import RecipeInstanceRouter from './API-Routers/ModelInstanceRouters/RecipeInstanceRouter';
import RestaurantInstanceRouter from './API-Routers/ModelInstanceRouters/RestaurantInstanceRouter';
import IngredientInstanceRouter from './API-Routers/ModelInstanceRouters/IngredientInstanceRouter';

// The 4 model search pages routers
import CultureSearchRouter from './API-Routers/ModelSearchRouters/CultureSearchRouter';
import RecipeSearchRouter from './API-Routers/ModelSearchRouters/RecipeSearchRouter';
import RestaurantSearchRouter from './API-Routers/ModelSearchRouters/RestaurantSearchRouter';
import IngredientSearchRouter from './API-Routers/ModelSearchRouters/IngredientSearchRouter';

import GlobalSearch from './Components/GlobalSearch/GlobalSearch';

// Data visualizations
import VisualizationPage from './Components/Visualizations/VisualizationPage';
import GerryMapVisualizations from './Components/Visualizations/GerryMapVisualizations';

// Aos package imports
import Aos from 'aos';
import 'aos/dist/aos.css';

// Overall App component for the entire website
class App extends Component {
  // Overall render function
  render() {
    // Render return: uses react-router to route pages based off of the current URL
    return (
      <div className="App">
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="Home" element={<Home />} />
            <Route path="About" element={<AboutPage />} />
            <Route path="Cultures" element={<CultureSearchRouter />} />
            <Route path="Restaurants" element={<RestaurantSearchRouter />} />
            <Route path="Recipes" element={<RecipeSearchRouter />} />
            <Route path="Ingredients" element={<IngredientSearchRouter />} />
            <Route path="Cultures/:id" element={<CultureInstanceRouter />} />
            <Route path="Recipes/:id" element={<RecipeInstanceRouter />} />
            <Route path="Restaurants/:id" element={<RestaurantInstanceRouter />} />
            <Route path="Ingredients/:id" element={<IngredientInstanceRouter />} />
            <Route path="Search" element={<GlobalSearch />} />
            <Route path="Visualizations" element={<VisualizationPage />} />
            <Route path="GerryVisualizations" element={<GerryMapVisualizations />} />
            <Route path="*" element={<NotFoundError />}></Route>
          </Route>
        </Routes>
      </div>
    );
  }
}

function Layout() {
  useEffect(() => {
    Aos.init({ duration: 1500 });
  }, []);
  return (
    <div className="App">
      <Nav />
      <Outlet />
    </div>
  );
}

const Home = () => {
  return (
    <body>
      <section id="landing">
        <Container>
          <Row>
            <Col id="title" data-aos="fade-up">
              <div id="landingTitle">{"Let's Start Cooking with Popular Recipes"}</div>
              <div className="landingSubtitle">
                <div>Want to learn to cook but confused how to start?</div>
                <div>No need to worry again!</div>
              </div>
              <div className="buttons">
                <Link to={'/Recipes'}>
                  <Button id="primary-btn">Discover Recipes</Button>
                </Link>
                <Link to={'/Restaurants'}>
                  <Button id="sec-btn" variant="outline-success">
                    Find Restaurants
                  </Button>
                </Link>
              </div>
            </Col>
            <Col data-aos="fade-left">
              <img id="plate" src="/plate.jpg" alt="homepage plate"></img>
            </Col>
          </Row>
        </Container>
      </section>

      <section id="homeSectionTwo">
        <Container>
          <Row>
            <Col id="flyingCake" data-aos="fade-right">
              <img id="sectionTwoImg" src="/cakes.jpg" alt="flying cakes" align="left"></img>
            </Col>

            <Col>
              <div id="sectionTwoTitle" data-aos="fade-down">
                <p>
                  {"Sometimes, you've got to "}
                  <span id="break-phrase">break</span> the rules!
                </p>
              </div>
              <div className="buttons second-section">
                <Link to={'/Cultures'}>
                  <Button id="primary-btn">Explore Cultures</Button>
                </Link>
                <Link to={'/Ingredients'}>
                  <Button id="sec-btn" variant="outline-success">
                    Discover Ingredients
                  </Button>
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </body>
  );
};

export default App;
