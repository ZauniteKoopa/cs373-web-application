import React, { useState, useEffect } from 'react';
import ErrorPage from '../../NotFoundError/NotFound';
import StateCompactnessSkeleton from '../../Skeleton/VisualizationSkeleton/GerryMap/StateCompactnessSkeleton';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Cell,
  Label,
  Legend,
  ResponsiveContainer
} from 'recharts';

// Constant colors
const REPUBLICAN_COLOR = '#ff0000';
const DEMOCRAT_COLOR = '#0000ff';
const INDEP_COLOR = '#ABA000';

// Main function to process Api Data
//  Pre: rawData is an array of element with state_abbrev and district_compactness
//  Post: Returns a list of states with average compactness, sorted in ascending order
async function processApiData(rawData) {
  let compactnessMap = {};

  // Get political alignment of all states
  const stateMap = await fetch('https://api.gerrymap.com/states?page=1&page_size=500')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let stateMap = {};
      let states = data.data;

      for (let i = 0; i < states.length; i++) {
        let curState = states[i];
        stateMap[curState.state_abbrev] = curState.party_detailed;
      }

      return stateMap;
    });

  // Iterate through all districts and calculate average compactness of districts
  for (let i = 0; i < rawData.length; i++) {
    const curDistrict = rawData[i];
    let curState = curDistrict.state_abbrev;

    // If map undefined, do another API call to get the state
    if (compactnessMap[curState] === undefined) {
      // Set object
      compactnessMap[curState] = {
        totalCompactness: 0,
        numDistricts: 0,
        party: stateMap[curState]
      };
    }

    // Update values
    compactnessMap[curState].totalCompactness += curDistrict.district_compactness;
    compactnessMap[curState].numDistricts += 1;
  }

  // Create processed data
  let compactnessList = [];
  for (const [key, value] of Object.entries(compactnessMap)) {
    // Get party color to color the bar
    let partyColor = undefined;

    if (value.party === 'REPUBLICAN') {
      partyColor = REPUBLICAN_COLOR;
    } else if (value.party === 'DEMOCRAT') {
      partyColor = DEMOCRAT_COLOR;
    } else {
      partyColor = INDEP_COLOR;
    }

    // Set current state object and push it in
    let curState = {
      name: key,
      'Average Compactness': Math.round((value.totalCompactness / value.numDistricts) * 100) / 100,
      partyColor: partyColor
    };

    compactnessList.push(curState);
  }

  // Sort list
  compactnessList.sort((a, b) => {
    return b['Average Compactness'] - a['Average Compactness'];
  });

  return compactnessList;
}

// Main function to render the chart and handle API call
function GerryCompactnessProcessor() {
  // Set state
  const [compactnessData, setCompactnessData] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Use effect to make API call to GerryMap
  useEffect(() => {
    // Function to get culture data
    const getDistrictData = async () => {
      // Get API call to fetch all districts
      const apiResponse = await fetch('https://api.gerrymap.com/districts?page=1&page_size=500')
        .then((response) => {
          return response.json();
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });

      // Asynchronous call to get the data
      await processApiData(apiResponse.data)
        .then((processedData) => {
          setCompactnessData(processedData);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Makes sure it only runs once
    if (!errorFlag && compactnessData == null) {
      getDistrictData();
    }
  });

  // Function to get loading screen or error screen for this data
  if (!errorFlag && compactnessData == null) {
    return <StateCompactnessSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // Main return
  return (
    <ResponsiveContainer width="100%" id="bar-charts">
      <BarChart
        data={compactnessData}
        margin={{
          top: 5,
          right: 30,
          left: 30,
          bottom: 15
        }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name">
          <Label
            style={{
              textAnchor: 'middle',
              fontSize: '130%'
            }}
            value={'State'}
            position={'insideBottom'}
            offset={-14}
          />
        </XAxis>
        <YAxis>
          <Label
            style={{
              textAnchor: 'middle',
              fontSize: '130%'
            }}
            angle={270}
            value={'Compactness Score'}
            position={'insideLeft'}
          />
        </YAxis>
        <Tooltip />
        <Bar dataKey="Average Compactness">
          {compactnessData.map((entry, index) => {
            return <Cell key={index} fill={entry.partyColor} />;
          })}
        </Bar>

        <Legend
          // margin={{ top: 20, left: 20, right: 20, bottom: 20 }}
          height={40}
          payload={[
            {
              id: 'Democrat',
              value: 'Democrat',
              type: 'square',
              color: DEMOCRAT_COLOR
            },
            {
              id: 'Republican',
              value: 'Republican',
              type: 'square',
              color: REPUBLICAN_COLOR
            },
            {
              id: 'Independent',
              value: 'Independent',
              type: 'square',
              color: INDEP_COLOR
            }
          ]}
          verticalAlign="top"
          layout="horizontal"
          align="right"
        />
      </BarChart>
    </ResponsiveContainer>
  );
}

export default GerryCompactnessProcessor;
