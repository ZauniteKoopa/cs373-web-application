import React, { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import PieChartDisplay from '../Charts/PieChart';
import ErrorPage from '../../NotFoundError/NotFound';
import DemographicsSkeleton from '../../Skeleton/VisualizationSkeleton/GerryMap/DemographicsSkeleton';

// Main function to just get starting demoPartyData
//  Returns an object with 2 parties, each with an array with the following set index
//      0 - white, 1 - Hispanic, 2 - Asian, 3 - Black, 4 - Other
function getStartingDemoPartyData() {
  return {
    DEMOCRAT: [
      {
        name: 'White',
        value: 0
      },
      {
        name: 'Hispanic',
        value: 0
      },
      {
        name: 'Asian',
        value: 0
      },
      {
        name: 'Black',
        value: 0
      }
    ],
    REPUBLICAN: [
      {
        name: 'White',
        value: 0
      },
      {
        name: 'Hispanic',
        value: 0
      },
      {
        name: 'Asian',
        value: 0
      },
      {
        name: 'Black',
        value: 0
      }
    ]
  };
}

// Function to convert numberical API data to percentage data in which all numbers add to 100
//  Pre: partyData is an array that contained all the demographic data in the format given in getStartingDemoPartyData
//  Post: converts all numbers into percentages
function convertToPercentages(partyData) {
  // Get total population first
  let total_pop = 0;
  for (let i = 0; i < partyData.length; i++) {
    total_pop += partyData[i].value;
  }

  // Converts all values to percentages afterwards
  for (let i = 0; i < partyData.length; i++) {
    let newValue = (partyData[i].value / total_pop) * 100;
    newValue = Math.round(newValue * 100) / 100;
    partyData[i].value = newValue;
  }

  return partyData;
}

// Main function to process the API data
//  Pre: rawData is an array in which each element has the following attributes:
//      asian_pop, black_pop, hispanic_pop, total_pop, white_pop, party_detailed
//  Post: return an object that has 2 elements: democrat and republican, and each element holds demographic data
function processApiData(rawData) {
  let demoPartyData = getStartingDemoPartyData();

  // Iterate through each district and update demoPartyData
  for (let i = 0; i < rawData.length; i++) {
    let curDistrict = rawData[i];

    // Get statistics immeidately
    let { asian_pop, black_pop, hispanic_pop, white_pop, party_detailed } = curDistrict;
    let curParty = demoPartyData[party_detailed];

    // Update demographic stats
    curParty[0].value += white_pop;
    curParty[1].value += hispanic_pop;
    curParty[2].value += asian_pop;
    curParty[3].value += black_pop;
  }

  demoPartyData['DEMOCRAT'] = convertToPercentages(demoPartyData['DEMOCRAT']);
  demoPartyData['REPUBLICAN'] = convertToPercentages(demoPartyData['REPUBLICAN']);

  return demoPartyData;
}

// Main function to render the charts
function GerryDemographics() {
  // Set state
  const [demoPartyData, setDemoPartyData] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Use effect to make API call to GerryMap
  useEffect(() => {
    // Function to get culture data
    const getDistrictData = async () => {
      // Get API call to fetch all districts
      await fetch('https://api.gerrymap.com/districts?page=1&page_size=500')
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          let processedData = processApiData(data.data);
          setDemoPartyData(processedData);
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Makes sure it only runs once
    if (!errorFlag && demoPartyData == null) {
      getDistrictData();
    }
  });

  // Function to get loading screen or error screen for this data
  if (!errorFlag && demoPartyData == null) {
    return <DemographicsSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // Main return
  return (
    <>
      <br />
      <br />
      <Row>
        <Col>
          <h4 id="gerry-demo-subtitle">Democrat Districts</h4>
          <PieChartDisplay data={demoPartyData['DEMOCRAT']} dataKey={'value'} />
        </Col>
        <Col>
          <h4 id="gerry-demo-subtitle">Republican Districts</h4>
          <PieChartDisplay data={demoPartyData['REPUBLICAN']} dataKey={'value'} />
        </Col>
      </Row>
    </>
  );
}

export default GerryDemographics;
