import React, { useState, useEffect } from 'react';
import ErrorPage from '../../NotFoundError/NotFound';
import CandidateVoter from '../../Skeleton/VisualizationSkeleton/GerryMap/CandidateVoter';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Label,
  ResponsiveContainer
} from 'recharts';

// Process API data
function processApiData(data) {
  let dataPoints = [];

  for (let i = 0; i < data.length; i++) {
    let curDistrict = data[i];
    let compactness = curDistrict.district_compactness;
    let candidateRatio = curDistrict.candidatevotes / curDistrict.totalvotes;

    let curPoint = {
      compactness: compactness,
      candidateRatio: candidateRatio
    };

    dataPoints.push(curPoint);
  }

  return dataPoints;
}

// Main render function
function GerryScatter() {
  // Set state
  const [compRatioPairs, setCompRatioPairs] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Use effect to make API call to GerryMap
  useEffect(() => {
    // Function to get culture data
    const getDistrictData = async () => {
      // Get API call to fetch all districts
      await fetch('https://api.gerrymap.com/districts?page=1&page_size=500')
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          let processedData = processApiData(data.data);
          setCompRatioPairs(processedData);
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Makes sure it only runs once
    if (!errorFlag && compRatioPairs == null) {
      getDistrictData();
    }
  });

  // Function to get loading screen or error screen for this data
  if (!errorFlag && compRatioPairs == null) {
    return <CandidateVoter />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  return (
    <ResponsiveContainer id="gerry-scatter">
      <ScatterChart>
        <CartesianGrid />
        <XAxis type="number" dataKey="compactness" height={40}>
          <Label
            style={{
              textAnchor: 'middle',
              fontSize: '120%'
            }}
            value={'Compactness Score'}
            position={'bottom'}
            offset={-10}
          />
        </XAxis>
        <YAxis type="number" dataKey="candidateRatio">
          <Label
            style={{
              textAnchor: 'middle',
              fontSize: '120%'
            }}
            angle={270}
            value={'Candidate Vote Ratio'}
            position={'insideLeft'}
          />
        </YAxis>
        <Scatter data={compRatioPairs} fill="green" />
      </ScatterChart>
    </ResponsiveContainer>
  );
}

export default GerryScatter;
