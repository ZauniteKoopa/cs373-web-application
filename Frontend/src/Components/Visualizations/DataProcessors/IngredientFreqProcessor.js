import React, { useState, useEffect } from 'react';
import BarChartDisplay from '../Charts/BarChart';
import ErrorPage from '../../NotFoundError/NotFound';
import IngredientsBargraphSkeleton from '../../Skeleton/VisualizationSkeleton/FeastForFriends/IngredientsBargraphSkeleton';

const TOP_NUMBER_INGREDIENTS = 10;
const Y_AXIS_LABEL = 'Num Recipes Using';

// Main function to process api data
function processApiData(ingredientData, topNumber) {
  let ingredientMap = {};

  // Go through each
  for (let i = 0; i < ingredientData.length; i++) {
    let curIngredient = ingredientData[i];
    ingredientMap[curIngredient.name] = parseInt(curIngredient.num_recipe_connections);
  }

  // Go through each subregion and make a processed data array
  let processedData = [];
  for (const [key, value] of Object.entries(ingredientMap)) {
    let curIngredient = {};
    curIngredient['name'] = key;
    curIngredient[Y_AXIS_LABEL] = value;

    processedData.push(curIngredient);
  }

  // Sort based off of recipes using number in desceding order
  processedData.sort((a, b) => {
    return b[Y_AXIS_LABEL] - a[Y_AXIS_LABEL];
  });

  processedData = processedData.slice(0, Math.min(topNumber, processedData.length));

  return processedData;
}

// Main function to render data
function IngredientFreqProcessor() {
  // set state
  const [ingredientFreq, setIngredientFreq] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Main effect to get ingredient API and process it
  useEffect(() => {
    // Function to get API
    const getIngredientData = async () => {
      fetch('https://api.feastforfriends.me/api/ingredients?per_page=200')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          let processedData = processApiData(data.data, TOP_NUMBER_INGREDIENTS);
          setIngredientFreq(processedData);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    if (ingredientFreq === null && !errorFlag) {
      getIngredientData();
    }
  });

  // Check if it's still loading or it error
  if (ingredientFreq === null && !errorFlag) {
    return <IngredientsBargraphSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  return <BarChartDisplay data={ingredientFreq} dataKey="Num Recipes Using" />;
}

export default IngredientFreqProcessor;
