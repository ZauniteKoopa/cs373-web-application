import React, { useState, useEffect } from 'react';
import PieChartDisplay from '../Charts/PieChart';
import ErrorPage from '../../NotFoundError/NotFound';
import SubregionPiechartSkeleton from '../../Skeleton/VisualizationSkeleton/FeastForFriends/SubregionPiechartSkeleton';
import '../Visualizations.css';

// Main function to process api data
//  Pre: apiData is an array with at least sub region, num_recipe_connections, and num_restaurant_connection
//  Post: Returns an array that has the name of a subregion and the number of instances connected to it
function processApiData(apiData) {
  // Set up mapping between subregion and instances
  let subregionMap = {};

  // Go through apiData and process it
  for (let i = 0; i < apiData.length; i++) {
    let curCulture = apiData[i];

    // Check if subregion was counted, if not create new object
    if (subregionMap[curCulture.subregion] === undefined) {
      subregionMap[curCulture.subregion] = 0;
    }

    subregionMap[curCulture.subregion] +=
      curCulture.num_recipe_connections + curCulture.num_restaurant_connections;
  }

  // Go through each subregion and make a processed data array
  let processedData = [];
  for (const [key, value] of Object.entries(subregionMap)) {
    let curRegion = {
      name: key,
      numInstances: value
    };

    processedData.push(curRegion);
  }

  return processedData;
}

// Main function to process the data for making the culture pie
function CulturePieProcessor() {
  // Set state
  const [pieData, setPieData] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Use effect to make culture API call
  useEffect(() => {
    // Function to get culture data
    const getCultureData = async () => {
      // Get API call
      await fetch('https://api.feastforfriends.me/api/cultures?per_page=200')
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          let processedData = processApiData(data.data);
          setPieData(processedData);
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Makes sure it only runs once
    if (!errorFlag && pieData == null) {
      getCultureData();
    }
  });

  // Function to get loading screen or error screen for this data
  if (!errorFlag && pieData == null) {
    return <SubregionPiechartSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // Main return
  return (
    <PieChartDisplay
      data={pieData}
      dataKey={'numInstances'}
      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
    />
  );
}

export default CulturePieProcessor;
