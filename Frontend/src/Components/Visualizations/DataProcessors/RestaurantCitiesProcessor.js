import React, { useState, useEffect } from 'react';
import TexasBubbleMap from '../Charts/TexasBubbleMap';
import ErrorPage from '../../NotFoundError/NotFound';
import RestaurantDistSkeleton from '../../Skeleton/VisualizationSkeleton/FeastForFriends/RestaurantDistSkeleton';

// Main function to process data
function processApiData(rawData) {
  // Create dictionary of city mapped to instance
  let cityMap = {};

  for (const curIndex in rawData) {
    const curRestaurant = rawData[curIndex];
    let curLocation = curRestaurant.location;

    if (cityMap[curLocation] === undefined) {
      cityMap[curLocation] = 0;
    }

    cityMap[curLocation]++;
  }

  // Convert dictionary into an array to be passed down
  let cityData = [];
  for (const [key, value] of Object.entries(cityMap)) {
    let curCity = {
      cityName: key,
      numRestaurants: value
    };

    cityData.push(curCity);
  }

  return cityData;
}

// Main processor for processing data
function RestaurantCitiesProcessor() {
  // Set state
  const [cityData, setCityData] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

  // Use effect to make culture API call
  useEffect(() => {
    // Function to get culture data
    const getCultureData = async () => {
      // Get API call
      await fetch('https://api.feastforfriends.me/api/restaurants?per_page=200')
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          let processedData = processApiData(data.data);
          setCityData(processedData);
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Makes sure it only runs once
    if (!errorFlag && cityData == null) {
      getCultureData();
    }
  });

  // Function to get loading screen or error screen for this data
  if (!errorFlag && cityData == null) {
    return <RestaurantDistSkeleton />;
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // Main return
  return <TexasBubbleMap cityData={cityData} />;
}

export default RestaurantCitiesProcessor;
