import React from 'react';
import { MapContainer, CircleMarker, TileLayer, Tooltip } from 'react-leaflet';
import { TexasCities } from './TexasCities';
import PropTypes from 'prop-types';
import '../Visualizations.css';
import 'leaflet/dist/leaflet.css';

// Main render function
function TexasBubbleMap(props) {
  //Austin coordinates
  const cityLong = -97.7431;
  const cityLat = 30.2672;

  // City Test data
  const { cityData } = props;

  // Constants
  const maxRadius = 25;
  const minRadius = 4;
  const maxValue = 35;

  return (
    <div>
      <MapContainer
        id="bubble-map"
        zoom={5}
        center={[cityLat, cityLong]}
        scrollWheelZoom={false}
        zoomControl={false}
        dragging={false}
        className="visualization">
        <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        {cityData.map((entry, index) => {
          let cityCoords = TexasCities[entry.cityName];

          if (cityCoords === undefined) {
            return null;
          } else {
            return (
              <CircleMarker
                key={index}
                center={[cityCoords.lat, cityCoords.long]}
                radius={Math.max(maxRadius * (entry.numRestaurants / maxValue), minRadius)}>
                <Tooltip>
                  {entry.cityName + ': ' + entry.numRestaurants + ' Restaurants Found'}
                </Tooltip>
              </CircleMarker>
            );
          }
        })}
      </MapContainer>
    </div>
  );
}

export default TexasBubbleMap;

TexasBubbleMap.propTypes = {
  cityData: PropTypes.arrayOf(
    PropTypes.shape({
      cityName: PropTypes.string,
      numRestaurants: PropTypes.number
    })
  )
};
