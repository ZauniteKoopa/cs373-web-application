// Main class to have city constants
export const TexasCities = {
  Austin: {
    lat: 30.2672,
    long: -97.7431
  },
  Houston: {
    lat: 29.7604,
    long: -95.3698
  },
  Dallas: {
    lat: 32.7767,
    long: -96.797
  },
  'San Antonio': {
    lat: 29.4252,
    long: -98.4946
  },
  Plano: {
    lat: 33.0198,
    long: -96.6989
  },
  Spring: {
    lat: 30.0799,
    long: -95.4172
  },
  West: {
    lat: 31.8023,
    long: -97.091
  },
  Carrollton: {
    lat: 32.9756,
    long: -96.89
  },
  Frisco: {
    lat: 33.1507,
    long: -96.8236
  },
  Cypress: {
    lat: 29.9717,
    long: -95.6938
  },
  'North Richland Hills': {
    lat: 32.8343,
    long: -97.2289
  },
  Pflugerville: {
    lat: 30.4548,
    long: -97.6223
  },
  Boerne: {
    lat: 29.7947,
    long: -98.732
  },
  'Leon Valley': {
    lat: 29.4952,
    long: -98.6186
  },
  'Farmers Branch': {
    lat: 32.9265,
    long: -96.8961
  },
  Addison: {
    lat: 32.9618,
    long: -96.8292
  },
  Leander: {
    lat: 30.5788,
    long: -97.8531
  }
};
