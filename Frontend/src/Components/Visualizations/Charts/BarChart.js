import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts';
import PropTypes from 'prop-types';

function BarChartDisplay(props) {
  const data = props.data;

  return (
    <ResponsiveContainer width="100%" id="bar-charts">
      <BarChart
        align="center"
        data={data}
        style={{ textTransform: 'capitalize' }}
        margin={{
          top: 5,
          right: 30,
          left: 0,
          bottom: 5
        }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey={props.dataKey} fill="#8884d8" />
      </BarChart>
    </ResponsiveContainer>
  );
}

export default BarChartDisplay;

BarChartDisplay.propTypes = {
  data: PropTypes.array,
  dataKey: PropTypes.string
};
