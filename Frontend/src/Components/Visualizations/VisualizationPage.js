import React from 'react';
import { Container, Row } from 'react-bootstrap';
import CulturePieProcessor from './DataProcessors/CulturePieProcessor';
import IngredientFreqProcessor from './DataProcessors/IngredientFreqProcessor';
import RestaurantCitiesProcessor from './DataProcessors/RestaurantCitiesProcessor';
import './Visualizations.css';

function VisualizationPage() {
  return (
    <Container>
      <Row>
        <h2 id="visualization-title">Data Visualizations for Feast For Friends</h2>
      </Row>

      <Row>
        <div className="visualization">
          <h3 id="visualization-subtitle">
            Sub-Region Representation in Site (Num Recipes and Restaurants connected)
          </h3>
          <CulturePieProcessor />
        </div>
      </Row>
      <br />
      <br />

      <Row>
        <h3 id="visualization-subtitle"> Top 10 Most Used Ingredients </h3>
      </Row>

      <Row>
        <div className="visualization">
          <IngredientFreqProcessor />
        </div>
      </Row>

      <br />
      <br />

      <Row>
        <h3 id="visualization-subtitle">Feast for Friends Restaurant Distribution in Texas</h3>
      </Row>

      <Row>
        <RestaurantCitiesProcessor />
      </Row>

      <br />
      <br />
    </Container>
  );
}

export default VisualizationPage;
