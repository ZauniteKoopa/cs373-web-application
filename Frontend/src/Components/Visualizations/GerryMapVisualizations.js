import React from 'react';
import { Container, Row } from 'react-bootstrap';
import GerryDemographics from './GerryMapProcessors/GerryDemographics';
import GerryCompactnessProcessor from './GerryMapProcessors/GerryCompactnessProcessor';
import GerryScatter from './GerryMapProcessors/GerryScatter';
import './Visualizations.css';

// Main render function
function GerryMapVisualizations() {
  return (
    <Container>
      <h2 id="visualization-title"> Data Visualizations for Gerry Map </h2>
      <Row>
        <h3 id="visualization-subtitle">
          {' '}
          Average Demographic Distribution of Congressional Districts{' '}
        </h3>
      </Row>
      <GerryDemographics />

      <br />
      <br />

      <Row>
        <h3 id="visualization-subtitle"> Average District Compactness Per State </h3>
      </Row>
      <Row>
        <GerryCompactnessProcessor />
      </Row>

      <br />
      <br />

      <Row>
        <h3 id="visualization-subtitle"> District Compactness on Candidate Voter Ratio </h3>
      </Row>
      <Row>
        <GerryScatter />
      </Row>
      <br />
      <br />
    </Container>
  );
}

export default GerryMapVisualizations;
