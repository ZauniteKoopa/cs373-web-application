import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Lottie from 'react-lottie';
import animationData from '../../assets/lottie/errorPerson.json';
import './NotFound.css';

const NotFoundError = function () {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <section>
      <div id="error-lottie">
        <Lottie
          options={defaultOptions}
          isClickToPauseDisabled={true}
          style={{ cursor: 'default' }}
        />
      </div>
      <p id="error-title">Whoops! Lost Looking for Food?</p>
      <p className="error-sub">The page you&apos;re looking for isn&apos;t found</p>
      <p className="error-sub">We suggest you go back to home</p>
      <Link to={'/Home'}>
        <Button id="error-home-btn">Back to Home</Button>
      </Link>
    </section>
  );
};

export default NotFoundError;
