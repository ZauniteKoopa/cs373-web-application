import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table, Form, Button, Container, Row, Col } from 'react-bootstrap';
import PaginationElement from './Pagination';
import { getSortButtonIcon } from './Sorting';
import { highlightTerms } from './Searching';
import './SearchPage.css';

const MAX_VALUE = 7000000;

//
// Functional component for culture search page: index is the ID
//
function IngredientSearchPage(props) {
  // set up filters and Get search params if there is any
  const { priceFiltersList, caloriesFiltersList, fatFiltersList, proteinFiltersList } = props;
  let searchQuery = props.searchQuery;

  // Get database and set up state
  const ingredientDB = props.database;
  const [tempQuery, setTempQuery] = useState(searchQuery);
  const sortSetting = props.sortSetting;
  const filterRequirements = props.activeFilters;
  const currentPage = props.currentPage;

  const navigate = useNavigate();

  const handleClick = (id) => {
    navigate('' + id);
  };

  // Main return
  return (
    <Container>
      <br />
      <h2 id="search-model-title" name="num-instances">
        {' '}
        {props.totalInstances} Ingredients Found{' '}
      </h2>
      <br />

      <Row>
        <Col sm={10}>
          <Form className="search-bar">
            <Form.Control
              type="text"
              placeholder="Search for an ingredient directly!"
              className="search-bar-text"
              name="search-bar-input"
              value={tempQuery}
              onChange={(event) => setTempQuery(event.target.value)}
            />
          </Form>
        </Col>
        <Col sm={2} className="submit-button">
          <Button
            id="submit-button"
            className="generic-primary-btn"
            variant="primary"
            type="submit"
            name="search-bar-submit"
            onClick={() => props.setSearchQuery(tempQuery)}>
            Submit
          </Button>
        </Col>
      </Row>
      <br />
      <br />

      <Row>
        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Price: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.price !== undefined ? filterRequirements.price[0] : MAX_VALUE
              }
              onChange={(event) => props.onActiveFiltersChange({ price: event.target.value })}>
              <option value="None" disabled>
                Price
              </option>
              <option value={MAX_VALUE}>All</option>
              {priceFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<= $' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Calories: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.calories !== undefined
                  ? filterRequirements.calories[0]
                  : MAX_VALUE
              }
              onChange={(event) => props.onActiveFiltersChange({ calories: event.target.value })}>
              <option value="None" disabled>
                Calories
              </option>
              <option value={MAX_VALUE}>All</option>
              {caloriesFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<= ' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Fat: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={filterRequirements.fat !== undefined ? filterRequirements.fat[0] : MAX_VALUE}
              onChange={(event) => props.onActiveFiltersChange({ fat: event.target.value })}>
              <option value="None" disabled>
                Fat (g)
              </option>
              <option value={MAX_VALUE}>All</option>
              {fatFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<= ' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Protein: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.protein !== undefined ? filterRequirements.protein[0] : MAX_VALUE
              }
              onChange={(event) => props.onActiveFiltersChange({ protein: event.target.value })}>
              <option value="None" disabled>
                Protein (g)
              </option>
              <option value={MAX_VALUE}>All</option>
              {proteinFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<= ' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>
      </Row>

      <br />
      <br />

      <Table responsive borderless>
        <thead>
          <td id="left-end">
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('title')}>
              {getSortButtonIcon(sortSetting, 'title')}
            </Button>
            Ingredient
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('price')}>
              {getSortButtonIcon(sortSetting, 'price')}
            </Button>
            Price ($)
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('calories')}>
              {getSortButtonIcon(sortSetting, 'calories')}
            </Button>
            Calories
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('fat')}>
              {getSortButtonIcon(sortSetting, 'fat')}
            </Button>
            Fat (g)
          </td>
          <td id="right-end">
            {' '}
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('protein')}>
              {getSortButtonIcon(sortSetting, 'protein')}
            </Button>
            Protein (g)
          </td>
        </thead>
        <tbody>
          {ingredientDB.map((item, index) => {
            return (
              <tr
                id="search-page-table-row"
                key={'Culture' + index}
                onClick={() => {
                  handleClick(item.id);
                }}>
                <td>{highlightTerms(item.title, searchQuery)}</td>
                <td>{'$' + item.price.toFixed(2)}</td>
                <td>{item.calories}</td>
                <td>{item.fat + 'g'}</td>
                <td>{item.protein + 'g'}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <br />
      <PaginationElement
        totalPages={props.totalPages}
        currentPage={currentPage}
        setPage={props.onPageChange}
      />
      <br />
    </Container>
  );
}

// Prop types
IngredientSearchPage.propTypes = {
  database: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      price: PropTypes.number,
      calories: PropTypes.number,
      fat: PropTypes.number,
      protein: PropTypes.number
    })
  ),
  numPerPage: PropTypes.number,
  priceFiltersList: PropTypes.arrayOf(PropTypes.number),
  caloriesFiltersList: PropTypes.arrayOf(PropTypes.number),
  fatFiltersList: PropTypes.arrayOf(PropTypes.number),
  proteinFiltersList: PropTypes.arrayOf(PropTypes.number),
  onSortSettingChange: PropTypes.func,
  sortSetting: PropTypes.shape({
    attribute: PropTypes.string,
    ascending: PropTypes.bool
  }),
  onPageChange: PropTypes.func,
  onActiveFiltersChange: PropTypes.func,
  currentPage: PropTypes.number,
  activeFilters: PropTypes.object,
  totalInstances: PropTypes.number,
  searchQuery: PropTypes.string,
  setSearchQuery: PropTypes.func,
  totalPages: PropTypes.number
};

export default IngredientSearchPage;
