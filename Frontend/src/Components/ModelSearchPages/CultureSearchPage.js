import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table, Form, Button, Container, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import PaginationElement from './Pagination';
import { getSortButtonIcon } from './Sorting';
import { highlightTerms } from './Searching';
import './SearchPage.css';

// Functional component for culture search page: index is the ID
function CultureSearchPage(props) {
  // Get search query from URL
  let searchQuery = props.searchQuery;

  // Get database and filters and set up state functions
  const cultureDB = props.database;
  const {
    subregionFiltersList,
    numRecipeFiltersList,
    languageFiltersList,
    numRestaurantFiltersList
  } = props;

  const [tempQuery, setTempQuery] = useState(searchQuery);
  const sortSetting = props.sortSetting;
  const filterRequirements = props.activeFilters;
  const currentPage = props.currentPage;

  const navigate = useNavigate();

  const handleClick = (id) => {
    navigate('' + id);
  };

  // Main return
  return (
    <Container>
      <br />
      <h2 id="search-model-title" name="num-instances">
        {' '}
        {props.totalInstances} Cultures Found{' '}
      </h2>
      <br />

      <Row>
        <Col sm={10}>
          <Form className="search-bar">
            <Form.Control
              type="text"
              placeholder="Search for a culture directly!"
              className="search-bar-text"
              name="search-bar-input"
              value={tempQuery}
              onChange={(event) => setTempQuery(event.target.value)}
            />
          </Form>
        </Col>
        <Col sm={2} className="submit-button">
          <Button
            id="submit-button"
            className="generic-primary-btn"
            variant="primary"
            type="submit"
            name="search-bar-submit"
            onClick={() => props.setSearchQuery(tempQuery)}>
            Submit
          </Button>
        </Col>
      </Row>
      <br />
      <br />

      <Row>
        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Sub-Region: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.subregion !== undefined ? filterRequirements.subregion : 'None'
              }
              onChange={(event) => props.onActiveFiltersChange({ subregion: event.target.value })}>
              <option value="None" disabled>
                Culture
              </option>
              <option value="None">All</option>
              {subregionFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Language:</Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.language !== undefined ? filterRequirements.language : 'None'
              }
              onChange={(event) => props.onActiveFiltersChange({ language: event.target.value })}>
              <option value="None" disabled>
                Language
              </option>
              <option value="None">All</option>
              {languageFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Recipes Count: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.num_recipe_connections !== undefined
                  ? filterRequirements.num_recipe_connections[0]
                  : 0
              }
              onChange={(event) =>
                props.onActiveFiltersChange({ num_recipe_connections: event.target.value })
              }>
              <option value={0} disabled>
                Recipes #
              </option>
              {numRecipeFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'>=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Restaurants Count: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.num_restaurant_connections !== undefined
                  ? filterRequirements.num_restaurant_connections[0]
                  : 0
              }
              onChange={(event) =>
                props.onActiveFiltersChange({ num_restaurant_connections: event.target.value })
              }>
              <option value={0} disabled>
                Restaurant #
              </option>
              {numRestaurantFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'>=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>
      </Row>

      <br />
      <br />

      <Table responsive borderless>
        <thead>
          <td id="left-end">
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('name')}>
              {getSortButtonIcon(sortSetting, 'name')}
            </Button>
            Culture
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('subRegion')}>
              {getSortButtonIcon(sortSetting, 'subRegion')}
            </Button>
            Sub-Region
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('language')}>
              {getSortButtonIcon(sortSetting, 'language')}
            </Button>
            Language
          </td>
          <td>
            <Button
              className="sortin-btn"
              onClick={() => props.onSortSettingChange('num_recipe_connections')}>
              {getSortButtonIcon(sortSetting, 'num_recipe_connections')}
            </Button>
            Recipes Count
          </td>
          <td id="right-end">
            {' '}
            <Button
              className="sortin-btn"
              onClick={() => props.onSortSettingChange('num_restaurant_connections')}>
              {getSortButtonIcon(sortSetting, 'num_restaurant_connections')}
            </Button>
            Restaurants Count
          </td>
        </thead>
        <tbody>
          {cultureDB.map((item, index) => {
            return (
              <tr
                id="search-page-table-row"
                key={'Culture' + index}
                onClick={() => {
                  handleClick(item.id);
                }}>
                <td>{highlightTerms(item.name, searchQuery)}</td>
                <td>{highlightTerms(item.subregion, searchQuery)}</td>
                <td>{highlightTerms(item.language, searchQuery)}</td>
                <td>{item.num_recipe_connections}</td>
                <td>{item.num_restaurant_connections}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <br />
      <PaginationElement
        totalPages={props.totalPages}
        currentPage={currentPage}
        setPage={props.onPageChange}
      />
      <br />
    </Container>
  );
}

// Expected prop-types (subject to change)
CultureSearchPage.propTypes = {
  database: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      subregion: PropTypes.string,
      num_restaurant_connections: PropTypes.number,
      language: PropTypes.string,
      num_recipe_connections: PropTypes.number
    })
  ),
  numPerPage: PropTypes.number,
  subregionFiltersList: PropTypes.arrayOf(PropTypes.string),
  numRecipeFiltersList: PropTypes.arrayOf(PropTypes.number),
  languageFiltersList: PropTypes.arrayOf(PropTypes.string),
  numRestaurantFiltersList: PropTypes.arrayOf(PropTypes.number),
  onSortSettingChange: PropTypes.func,
  sortSetting: PropTypes.shape({
    attribute: PropTypes.string,
    ascending: PropTypes.bool
  }),
  onPageChange: PropTypes.func,
  onActiveFiltersChange: PropTypes.func,
  currentPage: PropTypes.number,
  activeFilters: PropTypes.object,
  totalInstances: PropTypes.number,
  searchQuery: PropTypes.string,
  setSearchQuery: PropTypes.func,
  totalPages: PropTypes.number
};

export default CultureSearchPage;
