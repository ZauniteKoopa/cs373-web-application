// Main function to get valid filter categories from object O(N + ClogC), N (number of instances) >= C (number of categories)
//  Pre: filteredAttribute must be a string AND database must be an array of objects containing that string
//  Post: Returns a sorted list of attribute values that can be used to filter the database
export function getValidTextFilters(filteredAttribute, database) {
  console.assert(typeof filteredAttribute == 'string' && Array.isArray(database));

  // Set of filters that can be used
  let filterSet = new Set();
  let filterList = [];

  // Go through each database and look at the possible filters
  for (const dataInstance of database) {
    console.assert(dataInstance[filteredAttribute] !== undefined);

    if (!filterSet.has(dataInstance[filteredAttribute])) {
      filterSet.add(dataInstance[filteredAttribute]);
    }
  }

  // Go through the filters and add them to a sorted list
  filterSet.forEach((value) => {
    filterList.push(value);
  });

  // Sort filter list and then return
  filterList.sort();

  return filterList;
}

// Main function to get model connection attribute
//  Pre: filteredAttribute is a string representing connection.name, database is an array of objects with a model connection object
//  Post: return a sorted list of connections filters
export function getValidConnectionFilters(filteredAttribute, database) {
  console.assert(typeof filteredAttribute == 'string' && Array.isArray(database));

  // Set of filters that can be used
  let filterSet = new Set();
  let filterList = [];

  // Go through each database and look at the possible filters
  for (const dataInstance of database) {
    console.assert(dataInstance[filteredAttribute].name !== undefined);
    let connectionName = dataInstance[filteredAttribute].name;

    if (!filterSet.has(connectionName)) {
      filterSet.add(connectionName);
    }
  }

  // Go through the filters and add them to a sorted list
  filterSet.forEach((value) => {
    filterList.push(value);
  });

  // Sort filter list and then return
  filterList.sort();

  return filterList;
}

// Main function to get evenly spaced numerical filters automatically O(S), S = num step (usually O(1))
//  Pre: numSteps > 0
//  Post: returns an array from beginNumber to (beginNumber + numSteps * stepSize) in steps
export function getNumericalFilters(beginNumber, stepSize, numSteps) {
  console.assert(numSteps > 0);

  let filterList = [beginNumber];
  let curNumber = beginNumber;

  for (let i = 0; i < numSteps; i++) {
    curNumber += stepSize;
    filterList.push(curNumber);
  }

  console.assert(
    filterList.length > 0 &&
      filterList[0] == beginNumber &&
      filterList[filterList.length - 1] == beginNumber + stepSize * numSteps
  );
  return filterList;
}

// Main function to say whether or not a model instance fulfills filter requirement
//  Pre: a row representing model instance and filterReqs containing string or number properties as filters
//  Post: true if row fulfills filter requirements, false if it doesn't
export function filterInstance(modelInstance, filterRequirements) {
  // Iterate over all required attributes in filterRequirements object
  for (const attribute in filterRequirements) {
    let attributeField = filterRequirements[attribute];

    // Consider string case and numerical case
    if (typeof attributeField === 'string') {
      // If string doesn't match, just return false
      if (modelInstance[attribute] !== attributeField) {
        return false;
      }
    } else if (typeof attributeField !== 'undefined') {
      // Numerical comparisons is an array with 2 objects in attribute field: [0] -> number, [1] -> '>=' OR '<='
      //  >= will keep any number >= to value[0]
      //  <= will keep any number <= to value[0]
      if (attributeField[1] === '>=' && modelInstance[attribute] < attributeField[0]) {
        return false;
      } else if (attributeField[1] === '<=' && modelInstance[attribute] > attributeField[0]) {
        return false;
      }
    }
  }

  return true;
}
