import React from 'react';
import Highlighter from 'react-highlight-words';

// Extremely simple implementation of filter function for individual instance
//  Pre: instance is a model instance, searchQuery is a string, attributeList is a list of strings representing attributes in instance
//  Post: returns a boolean that says whether or not an instance stays in the array after filter
function filterInstanceBySearch(instance, searchQuery, attributeList) {
  // If search query is empty, just return true for all
  if (searchQuery === '') {
    return true;
  }

  // Split the search query into a list of words to search
  let searchTerms = searchQuery.split(' ');

  // For each attribute that is searchable, check if instance contains one of the search terms
  for (let a = 0; a < attributeList.length; a++) {
    let currentAttribute = attributeList[a];
    let instanceAttribute = instance[currentAttribute].toLowerCase();

    for (let w = 0; w < searchTerms.length; w++) {
      let currentSearchTerm = searchTerms[w].toLowerCase();
      if (instanceAttribute.includes(currentSearchTerm)) {
        return true;
      }
    }
  }

  // If not found
  return false;
}

// General function used to filterBySearch (can be enhanced)
//  Pre: database is array of instances to filter from, searchQuery is string used to filter, attributeList is list of attributes to consider in instance
//  Post: returns an array filtered by the searchQuery
export function filterBySearch(database, searchQuery, attributeList) {
  let filteredDatabase = database.filter((instance) =>
    filterInstanceBySearch(instance, searchQuery, attributeList)
  );
  return filteredDatabase;
}

// General function used to highlight terms directly
//  Pre: textToHighlight: the string you want to process, searchQuery: query containing words that are important
//  Post: Return the html element with the highlighted terms
export function highlightTerms(textToHighlight, searchQuery) {
  let searchTerms = searchQuery.split(' ');
  return (
    <Highlighter
      highlightClassName="highlightedSearchTerms"
      searchWords={searchTerms}
      autoEscape={true}
      textToHighlight={textToHighlight}
    />
  );
}

// General function used to get search query from URL
//  Pre: searchParams are the search paramters found in the site's url, obtained via useSearchParams() in react-router-dom
//  Post: returns a string representing the search query
export function getSearchQuery(searchParams) {
  let searchQuery = searchParams.get('search-bar-input');
  if (searchQuery === null || searchQuery === undefined) {
    searchQuery = '';
  }

  return searchQuery;
}
