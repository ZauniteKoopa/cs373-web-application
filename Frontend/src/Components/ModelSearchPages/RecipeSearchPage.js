import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table, Form, Button, Container, Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap';
import PropTypes from 'prop-types';
import PaginationElement from './Pagination';
import { getSortButtonIcon } from './Sorting';
import { highlightTerms } from './Searching';
import './SearchPage.css';

// Constants for calculation
const MIN_PER_HOUR = 60;
const MAX_RECIPE_TIME = 360000000;

// Functional component for culture search page: index is the ID
function RecipeSearchPage(props) {
  // Get search query from URL
  let searchQuery = props.searchQuery;

  // Get database and set up state functions
  const recipeDB = props.database;
  const { categoryFiltersList, cultureFiltersList, timeFiltersList, ratingFiltersList } = props;
  const [tempQuery, setTempQuery] = useState(searchQuery);
  const sortSetting = props.sortSetting;
  const filterRequirements = props.activeFilters;
  const currentPage = props.currentPage;

  const navigate = useNavigate();

  const handleClick = (id) => {
    navigate('' + id);
  };

  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {'Yelp recipe review'}
    </Tooltip>
  );

  // Main html to return
  return (
    <Container>
      <br />
      <h2 id="search-model-title" name="num-instances">
        {' '}
        {props.totalInstances} Recipes Found{' '}
      </h2>
      <br />
      <Row>
        <Col sm={10}>
          <Form className="search-bar">
            <Form.Control
              type="text"
              placeholder="Search for a recipe directly!"
              className="search-bar-text"
              name="search-bar-input"
              value={tempQuery}
              onChange={(event) => setTempQuery(event.target.value)}
            />
          </Form>
        </Col>
        <Col sm={2} className="submit-button">
          <Button
            id="submit-button"
            className="generic-primary-btn"
            variant="primary"
            type="submit"
            name="search-bar-submit"
            onClick={() => props.setSearchQuery(tempQuery)}>
            Submit
          </Button>
        </Col>
      </Row>

      <br />
      <br />

      <Row>
        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Category: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.category !== undefined ? filterRequirements.category : 'None'
              }
              onChange={(event) => props.onActiveFiltersChange({ category: event.target.value })}>
              <option value="None" disabled>
                Category
              </option>
              <option value="None">All</option>
              {categoryFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Culture: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={filterRequirements.culture !== undefined ? filterRequirements.culture : 'None'}
              onChange={(event) => props.onActiveFiltersChange({ culture: event.target.value })}>
              <option value="None" disabled>
                Culture
              </option>
              <option value="None">All</option>
              {cultureFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Time (Hours): </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={
                filterRequirements.time !== undefined
                  ? filterRequirements.time[0] / MIN_PER_HOUR
                  : MAX_RECIPE_TIME
              }
              onChange={(event) => props.onActiveFiltersChange({ time: event.target.value })}>
              <option value="" disabled>
                Time
              </option>
              <option value={MAX_RECIPE_TIME} key={-1}>
                All
              </option>
              {timeFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Rating: </Form.Label>
            <Form.Select
              className="search-filter-select"
              value={filterRequirements.rating !== undefined ? filterRequirements.rating[0] : 0}
              onChange={(event) => props.onActiveFiltersChange({ rating: event.target.value })}>
              <option value="None" disabled>
                Rating
              </option>
              {ratingFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'>=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>
      </Row>

      <br />
      <br />

      <Table responsive borderless>
        <thead>
          <td id="left-end">
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('name')}>
              {getSortButtonIcon(sortSetting, 'name')}
            </Button>
            Recipe
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('dishType')}>
              {getSortButtonIcon(sortSetting, 'dishType')}
            </Button>
            Category
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('cuisine')}>
              {getSortButtonIcon(sortSetting, 'cuisine')}
            </Button>
            Culture
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('cookTime')}>
              {getSortButtonIcon(sortSetting, 'cookTime')}
            </Button>
            Time
          </td>
          <td id="right-end">
            {' '}
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('rating')}>
              {getSortButtonIcon(sortSetting, 'rating')}
            </Button>
            <OverlayTrigger placement="top" overlay={renderTooltip} delay={25}>
              <a>Rating</a>
            </OverlayTrigger>
          </td>
        </thead>
        <tbody>
          {recipeDB.map((item, index) => {
            let recipeHours = Math.floor(item.time / MIN_PER_HOUR);
            let recipeMinutes = item.time % MIN_PER_HOUR;
            let recipeTimeString = recipeHours + ' hrs, ' + recipeMinutes + ' min';
            return (
              <tr
                id="search-page-table-row"
                key={'Culture' + index}
                onClick={() => {
                  handleClick(item.id);
                }}>
                <td>{highlightTerms(item.name, searchQuery)}</td>
                <td id="recipe-category">{highlightTerms(item.category, searchQuery)}</td>
                <td>{highlightTerms(item.culture, searchQuery)}</td>
                <td>{recipeTimeString}</td>
                <td>{item.rating + ' / 5'}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <br />
      <PaginationElement
        totalPages={props.totalPages}
        currentPage={currentPage}
        setPage={props.onPageChange}
      />
      <br />
    </Container>
  );
}

// Expected prop-types (subject to change)
RecipeSearchPage.propTypes = {
  database: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      category: PropTypes.string,
      culture: PropTypes.string,
      difficulty: PropTypes.number,
      rating: PropTypes.number
    })
  ),
  numPerPage: PropTypes.number,
  categoryFiltersList: PropTypes.arrayOf(PropTypes.string),
  cultureFiltersList: PropTypes.arrayOf(PropTypes.string),
  timeFiltersList: PropTypes.arrayOf(PropTypes.number),
  ratingFiltersList: PropTypes.arrayOf(PropTypes.number),
  onSortSettingChange: PropTypes.func,
  sortSetting: PropTypes.shape({
    attribute: PropTypes.string,
    ascending: PropTypes.bool
  }),
  onPageChange: PropTypes.func,
  onActiveFiltersChange: PropTypes.func,
  currentPage: PropTypes.number,
  activeFilters: PropTypes.object,
  totalInstances: PropTypes.number,
  searchQuery: PropTypes.string,
  setSearchQuery: PropTypes.func,
  totalPages: PropTypes.number
};

export default RecipeSearchPage;
