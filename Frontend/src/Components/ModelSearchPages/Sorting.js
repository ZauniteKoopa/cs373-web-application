// Main function to compare rows - used for sorting
//  Pre: rowA and row B are instances of this model, attribute is the attribute you're sorting by, ascending is a bool
//  Post: Returns an int. If negative, sort a before b. Positive, sort b before a. 0, keep original order
export function compareRows(rowA, rowB, attribute, ascending) {
  let originalSort = 0;

  if (typeof rowA[attribute] === 'string') {
    let stringA = rowA[attribute].toLowerCase();
    let stringB = rowB[attribute].toLowerCase();
    originalSort = stringA.localeCompare(stringB);
  } else {
    originalSort = rowA[attribute] - rowB[attribute];
  }

  return ascending ? originalSort : originalSort * -1;
}

// Main functional component to handle sort button activity
//  Pre: sortSetting and setSortSetting correspond to component state and currentAttribute is a string rep. sorting attribute of elements
//  Post: Returns the new sort setting
export function handleSortButtonClick(sortSetting, currentAttribute) {
  let newAttribute = currentAttribute;
  let newAscending = newAttribute === sortSetting.attribute ? !sortSetting.ascending : true;

  return {
    attribute: newAttribute,
    ascending: newAscending
  };
}

// Main function for determining button layout
//  Pre: sortSetting is the state corresponding to component state and button attribute correspond to attribute to sort
//  Post: Returns the button icon that should be shown for this sort button
export function getSortButtonIcon(sortSetting, buttonAttribute) {
  if (sortSetting.attribute !== buttonAttribute) {
    return '\u2012';
  } else {
    return sortSetting.ascending ? '\u02C4' : '\u02C5';
  }
}
