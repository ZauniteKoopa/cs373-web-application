import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Table, Form, Button, Container, Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap';
import PaginationElement from './Pagination';
import { getSortButtonIcon } from './Sorting';
import PropTypes from 'prop-types';
import { highlightTerms } from './Searching';
import './SearchPage.css';

// Functional component for culture search page: index is the ID
function RestaurantSearchPage(props) {
  // Get search query from URL
  let searchQuery = props.searchQuery;

  // Get database and filters from props and set state
  const restaurantDB = props.database;
  const { cultureFiltersList, locationFiltersList, costFiltersList, ratingFiltersList } = props;

  const [tempQuery, setTempQuery] = useState(searchQuery);
  const sortSetting = props.sortSetting;
  const filterRequirements = props.activeFilters;
  const currentPage = props.currentPage;

  const navigate = useNavigate();

  const handleClick = (id) => {
    navigate('' + id);
  };

  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {'Yelp restaurant review'}
    </Tooltip>
  );

  const renderTooltipCost = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {'Cost rating where 3 is most expensive'}
    </Tooltip>
  );

  // Main HTML to return
  return (
    <Container>
      <br />
      <h2 id="search-model-title" name="num-instances">
        {' '}
        {props.totalInstances} Restaurants Found{' '}
      </h2>
      <br />
      <Row>
        <Col sm={10}>
          <Form className="search-bar">
            <Form.Control
              type="text"
              placeholder="Search for a restaurant directly!"
              className="search-bar-text"
              name="search-bar-input"
              value={tempQuery}
              onChange={(event) => setTempQuery(event.target.value)}
            />
          </Form>
        </Col>
        <Col sm={2} className="submit-button">
          <Button
            id="submit-button"
            className="generic-primary-btn"
            variant="primary"
            type="submit"
            name="search-bar-submit"
            onClick={() => props.setSearchQuery(tempQuery)}>
            Submit
          </Button>
        </Col>
      </Row>
      <br />
      <br />

      <Row>
        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Culture: </Form.Label>
            <Form.Select
              className="search-filter-select"
              onChange={(event) => props.onActiveFiltersChange({ culture: event.target.value })}
              value={
                filterRequirements.culture !== undefined ? filterRequirements.culture : 'None'
              }>
              <option value="None" disabled>
                Culture
              </option>
              <option value="None">All</option>
              {cultureFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Location: </Form.Label>
            <Form.Select
              className="search-filter-select"
              onChange={(event) => props.onActiveFiltersChange({ location: event.target.value })}
              value={
                filterRequirements.location !== undefined ? filterRequirements.location : 'None'
              }>
              <option value="None" disabled>
                Location
              </option>
              <option value="None">All</option>
              {locationFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Cost: </Form.Label>
            <Form.Select
              className="search-filter-select"
              onChange={(event) => props.onActiveFiltersChange({ cost: event.target.value })}
              value={filterRequirements.cost !== undefined ? filterRequirements.cost[0] : 0}>
              <option value="None" disabled>
                Cost
              </option>
              {costFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'<=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>

        <Col>
          <Form className="search-filter">
            <Form.Label className="search-filter-label">Rating: </Form.Label>
            <Form.Select
              className="search-filter-select"
              onChange={(event) => props.onActiveFiltersChange({ rating: event.target.value })}
              value={filterRequirements.rating !== undefined ? filterRequirements.rating[0] : 0}>
              <option value="None" disabled>
                Rating
              </option>
              {ratingFiltersList.map((item, index) => {
                return (
                  <option value={item} key={index}>
                    {'>=' + item}
                  </option>
                );
              })}
            </Form.Select>
          </Form>
        </Col>
      </Row>

      <br />
      <br />

      <Table responsive borderless>
        <thead>
          <td id="left-end">
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('name')}>
              {getSortButtonIcon(sortSetting, 'name')}
            </Button>
            Restaurant
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('culture')}>
              {getSortButtonIcon(sortSetting, 'culture')}
            </Button>
            Culture
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('city')}>
              {getSortButtonIcon(sortSetting, 'city')}
            </Button>
            Location
          </td>
          <td>
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('cost')}>
              {getSortButtonIcon(sortSetting, 'cost')}
            </Button>
            <OverlayTrigger placement="top" overlay={renderTooltipCost} delay={25}>
              <a>Cost</a>
            </OverlayTrigger>
          </td>
          <td id="right-end">
            {' '}
            <Button className="sortin-btn" onClick={() => props.onSortSettingChange('rating')}>
              {getSortButtonIcon(sortSetting, 'rating')}
            </Button>
            <OverlayTrigger placement="top" overlay={renderTooltip} delay={25}>
              <a>Rating</a>
            </OverlayTrigger>
          </td>
        </thead>
        <tbody>
          {restaurantDB.map((item, index) => {
            return (
              <tr
                id="search-page-table-row"
                key={'Culture' + index}
                onClick={() => {
                  handleClick(item.id);
                }}>
                <td>{highlightTerms(item.name, searchQuery)}</td>
                <td>{highlightTerms(item.culture, searchQuery)}</td>
                <td>{highlightTerms(item.location, searchQuery)}</td>
                <td>{item.cost + ' / 3'}</td>
                <td>{item.rating + ' / 5'}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <br />
      <PaginationElement
        totalPages={props.totalPages}
        currentPage={currentPage}
        setPage={props.onPageChange}
      />
      <br />
    </Container>
  );
}

// Expected prop-types (subject to change)
RestaurantSearchPage.propTypes = {
  database: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      culture: PropTypes.string,
      cost: PropTypes.number,
      rating: PropTypes.number,
      location: PropTypes.string
    })
  ),
  numPerPage: PropTypes.number,
  cultureFiltersList: PropTypes.arrayOf(PropTypes.string),
  locationFiltersList: PropTypes.arrayOf(PropTypes.string),
  costFiltersList: PropTypes.arrayOf(PropTypes.number),
  ratingFiltersList: PropTypes.arrayOf(PropTypes.number),
  onSortSettingChange: PropTypes.func,
  sortSetting: PropTypes.shape({
    attribute: PropTypes.string,
    ascending: PropTypes.bool
  }),
  onPageChange: PropTypes.func,
  onActiveFiltersChange: PropTypes.func,
  currentPage: PropTypes.number,
  activeFilters: PropTypes.object,
  totalInstances: PropTypes.number,
  searchQuery: PropTypes.string,
  setSearchQuery: PropTypes.func,
  totalPages: PropTypes.number
};

export default RestaurantSearchPage;
