import React from 'react';
import Pagination from '@mui/material/Pagination';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import PropTypes from 'prop-types';

// Main function to get pagination elements
//  Pre: Takes in an array of shallow object instances that the website has access, currentPage > 0, numPerPage > 0, setPage is an event handler for when page changes
//  Post: returns an object with the following, an array of the displayed instances in order and the pagination HTML element at the bottom
function PaginationElement(props) {
  let { totalPages, currentPage, setPage } = props;

  // Pre-condition checks
  console.assert(currentPage > 0 && totalPages > 0);
  console.assert(setPage !== undefined);

  // helper function that handles page change
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  // pagination button styles
  const theme = createTheme({
    components: {
      MuiPagination: {
        styleOverrides: {
          root: {
            justifyContent: 'center',
            display: 'flex'
          },
          ul: {
            '& .Mui-selected': {
              backgroundColor: '#6c7ae0'
            }
          }
        }
      }
    },
    palette: {
      secondary: {
        main: '#6c7ae0',
        darker: '#4F61EB'
      }
    }
  });

  // Return object with HTML pagination element and showing instances
  return (
    <ThemeProvider theme={theme}>
      <Pagination
        count={totalPages}
        onChange={handlePageChange}
        page={currentPage}
        color="secondary"
      />
    </ThemeProvider>
  );
}

PaginationElement.propTypes = {
  totalPages: PropTypes.number,
  currentPage: PropTypes.number,
  setPage: PropTypes.func
};

export default PaginationElement;
