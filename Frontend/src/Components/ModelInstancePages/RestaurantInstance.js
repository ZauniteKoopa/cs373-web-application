import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Container, Table } from 'react-bootstrap';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import { Rating } from '@mui/material';
import { GiPositionMarker } from 'react-icons/gi';
import { styled } from '@mui/material/styles';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

// Main render function for the RestaurantInstance Component
function RestaurantInstance(props) {
  const currentRestaurant = props.restaurant;

  // Function to get a marker
  const Marker = () => (
    <GiPositionMarker
      style={{
        display: 'inline-flex',
        padding: '15px 15px',
        fontSize: '75px',
        color: 'rgb(110, 94, 254)',
        justifyContent: 'center',
        transform: 'translate(-50%, -75%)'
      }}></GiPositionMarker>
  );

  // Variable to get location of restaurant
  const center = {
    lat: currentRestaurant.mapLatitude,
    lng: currentRestaurant.mapLongitude
  };

  //Get attributes as strings
  let deliverableString = currentRestaurant.deliverable ? 'YES' : 'NO';

  // Get culture link
  let cultureLink =
    currentRestaurant.culture.id === null ? (
      <div>{currentRestaurant.culture.name}</div>
    ) : (
      <Link className="links" to={'/Cultures/' + currentRestaurant.culture.id}>
        {currentRestaurant.culture.name}
      </Link>
    );

  const StyledRating = styled(Rating)({
    '& .MuiRating-iconFilled': {
      color: '#18AC7B'
    },
    '& .MuiRating-iconHover': {
      color: '#18AC7B'
    }
  });

  return (
    <Container>
      <Row>
        <Col id="restaurantInfo">
          <p id="restName">{currentRestaurant.name}</p>
          <h4 id="instance-culture">Culture: {cultureLink}</h4>
          <span id="rest-info">
            <i className="bi bi-geo-alt-fill" id="restInfoIcons"></i> {currentRestaurant.address}
            <i id="separator" style={{ padding: '0 15px 0 15px' }}>
              |
            </i>
            <i className="bi bi-telephone-fill" id="restInfoIcons"></i> {currentRestaurant.phone}
          </span>
          <p id="rest-description">
            <i className="bi bi-chat-right-quote-fill" id="restInfoIcons"></i>{' '}
            {currentRestaurant.description}
          </p>
          <p id="rest-description">
            <i className="bi bi-globe2" id="restInfoIcons"></i> Visit the{' '}
            <a className="links" href={currentRestaurant.website} target="_blank" rel="noreferrer">
              {"restaurant's Yelp page"}
            </a>
          </p>
        </Col>
      </Row>
      <Row>
        <Col />
        <Col />
        <Col>
          <p style={{ fontFamily: 'Poppins' }}>
            {'Customers Rating'}
            <div>
              <Rating name="read-only" value={currentRestaurant.rating} precision={0.5} readOnly />
            </div>
          </p>
        </Col>
        <Col>
          <p style={{ fontFamily: 'Poppins' }}>
            {'Overall Cost'}
            <div>
              <StyledRating
                name="read-only"
                value={currentRestaurant.cost}
                getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
                precision={0.5}
                max={3}
                readOnly
                icon={<AttachMoneyIcon fontSize="inherit" />}
                emptyIcon={<AttachMoneyIcon fontSize="inherit" />}
              />
            </div>
          </p>
        </Col>
        <Col />
        <Col />
      </Row>
      <Row md={12} id="restImg">
        <Col md={6} id="map">
          <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyBMOeKQDczzf0UiatJpZvyr53IJtmMDlHo' }}
            defaultCenter={center}
            defaultZoom={15}
            options={{
              styles: [
                { featureType: 'all', elementType: 'labels', stylers: [{ visibility: 'on' }] }
              ]
            }}>
            <Marker lat={center.lat} lng={center.lng} />
          </GoogleMapReact>
        </Col>
        <Col md={6}>
          <img src={currentRestaurant.image} alt="error loading" id="restaurantImg"></img>
        </Col>
      </Row>
      <hr className="mb-5" />
      <Row>
        <h4 id="rest-attr"> Attributes </h4>
        <br />
        <br />
        <Table borderless responsive>
          <thead>
            <td colSpan={2} id="single-row-header">
              Services and Features
            </td>
          </thead>
          <tbody>
            <tr>
              <td style={{ width: '50%' }}>Deliverable?</td>
              <td>{deliverableString}</td>
            </tr>
          </tbody>
        </Table>
      </Row>
      <hr className="mb-5" />
      <Row>
        <Col md={6} id="rest-review">
          <div id="rest-review-border">
            <div>
              <p id="ing-desc-title">Customer Review</p>
              <p id="ing-desc">{currentRestaurant.review1}</p>
            </div>
          </div>
        </Col>
        <Col md={6} id="rest-review">
          <div id="rest-review-border">
            <div>
              <p id="ing-desc-title">Customer Review</p>
              <p id="ing-desc">{currentRestaurant.review2}</p>
            </div>
          </div>
        </Col>
        <Col md={6} id="rest-review">
          <div id="rest-review-border">
            <div>
              <p id="ing-desc-title">Customer Review</p>
              <p id="ing-desc">{currentRestaurant.review3}</p>
            </div>
          </div>
        </Col>
      </Row>
      <hr className="mb-5" />
      <Row>
        <div id="culture-links-title">
          <h4>Related Recipes: </h4>
        </div>
        <ul id="culture-links">
          {currentRestaurant.recipe_connections.map((item, index) => {
            return (
              <li style={{ textAlign: 'left' }} key={index}>
                <Link className="links" to={'/Recipes/' + item.id}>
                  {item.name}
                </Link>
              </li>
            );
          })}
        </ul>
      </Row>
      <hr className="mb-5" />
    </Container>
  );
}

RestaurantInstance.propTypes = {
  restaurant: PropTypes.shape({
    name: PropTypes.string,
    address: PropTypes.string,
    image: PropTypes.string,
    website: PropTypes.string,
    phone: PropTypes.string,
    culture: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.number
    }),
    description: PropTypes.string,
    cost: PropTypes.number,
    rating: PropTypes.number,
    mapLatitude: PropTypes.number,
    mapLongitude: PropTypes.number,
    deliverable: PropTypes.bool,
    review1: PropTypes.string,
    review2: PropTypes.string,
    review3: PropTypes.string,
    recipe_connections: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.number
      })
    )
  })
};

export default RestaurantInstance;
