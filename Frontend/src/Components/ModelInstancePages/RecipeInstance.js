import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Container } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Lottie from 'react-lottie';
import animationData from '../../assets/lottie/cookingCycle.json';
import './InstancePages.css';
import { Rating } from '@mui/material';

// Constant for time conversion
const MIN_PER_HOUR = 60;

// Main function to render recipe instance
function RecipeInstance(props) {
  const currentRecipe = props.recipe;
  let cultureConnection = currentRecipe.cuisine;

  // Get recipe time
  let recipeHours = Math.floor(currentRecipe.cookTime / MIN_PER_HOUR);
  let recipeMinutes = currentRecipe.cookTime % MIN_PER_HOUR;
  let recipeTimeString = recipeHours + ' hrs, ' + recipeMinutes + ' min';

  // Get culture link HTML
  let cultureLink =
    cultureConnection.id === null ? (
      <div>{cultureConnection.name}</div>
    ) : (
      <Link className="links" to={'/Cultures/' + cultureConnection.id}>
        {cultureConnection.name}
      </Link>
    );

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <Container>
      <h3 id="recipeName">Recipe: {currentRecipe.name}</h3>
      <h4 id="instance-culture">Culture: {cultureLink}</h4>
      <h7 style={{ fontFamily: 'Poppins' }}>{'Recipe rating'}</h7>
      <div>
        <Rating
          name="read-only"
          value={currentRecipe.rating}
          style={{ padding: '2px 0 0 2px' }}
          precision={0.5}
          readOnly
        />
      </div>
      <hr className="mb-5" />
      <Row>
        <div id="recipe-type">
          <strong>Dish Type: </strong>
          {currentRecipe.dishType}
        </div>
        <br />
        <br />
        <div id="recipe-description">{currentRecipe.description}</div>
        <br />
        <br />
        <Col id="recipe-link">
          <a className="links" href={currentRecipe.source} target="_blank" rel="noreferrer">
            Recipe Link
          </a>
        </Col>
      </Row>
      <hr className="mb-5" />
      <Row>
        <Col md={6} className="cookTime">
          <Row id="lottie">
            <Lottie
              options={defaultOptions}
              height={225}
              width={300}
              isClickToPauseDisabled={true}
            />
          </Row>
          <Row className="cookTime cookTimeNum">{recipeTimeString}</Row>
        </Col>
        <Col id="recipe-ingredients">
          <p id="ing-title">Ingredients</p>
          {currentRecipe.ingredients.map((item, index) => {
            if (item.id === null) {
              return (
                <li style={{ listStyle: 'none' }} key={index}>
                  {item.name}
                </li>
              );
            } else {
              return (
                <li style={{ listStyle: 'none' }} key={index}>
                  <Link className="links" to={'/Ingredients/' + item.id}>
                    {item.name}
                  </Link>
                </li>
              );
            }
          })}
        </Col>
      </Row>
      <hr className="mb-5" />

      <Row>
        <Col>
          <div>
            <p id="overview-title">Overview: How to Make {currentRecipe.name}</p>
          </div>
          <div id="overview">{currentRecipe.instructions}</div>
        </Col>
        <Col>
          <img id="recipe-img" src={currentRecipe.image} alt="recipe"></img>
        </Col>
      </Row>
      <hr className="mb-5" />
    </Container>
  );
}

RecipeInstance.propTypes = {
  recipe: PropTypes.shape({
    name: PropTypes.string,
    cuisine: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.number
    }),
    description: PropTypes.string,
    source: PropTypes.string,
    ingredients: PropTypes.array,
    instructions: PropTypes.string,
    image: PropTypes.string,
    cookTime: PropTypes.number,
    rating: PropTypes.number,
    dishType: PropTypes.string
  })
};

export default RecipeInstance;
