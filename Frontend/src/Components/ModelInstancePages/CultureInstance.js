import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Col, Row, Container, Table } from 'react-bootstrap';
import GoogleMapReact from 'google-map-react';
import './InstancePages.css';
import { GiPositionMarker } from 'react-icons/gi';

// Main render function for CultureInstance component
function CultureInstance(props) {
  // Retrieve from database (Must be moved to another area that allows API calls)
  const currentCulture = props.culture;
  if (props === undefined) console.log('bitch');

  const Marker = () => (
    <GiPositionMarker
      style={{
        display: 'inline-flex',
        padding: '15px 15px',
        fontSize: '70px',
        color: 'rgb(110, 94, 254)',
        justifyContent: 'center',
        transform: 'translate(-50%, -75%)'
      }}></GiPositionMarker>
  );

  const center = {
    lat: currentCulture.mapLatitude,
    lng: currentCulture.mapLongitude
  };

  // Render out all features of the culture found within the database instance
  return (
    <Container>
      <Row>
        <h3 id="cultureName">
          {' '}
          <b className="cultureName">{currentCulture.name}</b>
        </h3>
      </Row>
      <Row md={12} className="parent">
        <Col md={6} className="countryMap">
          <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyBMOeKQDczzf0UiatJpZvyr53IJtmMDlHo' }}
            defaultCenter={center}
            defaultZoom={4}
            options={{
              styles: [
                { featureType: 'all', elementType: 'labels', stylers: [{ visibility: 'on' }] }
              ]
            }}>
            <Marker lat={center.lat} lng={center.lng} />
          </GoogleMapReact>
        </Col>
        <Col md={6} className="flag">
          <img className="flagImg" src={currentCulture.image}></img>
        </Col>
      </Row>
      <Row>
        <Col md={6}>
          <div id="culture-description" style={{ textAlign: 'justify' }}>
            {currentCulture.description}
          </div>
        </Col>
        <Col>
          <Row id="culture-details">
            <Table borderless responsive>
              <thead>
                <td id="left-end">Type</td>
                <td id="right-end">Value</td>
              </thead>
              <tbody>
                <tr>
                  <td>Sub-region</td>
                  <td>{currentCulture.subRegion}</td>
                </tr>
                <tr>
                  <td>Language</td>
                  <td>{currentCulture.language}</td>
                </tr>
                <tr>
                  <td>Currency</td>
                  <td>{currentCulture.currency}</td>
                </tr>
                <tr>
                  <td>Recipes Found</td>
                  <td>{currentCulture.num_recipe_connections}</td>
                </tr>
                <tr>
                  <td>Restaurants Found</td>
                  <td>{currentCulture.num_restaurant_connections}</td>
                </tr>
              </tbody>
            </Table>
          </Row>
          <Row>
            <div>
              <a className="links" href={currentCulture.url} target="_blank" rel="noreferrer">
                More Info
              </a>
            </div>
          </Row>
        </Col>
      </Row>
      <hr />
      <Row>
        <h4 id="culture-produce">
          {currentCulture.name} is known for growing the following produce:
        </h4>
        {currentCulture.produce.map((item, index) => {
          return (
            <li style={{ textAlign: 'left' }} key={index}>
              {item}
            </li>
          );
        })}
      </Row>
      <hr className="mb-5" />
      <Row>
        <div id="culture-links-title">
          <h4>{currentCulture.name} Restaurants in Texas:</h4>
        </div>
        <ul id="culture-links">
          {currentCulture.restaurant_connections.map((item, index) => {
            return (
              <li style={{ textAlign: 'left' }} key={index}>
                <Link className="links" to={'/Restaurants/' + item.id}>
                  {item.name}
                </Link>
              </li>
            );
          })}
        </ul>
      </Row>
      <hr className="mb-5" />
      <Row>
        <div id="culture-links-title">
          <h4>{currentCulture.name} Recipes:</h4>
        </div>
        <ul id="culture-links">
          {currentCulture.recipe_connections.map((item, index) => {
            return (
              <li style={{ textAlign: 'left' }} key={index}>
                <Link className="links" to={'/Recipes/' + item.id}>
                  {item.name}
                </Link>
              </li>
            );
          })}
        </ul>
      </Row>
      <hr className="mb-5" />
    </Container>
  );
}

CultureInstance.propTypes = {
  culture: PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.string,
    country: PropTypes.string,
    subRegion: PropTypes.string,
    language: PropTypes.string,
    location: PropTypes.string,
    image: PropTypes.string,
    description: PropTypes.string,
    produce: PropTypes.array,
    restaurant_connections: PropTypes.array,
    recipe_connections: PropTypes.array,
    num_recipe_connections: PropTypes.number,
    num_restaurant_connections: PropTypes.number,
    currency: PropTypes.string,
    url: PropTypes.string,
    mapLatitude: PropTypes.number,
    mapLongitude: PropTypes.number
  })
};

export default CultureInstance;
