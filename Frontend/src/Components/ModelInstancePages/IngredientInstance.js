import React from 'react';
import { Col, Row, Container, OverlayTrigger, Tooltip, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactECharts from 'echarts-for-react';
import './InstancePages.css';

function IngredientInstance(props) {
  const currentIngredient = props.ingredient;

  const PieChart = function () {
    const options = {
      tooltip: {
        trigger: 'item'
      },
      legend: {
        top: '90%',
        left: 'center'
      },
      series: [
        {
          name: 'Nutrient (g)',
          type: 'pie',
          radius: ['40%', '70%'],
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 10,
            borderColor: '#fff',
            borderWidth: 2
          },
          label: {
            show: false,
            position: 'center'
          },
          emphasis: {
            label: {
              show: true,
              fontSize: '17',
              fontWeight: 'bold'
            }
          },
          labelLine: {
            show: false
          },
          data: [
            { value: currentIngredient.fat, name: 'Fat' },
            { value: currentIngredient.fiber, name: 'Fiber' },
            { value: currentIngredient.sugar, name: 'Sugar' },
            { value: currentIngredient.sodium, name: 'Sodium' },
            { value: currentIngredient.protein, name: 'Protein' }
          ]
        }
      ]
    };
    return <ReactECharts option={options} />;
  };

  const CalorieGauge = function () {
    const options = {
      series: [
        {
          type: 'gauge',
          startAngle: 270,
          endAngle: 0,
          min: 0,
          max: 200,
          splitNumber: 5,
          axisLine: {
            lineStyle: {
              width: 6,
              color: [
                [0.25, '#7CFFB2'],
                [0.5, '#58D9F9'],
                [0.75, '#FDDD60'],
                [1, '#FF6E76']
              ]
            }
          },
          pointer: {
            icon: 'path://M12.8,0.7l12,40.1H0.7L12.8,0.7z',
            length: '12%',
            width: 20,
            offsetCenter: [0, '-55%'],
            itemStyle: {
              color: 'auto'
            }
          },
          axisTick: {
            length: 12,
            lineStyle: {
              color: 'auto',
              width: 2
            }
          },
          splitLine: {
            length: 20,
            lineStyle: {
              color: 'auto',
              width: 5
            }
          },
          axisLabel: {
            color: '#464646',
            fontSize: 20,
            distance: -60,
            formatter: function (value) {
              if (value === 0.875) {
                return 'A';
              } else if (value === 0.625) {
                return 'B';
              } else if (value === 0.375) {
                return 'C';
              } else if (value === 0.125) {
                return 'D';
              }
              return '';
            }
          },
          title: {
            offsetCenter: [0, '-5%'],
            fontSize: 15
          },
          detail: {
            fontSize: 15,
            offsetCenter: [0, '15%'],
            valueAnimation: true,
            formatter: function (value) {
              return Math.round(value) + ' kcal';
            },
            color: 'auto'
          },
          data: [
            {
              value: currentIngredient.calories,
              name: 'Calorie Count'
            }
          ]
        }
      ]
    };
    return <ReactECharts option={options} />;
  };

  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {'Vegan'}
    </Tooltip>
  );

  return (
    <Container id="ingredient-container">
      <h3 id="ingredient-title">{currentIngredient.title}</h3>
      <Row md="auto" className="justify-content-md-center ing-subtitle">
        <h4 id="ingredient-price">${currentIngredient.price}</h4>
        <OverlayTrigger placement="right" overlay={renderTooltip}>
          <img
            className="food-badges"
            src={'/badges/vegan-3.png'}
            style={{ display: currentIngredient.vegan === true ? 'flex' : 'none' }}></img>
        </OverlayTrigger>
      </Row>
      <Row>
        <Col md={6} className="ingredient-details">
          <div id="details-border">
            <div>
              <p id="ing-desc-title">Description</p>
              <p id="ing-desc">{currentIngredient.description}</p>
            </div>
          </div>
        </Col>
        <Col md={6}>
          <img id="ingredient-img" src={currentIngredient.image}></img>
        </Col>
      </Row>
      <br />
      <h4 id="nut-facts">Nutrition Facts:</h4>
      <br />
      <Row>
        <Table borderless responsive>
          <thead>
            <td id="left-end">Nutrient</td>
            <td id="right-end">Count</td>
          </thead>
          <tbody>
            <tr>
              <td>Calories</td>
              <td>{currentIngredient.calories}</td>
            </tr>
            <tr>
              <td>Protein</td>
              <td>{currentIngredient.protein} g</td>
            </tr>
            <tr>
              <td>Fat</td>
              <td>{currentIngredient.fat} g</td>
            </tr>
            <tr>
              <td>Sugar</td>
              <td>{currentIngredient.sugar} g</td>
            </tr>
            <tr>
              <td>Fiber</td>
              <td>{currentIngredient.fiber} g</td>
            </tr>
            <tr>
              <td>Sodium</td>
              <td>{currentIngredient.sodium} g</td>
            </tr>
          </tbody>
        </Table>
      </Row>
      <Row id="ingredient-graphs">
        <Col>
          <PieChart />
        </Col>
        <Col>
          <CalorieGauge />
        </Col>
      </Row>
      <hr className="mb-5" />
      <Row>
        <div id="culture-links-title">
          <h4>Recipes Using Ingredient: </h4>
        </div>
        <ul id="culture-links">
          {currentIngredient.recipeConnections.map((item, index) => {
            return (
              <li style={{ textAlign: 'left' }} key={index}>
                <Link className="links" to={'/Recipes/' + item.id}>
                  {item.name}
                </Link>
              </li>
            );
          })}
        </ul>
      </Row>
    </Container>
  );
}

IngredientInstance.propTypes = {
  ingredient: PropTypes.shape({
    name: PropTypes.string,
    title: PropTypes.string,
    id: PropTypes.string,
    image: PropTypes.string,
    far: PropTypes.number,
    fiber: PropTypes.number,
    sugar: PropTypes.number,
    sodium: PropTypes.number,
    calcium: PropTypes.number,
    calories: PropTypes.number,
    fat: PropTypes.number,
    protein: PropTypes.number,
    tooltip: PropTypes.string,
    price: PropTypes.number,
    vegan: PropTypes.string,
    description: PropTypes.number,
    recipeConnections: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.number
      })
    )
  })
};

export default IngredientInstance;
