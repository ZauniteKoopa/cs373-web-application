import { Skeleton } from '@mui/material';
import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './InstanceSkeleton.css';

const RecipeSkeleton = () => {
  return (
    <Container>
      <Skeleton
        id="recipe-title-skeleton"
        variant="text"
        width={500}
        height={200}
        animation="wave"></Skeleton>
      <Skeleton
        id="recipe-title-skeleton"
        variant="rectangular"
        height={200}
        animation="wave"></Skeleton>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
      </Row>
    </Container>
  );
};

export default RecipeSkeleton;
