import React from 'react';
import { Skeleton } from '@mui/material';
import { Container, Row, Col } from 'react-bootstrap';
import './InstanceSkeleton.css';

const RestaurantSkeleton = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Skeleton
            id="restaurant-title-skeleton"
            variant="text"
            width={300}
            height={75}
            animation="wave"
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <Skeleton variant="rectangular" height={400} animation="wave" />
        </Col>
      </Row>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton id="restaurant-title-skeleton" variant="text" height={400} animation="wave" />
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton id="restaurant-title-skeleton" variant="text" height={400} animation="wave" />
        </Col>
      </Row>
      <Skeleton id="attr-table" variant="rectangular" height={75} animation="wave" />
    </Container>
  );
};

export default RestaurantSkeleton;
