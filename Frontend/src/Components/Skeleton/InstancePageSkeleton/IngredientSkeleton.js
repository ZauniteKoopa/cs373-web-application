import React from 'react';
import { Skeleton } from '@mui/material';
import { Container, Row, Col } from 'react-bootstrap';
import './InstanceSkeleton.css';

const IngredientSkeleton = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Skeleton
            id="ingredient-title-skeleton"
            variant="text"
            width={300}
            height={100}
            animation="wave"
          />
        </Col>
      </Row>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton variant="text" height={400} animation="wave" />
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton variant="text" height={400} animation="wave" />
        </Col>
      </Row>
      <Skeleton id="ingredient-title-skeleton" variant="text" height={400} animation="wave" />
      <Row className="graphs-skeleton">
        <Col>
          <Skeleton
            id="ingredient-title-skeleton"
            variant="circular"
            width={250}
            height={250}
            animation="wave"
          />
        </Col>
        <Col>
          <Skeleton
            id="ingredient-title-skeleton"
            variant="circular"
            width={250}
            height={250}
            animation="wave"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default IngredientSkeleton;
