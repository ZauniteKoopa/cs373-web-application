import { Skeleton } from '@mui/material';
import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import './InstanceSkeleton.css';

const CultureSkeleton = () => {
  return (
    <Container>
      <Skeleton
        id="culture-title-skeleton"
        variant="text"
        width={100}
        height={50}
        animation="wave"></Skeleton>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
        <Col md={6} sm={12} xs={12}>
          <Skeleton
            className="culture-block-skeleton"
            variant="rectangular"
            height={300}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <div id="links-skeleton">
        <Skeleton className="culture-produce-title-skeleton" width={500}></Skeleton>
        <Skeleton width={250}></Skeleton>
        <Skeleton width={250}></Skeleton>
        <Skeleton id="culture-produce-skeleton" width={250}></Skeleton>
        <Skeleton width={400}></Skeleton>
        <Skeleton width={250}></Skeleton>
        <Skeleton width={250}></Skeleton>
      </div>
    </Container>
  );
};

export default CultureSkeleton;
