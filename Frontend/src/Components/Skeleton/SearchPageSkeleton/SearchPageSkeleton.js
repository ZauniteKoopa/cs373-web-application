import React from 'react';
import { Skeleton } from '@mui/material';
import { Container, Row, Col } from 'react-bootstrap';
import './SearchPageSkeleton.css';

const SearchPageSkeleton = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Skeleton
            className="search-title-skeleton"
            variant="text"
            width={200}
            height={65}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row id="searchbar-skeleton">
        <Col md={10}>
          <Skeleton
            className="searchbar-skeleton"
            variant="text"
            width={1080}
            height={75}
            animation="wave"></Skeleton>
        </Col>
        <Col md={2}>
          <Skeleton
            className="searchbar-btn-skeleton"
            variant="text"
            width={100}
            height={75}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row id="sort-forms-skeleton">
        <Col md={3} sm={3} xs={3} className="sort-forms-div-skeleton">
          <Skeleton
            className="sort-forms-skeleton"
            variant="text"
            width={300}
            height={50}
            animation="wave"></Skeleton>
        </Col>
        <Col md={3} sm={3} xs={3} className="sort-forms-div-skeleton">
          <Skeleton
            className="sort-forms-skeleton"
            variant="text"
            width={300}
            height={50}
            animation="wave"></Skeleton>
        </Col>
        <Col md={3} sm={3} xs={3} className="sort-forms-div-skeleton">
          <Skeleton
            className="sort-forms-skeleton"
            variant="text"
            width={300}
            height={50}
            animation="wave"></Skeleton>
        </Col>
        <Col md={3} sm={3} xs={3} className="sort-forms-div-skeleton">
          <Skeleton
            className="sort-forms-skeleton"
            variant="text"
            width={300}
            height={50}
            animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row>
        <Col>
          <Skeleton variant="rectangular" height={600} animation="wave"></Skeleton>
        </Col>
      </Row>
      <Row>
        <Col>
          <Skeleton
            id="pagination-btn-skeleton"
            variant="text"
            width={250}
            height={50}
            animation="wave"></Skeleton>
        </Col>
      </Row>
    </Container>
  );
};

export default SearchPageSkeleton;
