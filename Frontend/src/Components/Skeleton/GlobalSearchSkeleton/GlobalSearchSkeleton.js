import React from 'react';
import { Skeleton } from '@mui/material';
import './GlobalSearchSkeleton.css';

const GlobalSearchSkeleton = () => {
  return (
    <Skeleton
      className="search-tiles-skeleton"
      variant="rectangular"
      width={240}
      height={475}
      animation="wave"></Skeleton>
  );
};

export default GlobalSearchSkeleton;
