import React from 'react';
import { Skeleton } from '@mui/material';
import { Container } from 'react-bootstrap';
import '../VisualizationsSkeleton.css';

const SubregionPiechartSkeleton = () => {
  return (
    <Container>
      <Skeleton
        className="visual-skelton"
        variant="circular"
        width={500}
        height={500}
        animation="wave"></Skeleton>
    </Container>
  );
};

export default SubregionPiechartSkeleton;
