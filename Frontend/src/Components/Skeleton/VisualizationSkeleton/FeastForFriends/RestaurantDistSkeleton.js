import React from 'react';
import { Skeleton } from '@mui/material';
import { Container } from 'react-bootstrap';
import '../VisualizationsSkeleton.css';

const RestaurantDistSkeleton = () => {
  return (
    <Container>
      <Skeleton
        className="visual-skelton"
        variant="rectangular"
        width={1000}
        height={400}
        animation="wave"></Skeleton>
    </Container>
  );
};

export default RestaurantDistSkeleton;
