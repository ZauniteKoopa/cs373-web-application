import React from 'react';
import { Skeleton } from '@mui/material';
import { Container, Row, Col } from 'react-bootstrap';
import '../VisualizationsSkeleton.css';

const DemographicsSkeleton = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Skeleton
            className="visual-skelton"
            variant="circular"
            width={500}
            height={500}
            animation="wave"></Skeleton>
        </Col>
        <Col>
          <Skeleton
            className="visual-skelton"
            variant="circular"
            width={500}
            height={500}
            animation="wave"></Skeleton>
        </Col>
      </Row>
    </Container>
  );
};

export default DemographicsSkeleton;
