import React from 'react';
import { Skeleton } from '@mui/material';
import { Container } from 'react-bootstrap';
import '../VisualizationsSkeleton.css';

const StateCompactnessSkeleton = () => {
  return (
    <Container>
      <Skeleton
        className="visual-skelton"
        variant="rectangular"
        width={1300}
        height={700}
        animation="wave"></Skeleton>
    </Container>
  );
};

export default StateCompactnessSkeleton;
