import React, { useState, useEffect } from 'react';
import { BiMenuAltRight } from 'react-icons/bi';
import { AiOutlineClose } from 'react-icons/ai';
import classes from './Nav.module.scss';
import { Link, NavLink } from 'react-router-dom';
import { MenuItems } from './MenuItems';

const Nav = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [size, setSize] = useState({
    width: undefined,
    height: undefined
  });

  useEffect(() => {
    const handleResize = () => {
      setSize({
        width: window.innerWidth,
        height: window.innerHeight
      });
    };
    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const menuToggleHandler = () => {
    setMenuOpen((p) => !p);
  };

  return (
    <header className={classes.navbar}>
      <div className={classes.navbar__content}>
        <Link to="/Home" className={classes.navbar__content__logo}>
          Feast For Friends
        </Link>
        <nav
          className={`${classes.navbar__content__nav} ${
            menuOpen && size.width < 768 ? classes.isMenu : ''
          }`}>
          <ul>
            {MenuItems.map((item, index) => {
              return (
                <li key={index} onClick={menuToggleHandler}>
                  <NavLink to={item.url}>{item.title}</NavLink>
                </li>
              );
            })}
          </ul>
        </nav>
        <div className={classes.navbar__content__toggle}>
          {!menuOpen ? (
            <BiMenuAltRight onClick={menuToggleHandler} />
          ) : (
            <AiOutlineClose onClick={menuToggleHandler} />
          )}
        </div>
      </div>
    </header>
  );
};

export default Nav;
