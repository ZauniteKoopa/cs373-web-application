export const MenuItems = [
  {
    title: 'Home',
    url: 'Home',
    className: 'nav-link'
  },
  {
    title: 'About',
    url: 'About',
    className: 'nav-link'
  },
  {
    title: 'Cultures',
    url: 'Cultures',
    className: 'nav-link'
  },
  {
    title: 'Recipes',
    url: 'Recipes',
    className: 'nav-link'
  },
  {
    title: 'Restaurants',
    url: 'Restaurants',
    className: 'nav-link'
  },
  {
    title: 'Ingredients',
    url: 'Ingredients',
    className: 'nav-link'
  },
  {
    title: 'Search',
    url: 'Search',
    className: 'nav-link'
  },
  {
    title: 'Visualizations',
    url: 'Visualizations',
    className: 'nav-link'
  },
  {
    title: 'GerryMap Data',
    url: 'GerryVisualizations',
    className: 'nav-link'
  }
];
