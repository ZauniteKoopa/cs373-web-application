import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Row, Container } from 'react-bootstrap';
import './Footer.css';
import '../../App.css';
import { AboutData } from '../AboutPage/AboutPageData';
import { HashLink } from 'react-router-hash-link';

const Footer = () => {
  const anthonyContact = AboutData.users[0].linkedIn;
  const aliContact = AboutData.users[1].linkedIn;
  const nathanContact = AboutData.users[2].linkedIn;
  const tannerContact = AboutData.users[3].linkedIn;
  return (
    <div id="Box">
      <h1 id="footerTitle">Discover Beyond the Knowns</h1>
      <Container>
        <Row>
          <Col>
            <div className="FooterSubheader">About Us</div>
            <div>
              <HashLink id="footer-link" className="underlined-links" to={'/About#purpose'}>
                Purpose
              </HashLink>
            </div>
            <div>
              <HashLink id="footer-link" className="underlined-links" to={'/About#members'}>
                Members
              </HashLink>
            </div>
            <div>
              <HashLink id="footer-link" className="underlined-links" to={'/About#resources'}>
                Resources
              </HashLink>
            </div>
          </Col>
          <Col>
            <div className="FooterSubheader">Services</div>
            <div>
              <Link id="footer-link" className="underlined-links" to={'/Cultures'}>
                Cultures
              </Link>
            </div>
            <div>
              <Link id="footer-link" className="underlined-links" to={'/Recipes'}>
                Recipes
              </Link>
            </div>
            <div>
              <Link id="footer-link" className="underlined-links" to={'/Restaurants'}>
                Restaurants
              </Link>
              <div>
                <Link id="footer-link" className="underlined-links" to={'/Ingredients'}>
                  Ingredients
                </Link>
              </div>
            </div>
          </Col>
          <Col style={{ marginBottom: '20px' }}>
            <div className="FooterSubheader">Contact Us</div>
            <div>
              <a
                id="footer-link"
                className="underlined-links"
                href={aliContact}
                target="_blank"
                rel="noreferrer">
                Ali Al-Adhami
              </a>
            </div>
            <div>
              <a
                id="footer-link"
                className="underlined-links"
                href={anthonyContact}
                target="_blank"
                rel="noreferrer">
                Anthony Hoang
              </a>
            </div>
            <div>
              <a
                id="footer-link"
                className="underlined-links"
                href={nathanContact}
                target="_blank"
                rel="noreferrer">
                Nathan Whyte
              </a>
            </div>
            <div>
              <a
                id="footer-link"
                className="underlined-links"
                href={tannerContact}
                target="_blank"
                rel="noreferrer">
                Tanner Dreiling
              </a>
            </div>
          </Col>
        </Row>
      </Container>
      <div id="copyRightsSection">
        <Container id="copyRights">
          &copy; Copyright{' '}
          <strong>
            <span>Feast For Friends</span>
          </strong>
          . All Rights Reserved
        </Container>
      </div>
    </div>
  );
};
export default Footer;
