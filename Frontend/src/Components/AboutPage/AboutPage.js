import React, { Component } from 'react';
import { AboutData } from './AboutPageData';
import DeveloperCard from './DeveloperCard';
import Footer from '../Footer/Footer';
import { Tabs, Tab, Container, Row, Col, Image, Card, Button } from 'react-bootstrap';
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';
import './AboutPage.css';
import '../../App.css';

// Icon images
import gitlabIcon from './AboutPageResourcesIcons/GitlabIcon.png';
import postmanIcon from './AboutPageResourcesIcons/Postman.png';

// REACT component focused on the About Page
class AboutPage extends Component {
  // Main initializer
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      numCommits: -1,
      numIssues: -1,
      devCommits: [0, 0, 0, 0],
      devIssues: [0, 0, 0, 0],
      didViewCountUp: false,
      didGitCountUp: false
    };

    this.getDevIssuesFreq = this.getDevIssuesFreq.bind(this);
    this.createDeveloperCard = this.createDeveloperCard.bind(this);
    this.onVisibilityChange = this.onVisibilityChange.bind(this);
    this.didGitViewCountUp = this.didGitViewCountUp.bind(this);
  }

  // Main method to get the number of issues that belong to a particular person
  //  Pre: issues is the list of gitlab issues on the project repo from gitlab API
  //  Post: returns an array whose indices match aboutdata.users and values = issues assigned
  getDevIssuesFreq(issues) {
    const usersList = AboutData.users;
    const userArray = new Array(usersList.length).fill(0);
    const userIndexHash = {};

    // Set up hashmap for users for faster lookup
    for (let i = 0; i < usersList.length; i++) {
      userIndexHash[usersList[i].gitlabID] = i;
    }

    // Go through each issue
    for (let i = 0; i < issues.length; i++) {
      const issueAssignees = issues[i].assignees;

      // If there were people assigned to this issue, go through assignees
      if (issueAssignees != null) {
        // For each assignee, see if found in the index has. If found, increment index
        for (let a = 0; a < issueAssignees.length; a++) {
          const currentAssignee = issueAssignees[a].username;
          const foundUserIndex = userIndexHash[currentAssignee];

          if (foundUserIndex !== undefined) {
            userArray[foundUserIndex]++;
          }
        }
      }
    }

    return userArray;
  }

  // Runs before rendering the first time on the page
  componentDidMount() {
    // Use Promise.All() to do multiple API calls
    Promise.all([
      fetch('https://gitlab.com/api/v4/projects/33818114/repository/contributors'),
      fetch('https://gitlab.com/api/v4/projects/33818114/issues?per_page=2000')

      // Convert all the API responses to readable JSON files
    ])
      .then((responses) => {
        return Promise.all(
          responses.map((response) => {
            return response.json();
          })
        );

        // Get the recorded data and convert that into relevant data for the about page
        // Data is an array in the following order [list of commits, list of issues]
      })
      .then((data) => {
        // Get rid of initial commit because of name issue (Anthony Tan Hoang instead of Anthony Hoang)
        // Get number of commits from contributors
        let usersList = AboutData.users;
        let numCommits = 0;
        let devCommits = [0, 0, 0, 0];
        let commitMap = {};

        // Create commit map for better efficiency
        for (let u = 0; u < usersList.length; u++) {
          let curUser = usersList[u];
          commitMap[curUser.commitEmail] = u;
        }

        // Go through each contributor and get the number of commits
        for (let c = 0; c < data[0].length; c++) {
          let currentContributor = data[0][c];
          let curNumCommits = currentContributor.commits;
          numCommits += curNumCommits;

          if (commitMap[currentContributor.email] !== undefined) {
            devCommits[commitMap[currentContributor.email]] = curNumCommits;
          }
        }

        // Get commits
        const numIssues = data[1].length;
        const devIssues = this.getDevIssuesFreq(data[1]);

        // Set the state
        this.setState({
          isLoaded: true,
          numCommits: numCommits,
          numIssues: numIssues,
          devCommits: devCommits,
          devIssues: devIssues
        });

        // Handle case of error
      })
      .catch((error) => {
        this.setState({
          isLoaded: true,
          error: error
        });
      });
  }

  // Main method to make a developer card
  createDeveloperCard(developer, index) {
    const numDevCommits = this.state.devCommits[index];
    const numDevIssues = this.state.devIssues[index];
    const numUnitTests = developer.Unittests;

    return (
      <DeveloperCard
        dev={developer}
        devCommits={numDevCommits}
        devIssues={numDevIssues}
        numUserTests={numUnitTests}
      />
    );
  }

  onVisibilityChange(isVisible) {
    if (isVisible) {
      this.setState({ didViewCountUp: true });
    }
  }

  didGitViewCountUp(isVisible) {
    if (isVisible) {
      this.setState({ didGitCountUp: true });
    }
  }

  // Main render function
  render() {
    const aboutData = AboutData;
    const totalCommits = this.state.numCommits;
    const totalIssues = this.state.numIssues;
    let totalUnitTests = 0;

    for (let i = 0; i < aboutData.users.length; i++) {
      totalUnitTests += aboutData.users[i].Unittests;
    }

    // Final render return, using about data
    return (
      <section>
        <Container>
          <h1 className="about-title" name="main-title">
            {' '}
            About{' '}
          </h1>
          <hr className="mb-5 title-spacer" />
          <Row>
            <h2 id="purpose" className="about-subtitle" name="purpose-title">
              {' '}
              Purpose{' '}
            </h2>
            <hr className="mb-4 spacer" />
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <p className="about-description" name="purpose-content">
              {aboutData.purpose}
            </p>
          </Row>
          <Row>
            <h2 className="about-subtitle" name="integration-title">
              {' '}
              Data Integration{' '}
            </h2>
            <hr className="mb-4 spacer" />
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <p className="about-description" name="integration-content">
              {aboutData.dataIntegrationResults}
            </p>
          </Row>

          <Row>
            <h2 id="members" className="about-subtitle" name="members-title">
              {' '}
              Members{' '}
            </h2>
            <hr className="mb-4 spacer" />
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
          </Row>
          <Row>
            {aboutData.users.map((item, index) => {
              const numDevCommits = this.state.devCommits[index];
              const numDevIssues = this.state.devIssues[index];
              return (
                <Card
                  key={index}
                  style={{
                    width: '17rem',
                    margin: '25px auto',
                    float: 'none',
                    marginBottom: '5vh'
                  }}>
                  <a href={item.linkedIn} target="_blank" rel="noreferrer">
                    <div className="dev-img" style={{ height: '280px' }}>
                      <Card.Img
                        variant="top"
                        src={item.photo}
                        style={{ marginTop: '20px', borderRadius: '50%' }}
                      />
                    </div>
                  </a>
                  <Card.Body>
                    <Card.Title style={{ textAlign: 'left', fontFamily: 'Poppins' }}>
                      {item.name}
                    </Card.Title>
                    <Card.Text
                      style={{
                        textAlign: 'left',
                        height: '250px',
                        fontFamily: 'Poppins',
                        fontSize: '13px'
                      }}
                      className="small text-muted mb-0">
                      <Row>
                        <Col>
                          <div
                            className="d-flex role-pill rounded-pill"
                            style={{ justifyContent: 'center' }}>
                            <p className="small mb-0">{item.responsibilities}</p>
                          </div>
                        </Col>
                        <Col>
                          <div
                            className="d-flex role-pill rounded-pill"
                            style={{ justifyContent: 'center' }}>
                            <p className="small mb-0">{item.leader}</p>
                          </div>
                        </Col>
                      </Row>
                      {item.bio}
                    </Card.Text>
                    <div className="gitlab-info-parent">
                      <div className="gitlab-info">
                        <h2>
                          <a>
                            <span>
                              <VisibilitySensor onChange={this.onVisibilityChange} delayedCall>
                                <CountUp
                                  start={0}
                                  end={this.state.didViewCountUp ? numDevCommits : 0}
                                  duration={3}
                                  delay={0.1}
                                  useEasing
                                />
                              </VisibilitySensor>
                            </span>
                            <small>Commits</small>
                          </a>
                        </h2>
                        <h2>
                          <a>
                            <span>
                              <VisibilitySensor onChange={this.onVisibilityChange} delayedCall>
                                <CountUp
                                  start={0}
                                  end={this.state.didViewCountUp ? numDevIssues : 0}
                                  duration={3}
                                  delay={0.1}
                                  useEasing
                                />
                              </VisibilitySensor>
                            </span>
                            <small>Issues</small>
                          </a>
                        </h2>
                        <h2>
                          <a>
                            <span>
                              <VisibilitySensor onChange={this.onVisibilityChange} delayedCall>
                                <CountUp
                                  start={0}
                                  end={this.state.didViewCountUp ? item.Unittests : 0}
                                  duration={3}
                                  delay={0.1}
                                  useEasing
                                />
                              </VisibilitySensor>
                            </span>
                            <small>Unittests</small>
                          </a>
                        </h2>
                      </div>
                    </div>
                    <div className="dev-btn">
                      <button>
                        <a
                          href={'' + item.gitlab}
                          target="_blank"
                          rel="noreferrer"
                          style={{ textDecoration: 'none', color: 'white' }}>
                          <i className="bi bi-git" style={{ fontSize: '20px' }}></i>
                        </a>
                      </button>
                      <button>
                        <a
                          href={'' + item.linkedIn}
                          target="_blank"
                          rel="noreferrer"
                          style={{ textOrientation: 'none', color: 'white' }}>
                          <i className="bi bi-linkedin" style={{ fontSize: '20px' }}></i>
                        </a>
                      </button>
                    </div>
                  </Card.Body>
                </Card>
              );
            })}
          </Row>

          <Row>
            <h2 className="about-subtitle" name="gitlab-repo-stats-title">
              {' '}
              GitLab Repository Stats{' '}
            </h2>
            <hr className="mb-4 spacer" />
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <Row>
              <Col>
                <a
                  href={AboutData.gitlabContributersURL}
                  target="_blank"
                  rel="noreferrer"
                  className="data-card">
                  <h3>
                    <VisibilitySensor onChange={this.didGitViewCountUp} delayedCall>
                      <CountUp
                        start={0}
                        end={this.state.didGitCountUp ? totalCommits : 0}
                        duration={4}
                        delay={0.1}
                        useEasing
                      />
                    </VisibilitySensor>
                  </h3>
                  <h4>Total Commits</h4>
                  <p>This is the total number of commits by the Feast For Friends team.</p>
                  <span className="link-text">
                    See on GitLab
                    <i className="bi bi-arrow-right gitlab-arrow"></i>
                  </span>
                </a>
              </Col>
              <Col>
                <a
                  href={AboutData.gitlabIssuesURL}
                  target="_blank"
                  rel="noreferrer"
                  className="data-card">
                  <h3>
                    <VisibilitySensor onChange={this.didGitViewCountUp} delayedCall>
                      <CountUp
                        start={0}
                        end={this.state.didGitCountUp ? totalIssues : 0}
                        duration={4}
                        delay={0.1}
                        useEasing
                      />
                    </VisibilitySensor>
                  </h3>
                  <h4>Total Issues</h4>
                  <p>
                    This is the total number of issues and feature requests by the Feast For Friends
                    team.
                  </p>
                  <span className="link-text">
                    See on GitLab
                    <i className="bi bi-arrow-right gitlab-arrow"></i>
                  </span>
                </a>
              </Col>
              <Col>
                <a
                  href={AboutData.gitlabURL}
                  target="_blank"
                  rel="noreferrer"
                  className="data-card">
                  <h3>
                    <VisibilitySensor onChange={this.didGitViewCountUp} delayedCall>
                      <CountUp
                        start={0}
                        end={this.state.didGitCountUp ? totalUnitTests : 0}
                        duration={4}
                        delay={0.1}
                        useEasing
                      />
                    </VisibilitySensor>
                  </h3>
                  <h4>Total Unittests</h4>
                  <p>This is the total number of unit tests done by the Feast For Friends team.</p>
                  <span className="link-text">
                    See on GitLab
                    <i className="bi bi-arrow-right gitlab-arrow"></i>
                  </span>
                </a>
              </Col>
            </Row>
          </Row>
          <Row>
            <h2
              id="resources"
              className="about-subtitle"
              style={{ paddingTop: '20px' }}
              name="resources-title">
              {' '}
              Other Resources{' '}
            </h2>
            <hr className="mb-4 spacer" />
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>

            <div className="about-resources-tabs">
              <Row>
                <Col>
                  <Tabs defaultActiveKey="used-apis" style={{ fontFamily: 'Poppins' }}>
                    <Tab eventKey="used-apis" title="Used APIs">
                      <Row>
                        {aboutData.usedAPIs.map((item, index) => {
                          return (
                            <Card
                              key={index}
                              style={{
                                width: '15rem',
                                margin: '25px auto',
                                float: 'none',
                                marginBottom: '10px'
                              }}>
                              <div style={{ height: '250px' }}>
                                <Card.Img
                                  variant="top"
                                  src={item.logo}
                                  style={{ marginTop: '20px' }}
                                />
                              </div>
                              <Card.Body>
                                <Card.Title style={{ textAlign: 'left', fontFamily: 'Poppins' }}>
                                  {item.name}
                                </Card.Title>
                                <Card.Text
                                  style={{
                                    height: '100px',
                                    textAlign: 'left',
                                    fontFamily: 'Poppins'
                                  }}
                                  className="small text-muted mb-0">
                                  {item.description}
                                </Card.Text>
                                <Button
                                  variant="primary"
                                  style={{ marginTop: '10px' }}
                                  className="badge badge-primary px-3 rounded-pill font-weight-normal generic-primary-btn">
                                  <a
                                    href={item.link}
                                    target="_blank"
                                    rel="noreferrer"
                                    style={{ textDecoration: 'none', color: 'white' }}>
                                    Visit {item.name}
                                  </a>
                                </Button>
                              </Card.Body>
                            </Card>
                          );
                        })}
                      </Row>
                    </Tab>
                    <Tab eventKey="tools-used" title="Tools Used">
                      <Row>
                        {aboutData.toolsUsed.map((item, index) => {
                          return (
                            <Card
                              key={index}
                              style={{
                                width: '15rem',
                                margin: '25px auto',
                                float: 'none',
                                marginBottom: '10px'
                              }}>
                              <div style={{ height: '250px' }}>
                                <Card.Img
                                  variant="top"
                                  src={item.logo}
                                  style={{ marginTop: '20px' }}
                                />
                              </div>
                              <Card.Body>
                                <Card.Title style={{ textAlign: 'left' }}>{item.name}</Card.Title>
                                <Card.Text
                                  style={{ height: '100px', textAlign: 'left' }}
                                  className="small text-muted mb-0">
                                  {item.description}
                                </Card.Text>
                                <Button
                                  variant="primary"
                                  style={{ marginTop: '10px' }}
                                  className="badge badge-primary px-3 rounded-pill font-weight-normal generic-primary-btn">
                                  <a
                                    href={item.link}
                                    target="_blank"
                                    rel="noreferrer"
                                    style={{ textDecoration: 'none', color: 'white' }}>
                                    Visit {item.name}
                                  </a>
                                </Button>
                              </Card.Body>
                            </Card>
                          );
                        })}
                      </Row>
                    </Tab>
                    <Tab eventKey="project-links" title="Project Links">
                      <br />
                      <div className="about-resources-link">
                        <h4 id="project-link-title">GitLab</h4>
                        <a href={aboutData.gitlabURL} target="_blank" rel="noreferrer">
                          <Image src={gitlabIcon} className="about-resources-icon" />
                        </a>
                      </div>
                      <div className="about-resources-link">
                        <h4 id="project-link-title">Postman</h4>
                        <a href={aboutData.postmanURL} target="_blank" rel="noreferrer">
                          <Image src={postmanIcon} className="about-resources-icon" />
                        </a>
                      </div>
                    </Tab>
                  </Tabs>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>
        <div style={{ paddingTop: '4vh' }}>
          <Footer />
        </div>
      </section>
    );
  }
}

export default AboutPage;
