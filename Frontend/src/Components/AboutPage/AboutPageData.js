// Imported images
import anthonyHoangImg from './AboutPagePhotos/AnthonyHoang_Photo.jpg';
import nathanWhyteImg from './AboutPagePhotos/NathanWhyte_Photo.jpg';
import aliAlAhamiImg from './AboutPagePhotos/AliAlAdhami_Photo.jpg';
import tannerDreilingImg from './AboutPagePhotos/TannerDreiling_Photo.png';

// Main Data for the about page
export const AboutData = {
  purpose:
    'Feast for Friends is a tool for those who are looking for recipes, figuring out what they can cook with a certain ingredient, or want to find a restaurant. Our website will feature recipes, ingredients, and restaurants from a variety of cultures. Food is an integral part of all of our lives, so Feast for Friends helps you find great recipes that fit any appetite or budget. Have an ingredient that you want to incorporate in your cooking? Feast for Friends gives you recipes that use what you have. Not in the mood for cooking? Feast for Friends will provide restaurants to fit your tastes.',
  dataIntegrationResults:
    'As a result of our data integration, we discovered that a lot of the recipes and connections found mainly stem from Asia and Europe, which indicates a higher demand for european food, possibly French and Italian cuisines, and asian foods like Chinese or Japan. Additionally, many of the restaurants found tend ton cluster towards densely populated cities in South and East Texas instead of the dry, sparse North and West, reflecting immigrant tendencies to settle in cities of perceived oppurtunity. Additionally, we have found many recipes using ingredients like garlic, butter, black pepper, and flour.',
  gitlabURL: 'https://gitlab.com/ZauniteKoopa/cs373-web-application',
  gitlabIssuesURL:
    'https://gitlab.com/ZauniteKoopa/cs373-web-application/-/issues/?sort=label_priority_desc&state=all',
  gitlabContributersURL: 'https://gitlab.com/ZauniteKoopa/cs373-web-application/-/graphs/main',
  postmanURL: 'https://documenter.getpostman.com/view/19722596/Uyr4LLAy',
  usedAPIs: [
    {
      name: 'Nutritionix',
      description: 'Searching for nutritional values for recipes and ingredients',
      logo: '/api_img/nutritionix.png',
      link: 'https://www.nutritionix.com/'
    },
    {
      name: 'Google Maps',
      description: 'Searching for locations of restaurants and displaying them on a map',
      logo: '/api_img/google_maps.png',
      link: 'https://developers.google.com/maps'
    },
    {
      name: 'Yelp Fusion',
      description: 'searching for restaurant reviews and ratings',
      logo: '/api_img/yelp.png',
      link: 'https://fusion.yelp.com/'
    },
    {
      name: 'Countries-Cities',
      description: 'Information and location about different cultures',
      logo: '/api_img/geo_api.jpg',
      link: 'https://rapidapi.com/natkapral/api/countries-cities/'
    },
    {
      name: 'Spoonacular',
      description: 'Information about recipes',
      logo: '/api_img/spoonacular.jpg',
      link: 'https://spoonacular.com/food-api'
    },
    {
      name: 'GitLab',
      description: 'Information about commits and issues concerning our git repo',
      logo: '/api_img/gitlab.png',
      link: 'https://about.gitlab.com/'
    }
  ],

  toolsUsed: [
    {
      name: 'Docker',
      logo: '/tools_img/docker.png',
      link: 'https://www.docker.com/',
      description:
        'Program that allows easy usage of toolsets across teams and on automated cloud servers'
    },
    {
      name: 'AWS Amplify',
      logo: '/tools_img/amplify.png',
      link: 'https://aws.amazon.com/amplify/',
      description: 'Cloud server in which the website will be hosted on'
    },
    {
      name: 'React-Router',
      logo: '/tools_img/react_router.jpg',
      link: 'https://reactrouter.com/',
      description: 'Node module to allow for easy page navigation'
    },
    {
      name: 'React-Bootstrap',
      logo: '/tools_img/react_bootstrap.png',
      link: 'https://react-bootstrap.github.io/',
      description: 'Node module to apply bootstrap to react code in the form of components'
    },
    {
      name: 'Bootstrap Icons',
      logo: '/tools_img/bootstrap_icons.png',
      link: 'https://www.npmjs.com/package/bootstrap-icons',
      description: "Bootstrap's high quality, open source icon library"
    },
    {
      name: 'MUI',
      logo: '/tools_img/mui.png',
      link: 'https://mui.com/',
      description: 'A comprehensive suite of UI tools and components used alongside Bootstrap'
    },
    {
      name: 'NPM',
      logo: '/tools_img/npm.png',
      link: 'https://www.npmjs.com/',
      description: 'Installer manager for node module packages for the web application'
    },
    {
      name: 'Postman',
      logo: '/tools_img/postman.png',
      link: 'https://www.postman.com/',
      description: 'Backend API Documentation'
    },
    {
      name: 'Google Maps API',
      logo: '/tools_img/google_maps.png',
      link: 'https://developers.google.com/maps',
      description: 'Google Maps API used to integrate a Google Map window'
    },
    {
      name: 'React Lottie',
      logo: '/tools_img/generic_npm_package.png',
      link: 'https://github.com/chenqingspring/react-lottie',
      description: 'NPM package to display Lottie vectorial animations'
    },
    {
      name: 'LottieFiles',
      logo: '/tools_img/lottiefiles.png',
      link: 'https://lottiefiles.com/',
      description: 'Library for Lottie animations'
    },
    {
      name: 'ECharts',
      logo: '/tools_img/echarts.png',
      link: 'https://echarts.apache.org/en/index.html',
      description: 'Library for charts and graphs used to visualize data to users'
    },
    {
      name: 'Inkscape',
      logo: '/tools_img/inkscape.png',
      link: 'https://inkscape.org/',
      description:
        'Vector graphics editor that offers a rich set of features used for artistic illustrations'
    },
    {
      name: 'AOS',
      logo: '/tools_img/generic_npm_package.png',
      link: 'https://www.npmjs.com/package/aos',
      description: 'Allows for page animation on scroll'
    },
    {
      name: 'Node Sass',
      logo: '/tools_img/sass.jpg',
      link: 'https://www.npmjs.com/package/node-sass/',
      description: 'A library that provides binding for Node.js to LibSass.'
    },
    {
      name: 'AWS Elastic Beanstalk',
      description:
        'Orchestration service that deploys the API backend so that the API can take all requests onine',
      logo: '/api_img/aws.png',
      link: 'https://aws.amazon.com/elasticbeanstalk/'
    },
    {
      name: 'AWS RDS',
      description: 'Relational database manager that holds all of our scraped instances',
      logo: '/api_img/aws_rds.png',
      link: 'https://aws.amazon.com/rds/'
    },
    {
      name: 'Flask',
      description:
        'Used in conjunction with Flask-Restless to make the API calls to the backend and get the data',
      logo: '/tools_img/flask.png',
      link: 'https://flask.palletsprojects.com/en/2.1.x/'
    },
    {
      name: 'SQLAlchemy',
      description: 'Used to create the database and populate it with the data scraped',
      logo: '/tools_img/sql.png',
      link: 'https://www.sqlalchemy.org/'
    },
    {
      name: 'pgAdmin',
      description: "Used to view and manage our SQLAlchemy database that's hosted on RDS",
      logo: '/tools_img/pgadmin.png',
      link: 'https://www.pgadmin.org/'
    },
    {
      name: 'Namecheap',
      logo: '/tools_img/namecheap.png',
      link: 'https://www.namecheap.com/',
      description: 'Namecheap was used to obtain the current domain of the project'
    },
    {
      name: 'Recharts',
      logo: '/tools_img/recharts.png',
      link: 'https://recharts.org/en-US/',
      description:
        'Main tool for data visualizations, specifically Pie charts, bar charts, and scatter plots.'
    },
    {
      name: 'React-Leaflet',
      logo: '/tools_img/react-leaflet.png',
      link: 'https://recharts.org/en-US/',
      description: 'Main tool for data visualizations concerning geographic maps like Bubble Maps.'
    },
    {
      name: 'Discord',
      logo: '/tools_img/discord.png',
      link: 'https://discord.com/',
      description: 'Main medium for team communication'
    },
    {
      name: 'GitLab',
      logo: '/tools_img/gitlab.png',
      link: 'https://about.gitlab.com/',
      description: 'Main location of our repository'
    }
  ],

  users: [
    {
      name: 'Anthony Hoang',
      gitlabID: 'ZauniteKoopa',
      commitEmail: 'anthonyhoang3074@gmail.com',
      photo: anthonyHoangImg,
      bio: "I'm a Senior at The University of Texas at Austin, majoring in Computer Science. I was born in California and eventually moved to Pflugerville Texas when I was 5. I'm a Vietnamese-American software engineer with an interest in game development and AI who will go on to work at Expedia after graduation.",
      responsibilities: 'Front-End',
      leader: 'Phase 1',
      linkedIn: 'https://www.linkedin.com/in/anthony-hoang-253365184/',
      gitlab: 'https://gitlab.com/ZauniteKoopa',
      Unittests: 49
    },
    {
      name: 'Ali Al-Adhami',
      gitlabID: 'Ali-AlAdhami',
      commitEmail: 'aliraad@utexas.edu',
      photo: aliAlAhamiImg,
      bio: "I'm a Computer Science junior at the University of Texas at Austin. I was born in Iraq and moved to Texas at the age of 17. I've been living in North Austin for the past 5 years. In my free time, I like to play a lot of FPS video games, watch interesting YouTube videos, or just follow the news. I will be interning at Wells Fargo as a Software Engineer this summer.",
      responsibilities: 'Front-End',
      leader: 'Phase 4',
      linkedIn: 'https://www.linkedin.com/in/ali-raad-aladhami/',
      gitlab: 'https://gitlab.com/Ali-AlAdhami',
      Unittests: 13
    },
    {
      name: 'Nathan Whyte',
      gitlabID: 'nathanwhyte',
      commitEmail: 'nathanwhyte35@gmail.com',
      photo: nathanWhyteImg,
      bio: "I'm a Junior at the University of Texas at Austin majoring in Computer Science. I have an interest in Linux, GNU and free software as a whole. In my free time, I enjoy lifing weights and playing guitar, ocassoinally working in some time to watch sports and video games.",
      responsibilities: 'Back-End',
      leader: 'Phase 2',
      linkedIn: 'https://www.linkedin.com/in/nathan-whyte/',
      gitlab: 'https://gitlab.com/nathanwhyte',
      Unittests: 32
    },
    {
      name: 'Tanner Dreiling',
      gitlabID: 'tdreiling19',
      commitEmail: 'tdreiling19@utexas.edu',
      photo: tannerDreilingImg,
      bio: 'I am a senior CS student from Houston who is interested in Networks. In my spare time I like to watch movies, play video games, and learn about legal matters and history. I am currently set to internship at Amazon this summer.',
      responsibilities: 'Back-End',
      leader: 'Phase 3',
      linkedIn: 'https://www.linkedin.com/in/tanner-dreiling-15a84a224/',
      gitlab: 'https://gitlab.com/tdreiling19',
      Unittests: 32
    }
  ]
};
