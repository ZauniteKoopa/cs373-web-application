import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './DeveloperCard.css';

// REACT component for Developer cards shown in About Page
//  Has 3 things as props: dev information from about data (dev), devCommits, devIssues
class DeveloperCard extends Component {
  // Render function for developer card
  render() {
    const { name, bio, responsibilities, photo } = this.props.dev;
    const { devCommits, devIssues, numUnitTests } = this.props;

    return (
      <Table striped bordered className="dev-card">
        <thead className="dev-photo-container">
          <tr key="dev-photo">
            <td>
              <img src={photo} alt={name + ' Profile pic'} className="dev-photo" />
            </td>
          </tr>
        </thead>
        <tbody>
          <tr className="dev-header" key="dev-name">
            <td className="dev-header-content">
              <strong>{name}</strong>
            </td>
          </tr>
          <tr className="dev-item" key="dev-bio">
            <td className="dev-item">
              <strong>Bio:</strong> {bio}
            </td>
          </tr>
          <tr className="dev-item" key="dev-role">
            <td>
              <strong>Role:</strong> {responsibilities}
            </td>
          </tr>
          <tr className="dev-item" key="dev-commits">
            <td>
              <strong>Num Commits:</strong> {devCommits}
            </td>
          </tr>
          <tr className="dev-item" key="dev-issues">
            <td>
              <strong>Num Issues:</strong> {devIssues}
            </td>
          </tr>
          <tr className="dev-item" key="dev-tests">
            <td>
              <strong>Num Unit Tests:</strong> {numUnitTests}
            </td>
          </tr>
        </tbody>
      </Table>
    );
  }
}

// Developer prop types
DeveloperCard.propTypes = {
  dev: PropTypes.shape({
    name: PropTypes.string,
    bio: PropTypes.string,
    responsibilities: PropTypes.string,
    photo: PropTypes.any
  }),
  devCommits: PropTypes.number,
  devIssues: PropTypes.number,
  numUnitTests: PropTypes.number
};

export default DeveloperCard;
