import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button, Card, Row, Col } from 'react-bootstrap';
import { highlightTerms } from '../ModelSearchPages/Searching';
import GlobalSearchSkeleton from '../Skeleton/GlobalSearchSkeleton/GlobalSearchSkeleton';
import ErrorPage from '../NotFoundError/NotFound';
import '../Skeleton/GlobalSearchSkeleton/GlobalSearchSkeleton.css';

function GlobalCultures(props) {
  const searchQuery = props.searchQuery;

  // Setting state
  const [apiState, setApiState] = useState({
    content: null,
    currentPage: 1
  });
  const [errorFlag, setErrorFlag] = useState(false);

  // Get list of shallow recipes
  useEffect(() => {
    const getCultures = async () => {
      console.log(searchQuery + ' ' + apiState.currentPage);
      await fetch('https://api.feastforfriends.me/api/cultures?search_term=' + searchQuery)
        // Turns response into a json that can actually be used
        .then((response) => {
          return response.json();
        })
        // The actual data we need to display
        .then((data) => {
          setApiState({
            content: data,
            currentPage: apiState.currentPage
          });
        })
        // Catching errors
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    // Ensures that only run api calls once
    if (!errorFlag) {
      getCultures();
    }
  }, [props.searchQuery]);

  // Check if loading or error
  if (!errorFlag && apiState.content === null) {
    return (
      <Row>
        {[...Array(10)].map((x, i) => (
          <Col key={i} id="search-tile-col">
            <GlobalSearchSkeleton />
          </Col>
        ))}
      </Row>
    );
  } else if (errorFlag) {
    return <ErrorPage />;
  }

  // get data
  const cultures = apiState.content.data;

  return (
    <>
      {cultures.map((item, index) => {
        return (
          <Card
            key={index}
            style={{
              width: '15rem',
              margin: '25px auto',
              float: 'none',
              marginBottom: '10px'
            }}>
            <div style={{ height: '200px' }}>
              <Card.Img
                variant="top"
                src={item.image}
                style={{ marginTop: '20px', maxWidth: '215px', maxHeight: '135px' }}
              />
            </div>
            <Card.Body>
              <Card.Title style={{ textAlign: 'left', fontFamily: 'Poppins', fontSize: '18px' }}>
                {highlightTerms(item.name, searchQuery)}
              </Card.Title>
              <Card.Text
                style={{
                  height: '200px',
                  textAlign: 'left',
                  fontFamily: 'Poppins',
                  fontSize: 'smaller'
                }}
                className="small text-muted mb-0">
                {item.description.length > 175
                  ? highlightTerms(item.description.substr(0, 174) + ' ...', searchQuery)
                  : highlightTerms(item.description, searchQuery)}
              </Card.Text>
              <Button
                variant="primary"
                style={{ marginTop: '10px' }}
                className="badge badge-primary px-3 rounded-pill font-weight-normal generic-primary-btn">
                <Link
                  to={'/Cultures/' + item.id}
                  style={{ textDecoration: 'none', color: 'white' }}>
                  Visit {item.name}
                </Link>
              </Button>
            </Card.Body>
          </Card>
        );
      })}
      <br />
      <Row>
        <Col md={{ offset: 4, span: 4 }}>
          <Button
            variant="primary"
            style={{ marginTop: '10px' }}
            className="badge badge-primary px-3 rounded-pill font-weight-normal generic-primary-btn">
            <Link
              to={'/Cultures?search-bar-input=' + searchQuery}
              style={{ textDecoration: 'none', color: 'white' }}>
              See More Cultures
            </Link>
          </Button>
        </Col>
      </Row>
      <br />
    </>
  );
}

export default GlobalCultures;

GlobalCultures.propTypes = {
  searchQuery: PropTypes.string
};
