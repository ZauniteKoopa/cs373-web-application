import React, { useState } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useSearchParams } from 'react-router-dom';
import './GlobalSearch.css';
import GlobalCultures from './GlobalCultures';
import GlobalRecipes from './GlobalRecipes';
import GlobalRestaurants from './GlobalRestaurants';
import GlobalIngredients from './GlobalIngredients';

const GlobalSearch = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  let searchQuery = searchParams.get('search-bar-input');
  if (searchQuery === null || searchQuery === undefined) {
    searchQuery = '';
  }
  const [tempQuery, setTempQuery] = useState(searchQuery);

  return (
    <Container>
      <h3 id="search-title">
        Search results for: <i>{searchQuery}</i>
      </h3>
      <Row style={{ margin: '0 0 30px 0' }}>
        <Col sm={10}>
          <Form className="search-bar">
            <Form.Control
              type="text"
              placeholder="Search FeastForFriends"
              className="search-bar-text"
              name="search-bar-input"
              value={tempQuery}
              onChange={(event) => {
                setTempQuery(event.target.value);
              }}
            />
          </Form>
        </Col>
        <Col sm={2} className="submit-button">
          <Button
            id="submit-button"
            variant="primary"
            className="generic-primary-btn"
            type="submit"
            name="search-bar-submit"
            onClick={() => {
              setSearchParams({ 'search-bar-input': tempQuery });
            }}>
            Search
          </Button>
        </Col>
      </Row>
      <Row>
        <h4 id="searchpage-model-title">Cultures</h4>
        <GlobalCultures searchQuery={searchQuery} />
      </Row>
      <hr className="mb-5" />
      <Row>
        <h4 id="searchpage-model-title">Recipes</h4>
        <GlobalRecipes searchQuery={searchQuery} />
      </Row>
      <hr className="mb-5" />
      <Row>
        <h4 id="searchpage-model-title">Restaurants</h4>
        <GlobalRestaurants searchQuery={searchQuery} />
      </Row>
      <hr className="mb-5" />
      <Row>
        <h4 id="searchpage-model-title">Ingredients</h4>
        <GlobalIngredients searchQuery={searchQuery} />
      </Row>
    </Container>
  );
};

export default GlobalSearch;
