import {
  getValidTextFilters,
  getNumericalFilters,
  filterInstance
} from '../Components/ModelSearchPages/Filtering';

// Main function to check if filter lists matches what is expected
function confirmFilterList(actualList, expectedList) {
  expect(actualList.length).toBe(expectedList.length);

  for (let i = 0; i < actualList.length; i++) {
    expect(actualList[i]).toBe(expectedList[i]);
  }
}

// Test for checking if function can discern 3 categories and objects only have 1 field
test('Text Filters Test 1: Distinct Categories, One Attribute', () => {
  const testSample = [{ animal: 'dog' }, { animal: 'cat' }, { animal: 'rat' }];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = ['cat', 'dog', 'rat'];

  confirmFilterList(filterList, correctAnswer);
});

// Test for checking if function can merge the same categories together and objects only have one field
test('Text Filters Test 2: Same Category, One Attribute', () => {
  const testSample = [{ animal: 'dog' }, { animal: 'dog' }, { animal: 'dog' }];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = ['dog'];

  confirmFilterList(filterList, correctAnswer);
});

// Text filters: distinct category, multiple attributes
test('Text Filters Test 3: Distinct Categories, Multiple Attribute', () => {
  const testSample = [
    { animal: 'dog', color: 'red' },
    { animal: 'cat', color: 'blue' },
    { animal: 'rat', color: 'green' }
  ];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = ['cat', 'dog', 'rat'];

  confirmFilterList(filterList, correctAnswer);
});

// Text filters: same category, multiple attributes
test('Text Filters Test 4: Same Categories, Multiple Attribute', () => {
  const testSample = [
    { animal: 'cat', color: 'red' },
    { animal: 'cat', color: 'blue' },
    { animal: 'cat', color: 'green' }
  ];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = ['cat'];

  confirmFilterList(filterList, correctAnswer);
});

// Text filters: some merging and diffrentiation
test('Text Filters Test 5: Merge and Discern', () => {
  const testSample = [
    { animal: 'cat', color: 'red' },
    { animal: 'dog', color: 'blue' },
    { animal: 'cat', color: 'green' }
  ];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = ['cat', 'dog'];

  confirmFilterList(filterList, correctAnswer);
});

// Text filters: Edge Case - Empty database
test('Text Filters Test 6: Empty Database', () => {
  const testSample = [];
  let filterList = getValidTextFilters('animal', testSample);
  let correctAnswer = [];

  confirmFilterList(filterList, correctAnswer);
});

// Numberical Filter: Normal case, adding
test('Numerical Filters Test 1: Normal Adding Case', () => {
  let actualAnswer = getNumericalFilters(0, 1, 5);
  let correctAnswer = [0, 1, 2, 3, 4, 5];

  confirmFilterList(actualAnswer, correctAnswer);
});

// Numberical Filter: Normal case, Subtracting
test('Numerical Filters Test 2: Normal Subtracting Case', () => {
  let actualAnswer = getNumericalFilters(5, -1, 5);
  let correctAnswer = [5, 4, 3, 2, 1, 0];

  confirmFilterList(actualAnswer, correctAnswer);
});

// Edge Case: Step size == 0
test('Numerical Filters Test 3: Edge Case - Step Size == 0', () => {
  let actualAnswer = getNumericalFilters(0, 0, 5);
  let correctAnswer = [0, 0, 0, 0, 0, 0];

  confirmFilterList(actualAnswer, correctAnswer);
});

//
// Tests for filtering
//
test('Filtering Instance Test 1: Filter by single string attribute', () => {
  let filterRequirements = { name: 'alice' };

  let instance1 = { name: 'alice', points: 5 };
  let instance2 = { name: 'jacob', points: 3 };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(true);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(false);
});

test('Filtering Instance Test 2: Filter by single number >= attribute', () => {
  let filterRequirements = { points: [4, '>='] };

  let instance1 = { name: 'alice', points: 5 };
  let instance2 = { name: 'jacob', points: 3 };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(true);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(false);
});

test('Filtering Instance Test 3: Filter by single number <= attribute', () => {
  let filterRequirements = { points: [4, '<='] };

  let instance1 = { name: 'alice', points: 5 };
  let instance2 = { name: 'jacob', points: 3 };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(true);
});

test('Filtering Instance Test 4: Filter by multiple string attributes', () => {
  let filterRequirements = { name: 'alice', city: 'austin' };

  let instance1 = { name: 'alice', city: 'austin', culture: 'german' };
  let instance2 = { name: 'george', city: 'austin', culture: 'chinese' };
  let instance3 = { name: 'alice', city: 'dallas', culture: 'american' };
  let instance4 = { name: 'alice', city: 'austin', culture: 'viet' };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(true);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance3, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance4, filterRequirements);
  expect(result).toBe(true);
});

test('Filtering Instance Test 5: Filter by multiple numerical attributes', () => {
  let filterRequirements = { points: [3, '>='], deductions: [2, '<='] };

  let instance1 = { name: 'alice', points: 5, deductions: 1 };
  let instance2 = { name: 'mary', points: 2, deductions: 0 };
  let instance3 = { name: 'anthony', points: 4, deductions: 3 };
  let instance4 = { name: 'alex', points: 7, deductions: 2 };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(true);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance3, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance4, filterRequirements);
  expect(result).toBe(true);
});

test('Filtering Instance Test 6: Filter by combination of numerical and string attributes', () => {
  let filterRequirements = { name: 'alice', points: [3, '>='] };

  let instance1 = { name: 'alice', points: 5, deductions: 1 };
  let instance2 = { name: 'alice', points: 2, deductions: 0 };
  let instance3 = { name: 'anthony', points: 4, deductions: 3 };
  let instance4 = { name: 'alice', points: 7, deductions: 2 };

  let result = filterInstance(instance1, filterRequirements);
  expect(result).toBe(true);

  result = filterInstance(instance2, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance3, filterRequirements);
  expect(result).toBe(false);

  result = filterInstance(instance4, filterRequirements);
  expect(result).toBe(true);
});
