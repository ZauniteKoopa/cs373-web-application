import { filterBySearch } from '../Components/ModelSearchPages/Searching';

// Filter by search test: basic case
test('Search filter Test 1: Basic Case', () => {
  let testData = [
    {
      name: 'Mario'
    },
    {
      name: 'Luigi'
    },
    {
      name: 'Peach'
    },
    {
      name: 'Yoshi'
    }
  ];

  let filteredData = filterBySearch(testData, 'Yoshi', ['name']);

  expect(filteredData.length).toBe(1);
  expect(filteredData[0].name).toBe('Yoshi');
});

// Filter by search test: lowercase test
test('Search filter Test 2: lowercase Case', () => {
  let testData = [
    {
      name: 'MARIO'
    },
    {
      name: 'LUIGI'
    },
    {
      name: 'PEACH'
    },
    {
      name: 'YOSHI'
    }
  ];

  let filteredData = filterBySearch(testData, 'yoshi', ['name']);

  expect(filteredData.length).toBe(1);
  expect(filteredData[0].name).toBe('YOSHI');
});

// Filter by search test, partial completion
test('Search filter Test 3: partial completion', () => {
  let testData = [
    {
      name: 'Mario'
    },
    {
      name: 'Luigi'
    },
    {
      name: 'Peach'
    },
    {
      name: 'Yoshi'
    },
    {
      name: 'Marsh'
    }
  ];

  let filteredData = filterBySearch(testData, 'mar', ['name']);

  expect(filteredData.length).toBe(2);
  expect(filteredData[0].name).toBe('Mario');
  expect(filteredData[1].name).toBe('Marsh');
});

// Filter by search test, empty spaces
test('Search Filter Test 4: search query with only spaces', () => {
  let testData = [
    {
      name: 'Mario'
    },
    {
      name: 'Luigi'
    },
    {
      name: 'Peach'
    },
    {
      name: 'Yoshi'
    },
    {
      name: 'Marsh'
    }
  ];

  let filteredData = filterBySearch(testData, '            ', ['name']);

  expect(filteredData.length).toBe(testData.length);
  for (let i = 0; i < filteredData.length; i++) {
    expect(filteredData[i].name).toBe(testData[i].name);
  }
});

// Filter by search test: More than 1 attribute
test('Search Filter Test 5: More than 1 Attribute Considered', () => {
  let testData = [
    {
      name: 'Pizza 1',
      topping1: 'pepperoni',
      topping2: 'italian sausage'
    },
    {
      name: 'Pizza 2',
      topping1: 'pineapple',
      topping2: 'olives'
    },
    {
      name: 'Pizza 3',
      topping1: 'bacon',
      topping2: 'pineapple'
    },
    {
      name: 'Pizza 4',
      topping1: 'basil',
      topping2: 'olives'
    },
    {
      name: 'Pizza 5',
      topping1: 'pepperoni',
      topping2: 'jalapeno sausage'
    }
  ];

  let filteredData = filterBySearch(testData, 'pepperoni sausage pizza', ['topping1', 'topping2']);
  expect(filteredData.length).toBe(2);
  expect(filteredData[0].name).toBe('Pizza 1');
  expect(filteredData[1].name).toBe('Pizza 5');
});
