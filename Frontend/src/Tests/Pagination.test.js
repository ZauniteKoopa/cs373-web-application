import { render, screen, fireEvent } from '@testing-library/react';
// import { BrowserRouter } from 'react-router-dom';
// import RestaurantSearchPage from '../Components/ModelSearchPages/RestaurantSearchPage';
// import { restaurantInstances } from '../TestDatabase/TestInstanceDatabase';
import PaginationElement from '../Components/ModelSearchPages/Pagination';

// Test on finding pagination text
test('Pagination Element Test 1: Text 1/3', () => {
  let currentPage = 1;
  let totalPages = 3;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let totalPageText = screen.getByLabelText('page 1');
  expect(totalPageText).toBeInTheDocument();
});

// // Test on finding pagination text
test('Pagination Element Test 2: Text 1/1', () => {
  let currentPage = 1;
  let totalPages = 1;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let totalPageText = screen.getByLabelText('page 1');
  expect(totalPageText).toBeInTheDocument();
});

// // Test on finding pagination text
test('Pagination Element Test 3: Text 2/4', () => {
  let currentPage = 2;
  let totalPages = 4;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let totalPageText = screen.getByLabelText('page 2');
  expect(totalPageText).toBeInTheDocument();
});

// Test on button functionality
test('Pagination Element Button Test 1: Next Page Button', () => {
  let currentPage = 3;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByTestId('NavigateNextIcon');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(1);
  expect(mockSetPage.mock.calls[0][0]).toBe(4);
});

// Test on button functionality
test('Pagination Element Button Test 2: Prev Page Button', () => {
  let currentPage = 3;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByTestId('NavigateBeforeIcon');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(1);
  expect(mockSetPage.mock.calls[0][0]).toBe(2);
});

// Test on button functionality
test('Pagination Element Button Test 3: Last Page Button', () => {
  let currentPage = 3;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByLabelText('Go to page 5');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(1);
  expect(mockSetPage.mock.calls[0][0]).toBe(5);
});

// // Test on button functionality
test('Pagination Element Button Test 4: First Page Button', () => {
  let currentPage = 3;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByLabelText('Go to page 1');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(1);
  expect(mockSetPage.mock.calls[0][0]).toBe(1);
});

// // Test on Disabled button functionality
test('Pagination Element Disabled Button Test 1: Next Page Button', () => {
  let currentPage = 5;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByTestId('NavigateNextIcon');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(0);
});

// // Test on Disabled button functionality
test('Pagination Element Disabled Button Test 2: Prev Page Button', () => {
  let currentPage = 1;
  let totalPages = 5;
  let mockSetPage = jest.fn();

  render(
    <PaginationElement totalPages={totalPages} currentPage={currentPage} setPage={mockSetPage} />
  );

  let nextButton = screen.getByTestId('NavigateBeforeIcon');
  fireEvent.click(nextButton);

  expect(mockSetPage.mock.calls.length).toBe(0);
});
