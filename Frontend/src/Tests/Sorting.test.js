import {
  compareRows,
  getSortButtonIcon,
  handleSortButtonClick
} from '../Components/ModelSearchPages/Sorting';

// Test relation to test sorting with
const testRelation = [
  {
    name: 'Zippy',
    points: 1
  },
  {
    name: 'Aspers',
    points: 3
  },
  {
    name: 'Meow',
    points: 5
  },
  {
    name: 'Meow',
    points: 3
  }
];

// Main tests to check if compareRows work on string attributes, ascending
test('Compare Rows Test 1: String Attribute, Single, Ascending', () => {
  // Test inequality
  let objectA = testRelation[0];
  let objectB = testRelation[1];

  let result = compareRows(objectA, objectB, 'name', true);
  expect(result).toBeGreaterThan(0);

  result = compareRows(objectB, objectA, 'name', true);
  expect(result).toBeLessThan(0);

  // Test equality
  let objectC = testRelation[2];
  let objectD = testRelation[3];

  result = compareRows(objectC, objectD, 'name', true);
  expect(result).toBe(0);

  result = compareRows(objectD, objectC, 'name', true);
  expect(result).toBe(0);
});

test('Compare Rows Test 2: String Attribute, Single, Descending', () => {
  // Test inequality
  let objectA = testRelation[0];
  let objectB = testRelation[1];

  let result = compareRows(objectA, objectB, 'name', false);
  expect(result).toBeLessThan(0);

  result = compareRows(objectB, objectA, 'name', false);
  expect(result).toBeGreaterThan(0);

  // Test equality
  let objectC = testRelation[2];
  let objectD = testRelation[3];

  result = compareRows(objectC, objectD, 'name', false);
  expect(result).toBe(-0);

  result = compareRows(objectD, objectC, 'name', false);
  expect(result).toBe(-0);
});

// Compare Row test that checks if the operation can work on numbers
test('Compare Rows Test 3: Number Attribute, Single, Asending', () => {
  let objectA = testRelation[0];
  let objectB = testRelation[1];
  let objectC = testRelation[3];

  let result = compareRows(objectA, objectB, 'points', true);
  expect(result).toBeLessThan(0);

  result = compareRows(objectB, objectA, 'points', true);
  expect(result).toBeGreaterThan(0);

  result = compareRows(objectB, objectC, 'points', true);
  expect(result).toBe(0);

  result = compareRows(objectC, objectB, 'points', true);
  expect(result).toBe(0);
});

test('Compare Rows Test 4: Number Attributes, Single, Descending', () => {
  let objectA = testRelation[0];
  let objectB = testRelation[1];
  let objectC = testRelation[3];

  let result = compareRows(objectA, objectB, 'points', false);
  expect(result).toBeGreaterThan(0);

  result = compareRows(objectB, objectA, 'points', false);
  expect(result).toBeLessThan(0);

  result = compareRows(objectB, objectC, 'points', false);
  expect(result).toBe(-0);

  result = compareRows(objectC, objectB, 'points', false);
  expect(result).toBe(-0);
});

//
// Main tests to check if sortButtonIcon works
//
test('getSortButtonIcon Test 1: Ascending Case', () => {
  let currentSortSetting = {
    attribute: 'name',
    ascending: true
  };

  let result = getSortButtonIcon(currentSortSetting, 'name');
  expect(result).toBe('\u02C4');
});

test('getSortButtonIcon Test 2: Descending Case', () => {
  let currentSortSetting = {
    attribute: 'name',
    ascending: false
  };

  let result = getSortButtonIcon(currentSortSetting, 'name');
  expect(result).toBe('\u02C5');
});

test('getSortButtonIcon Test 3: Neutral Case', () => {
  let currentSortSetting = {
    attribute: 'culture',
    ascending: true
  };

  let result = getSortButtonIcon(currentSortSetting, 'name');
  expect(result).toBe('\u2012');
});

//
// Main Test to Check if handleSortButtonClick works
//
test('handleSortButtonClick Test 1: Change Sorting Attribute', () => {
  let currentSort = {
    attribute: 'name',
    ascending: true
  };

  let newSortSetting = handleSortButtonClick(currentSort, 'culture');
  expect(newSortSetting).toStrictEqual({
    attribute: 'culture',
    ascending: true
  });
});

test('handleSortButtonClick Test 2: Change Sorting to Descending', () => {
  let currentSort = {
    attribute: 'name',
    ascending: true
  };

  let newSortSetting = handleSortButtonClick(currentSort, 'name');
  expect(newSortSetting).toStrictEqual({
    attribute: 'name',
    ascending: false
  });
});

test('handleSortButtonClick Test 2: Change Sorting back to Ascending', () => {
  let currentSort = {
    attribute: 'name',
    ascending: false
  };

  let newSortSetting = handleSortButtonClick(currentSort, 'name');
  expect(newSortSetting).toStrictEqual({
    attribute: 'name',
    ascending: true
  });
});
