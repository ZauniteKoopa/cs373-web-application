import vietnamMap from '../assets/img/vietnam.png';
import franceMap from '../assets/img/france.png';
import usaMap from '../assets/img/usa.png';

import vietnamFlag from '../assets/img/vietnamFlag.png';
import franceFlag from '../assets/img/franceFlag.png';
import usaFlag from '../assets/img/usFlag.jpg';

import phoImg from '../assets/img/Pho.jpg';
import crepesImg from '../assets/img/Crepes.jpg';
import steakImg from '../assets/img/steak.png';

import phoMap from '../assets/img/phoMap.png';
import laMadMap from '../assets/img/laMadMap.png';
import steakMap from '../assets/img/steakMap.png';

import phoPhongLu from '../assets/img/PhoPhongLu.jpg';
import laMadeleine from '../assets/img/laMadeleine.png';
import longHornSteakHouse from '../assets/img/longhornsteakhouse.jpg';

import keystone from '../assets/img/KeystoneAllNaturalGroundBeef.jpg';
import sweetGinger from '../assets/img/GreatValueOrganicSweetGingerShaker.jpg';
import cheese from '../assets/img/ElMexicanoRequesonRicottaStyleMexicanCheese.jpg';

// Test recipe instances
export const recipeInstances = [
  {
    name: 'Pho',
    id: 'vietRecipe2',
    culture: { name: 'Vietnamese', id: 'asia123' },
    ingredient: [
      'Steak',
      'Rice noodles',
      'Beef stock',
      'Spices',
      'Sweetner',
      'Sea salt',
      'Fresh herbs',
      'Bean sprouts',
      'Lime wedges',
      'Chiles (optional)',
      'Onions (optional)',
      'Sauces (optional)'
    ],
    instructions: [
      'Prep the protein. In order to slice the beef, chicken or pork as thinly as possible, I recommend popping the meat in the freezer for at least 30-45 minutes to chill. Then use a sharp knife to thinly slice the meat before adding it to the soup.',
      'Make the broth. Meanwhile, as the meat is chilling, char the onion and ginger (either in a skillet or under the broiler). Meanwhile, briefly toast the spices in a large stockpot. Then add in the onion, ginger, and broth. Cover and let everything simmer together for at least 30 minutes so that all of those flavors can meld. Strain out and discard all of the onions, ginger and spices. Then stir in the remaining ingredients and season with salt.',
      'Prep the noodles. Meanwhile, as your broth is simmering, go ahead and cook the noodles al dente according to the package instructions. Drain in a strainer, then toss briefly with cold water to prevent the noodles from continuing to cook, and set aside. (I also recommend tossing the noodles with a drizzle of oil — such as sesame oil — to prevent them from sticking.)',
      'Assemble. Once everything is ready to go, add a handful of noodles to each individual serving bowl, topped with your desired protein. Then ladle the simmering hot broth into the serving bowls, being sure to submerge the meat so that it will get cooked. Top with lots and lots of garnishes.',
      'Serve. And serve warm, encouraging everyone to stir the garnishes into the soup so that they can flavor the broth, also adding in additional extra sauces if desired.'
    ],
    img: phoImg,
    time: 210,
    rating: 4,
    difficulty: 3,
    category: 'Soup'
  },
  {
    name: 'Crepes',
    id: 'frenchRecipe3',
    culture: { name: 'French', id: 'euro123' },
    ingredient: [
      'All-purpose flour',
      'Eggs',
      'Milk',
      'Salt',
      'Unsalted Butter',
      'Granulated Sugar',
      'Vanilla Extract'
    ],
    instructions: [
      'Melt some butter in the microwave or on the stove. Let it cool for a few minutes before using in the batter. (Otherwise you could scramble the eggs.)',
      "Add the cooled melted butter and all the remaining ingredients into a blender. A blender works WONDERFULLY to smooth out the batter because it cuts that flour perfectly into all the wet ingredients. If you don't have a blender, just use a mixing bowl and whisk.",
      "Chill the crepe batter for at least 30-60 minutes before cooking it. This time in the refrigerator is crucial to the taste, texture, and success of your crepes. Use this time to clean up and get your skillet ready. You can even chill the batter overnight so it's ready to cook the next day.",
      'Generously butter the pan and keep butter nearby for greasing the pan between each crepe too. Though professional chefs may use a specialty crepe pan.',
      "The longest part of this recipe is standing over the stove and cooking them one at a time over medium heat. Use only 3-4 Tablespoons of batter per crepe. (I usually use 3 Tablespoons.) The less you use and the larger you stretch the crepe, the thinner they'll be. As you can see in my video tutorial above, I twirl the pan so the batter stretches as far as it can go. If you don't do this, your crepes will be pretty thick and taste like tortillas. Still delicious, but very different. Flip the crepe over and cook the other side, too.",
      'I love serving them warm with cold whipped cream and fresh berries. Keep scrolling because I have lots of filling ideas listed for you below.'
    ],
    img: crepesImg,
    time: 75,
    rating: 5,
    difficulty: 4,
    category: 'Bread'
  },
  {
    name: 'American Steak',
    id: 'americanRecipe',
    culture: { name: 'American', id: 'na123' },
    ingredient: [
      'Olive oil',
      'Minced garlic',
      'Filet mignon',
      'Dried rosemary',
      'Dried thyme',
      'Dried marjoram',
      'Salt',
      'Ground black pepper'
    ],
    instructions: [
      'In a covered microwave-safe bowl, heat the olive oil and garlic in the microwave for 50 to 60 seconds. Remove and allow to cool.',
      'Add the rosemary, thyme, and marjoram, and stir. Let sit for 5 minutes.',
      'Place the filet mignon into a shallow glass dish',
      'Pour the herb mixture over the steaks, turning them over to coat evenly. Cover and let marinate for 2 to 4 hours in the refrigerator.',
      'Preheat the grill for high heat. Right before placing steaks on the grill, oil the grate with a long pair of tongs, folded paper towels, and oil. Make 3 to 4 passes on the grate.',
      'Remove the steaks from the fridge, and season both sides with salt and pepper.',
      'Place the steaks on the grill and cook for 5 to 6 minutes per side, or to the desired doneness.',
      'Remove steaks from the heat and let rest for at least 5 minutes.'
    ],
    img: steakImg,
    time: 145,
    rating: 3,
    difficulty: 2,
    category: 'Beef'
  }
];

// Recipe dictionary to be used by the instance pages
export const recipeDictionary = {
  vietRecipe2: {
    name: 'Pho',
    culture: { name: 'Vietnamese', id: 'asia123' },
    description:
      "Phở or pho is a Vietnamese soup dish consisting of broth, rice noodles, herbs, and meat. Pho is a popular food in Vietnam where it is served in households, street stalls and restaurants countrywide. Pho is considered Vietnam's national dish.",
    source: 'https://theforkedspoon.com/pho-recipe/',
    ingredient: [
      { name: 'Steak', id: 'beef123' },
      { name: 'Rice noodles', id: null },
      { name: 'Beef stock', id: null },
      { name: 'Spices', id: null },
      { name: 'Sweetner', id: 'gingy123' },
      { name: 'Sea salt', id: null },
      { name: 'Fresh Herbs', id: null },
      { name: 'Bean Sprouts', id: null },
      { name: 'Lime Wedges', id: null },
      { name: 'Chiles (optional)', id: null },
      { name: 'Onions (optional)', id: null },
      { name: 'Sauces (optional)', id: null }
    ],
    instructions: [
      'Prep the protein. In order to slice the beef, chicken or pork as thinly as possible, I recommend popping the meat in the freezer for at least 30-45 minutes to chill. Then use a sharp knife to thinly slice the meat before adding it to the soup.',
      'Make the broth. Meanwhile, as the meat is chilling, char the onion and ginger (either in a skillet or under the broiler). Meanwhile, briefly toast the spices in a large stockpot. Then add in the onion, ginger, and broth. Cover and let everything simmer together for at least 30 minutes so that all of those flavors can meld. Strain out and discard all of the onions, ginger and spices. Then stir in the remaining ingredients and season with salt.',
      'Prep the noodles. Meanwhile, as your broth is simmering, go ahead and cook the noodles al dente according to the package instructions. Drain in a strainer, then toss briefly with cold water to prevent the noodles from continuing to cook, and set aside. (I also recommend tossing the noodles with a drizzle of oil — such as sesame oil — to prevent them from sticking.)',
      'Assemble. Once everything is ready to go, add a handful of noodles to each individual serving bowl, topped with your desired protein. Then ladle the simmering hot broth into the serving bowls, being sure to submerge the meat so that it will get cooked. Top with lots and lots of garnishes.',
      'Serve. And serve warm, encouraging everyone to stir the garnishes into the soup so that they can flavor the broth, also adding in additional extra sauces if desired.'
    ],
    img: phoImg,
    time: 210,
    rating: 4,
    difficulty: 3,
    category: 'Soup'
  },

  frenchRecipe3: {
    name: 'Crepes',
    culture: { name: 'French', id: 'euro123' },
    description:
      'A crêpe or crepe is a very thin type of pancake. Crêpes originated in Brittany, a region in the west of France, during the 13th century, and are now consumed around the world. Crêpes are usually one of two varieties: sweet crêpes or savoury galettes.',
    source: 'https://www.allrecipes.com/recipe/16383/basic-crepes/',
    ingredient: [
      { name: 'All-purpose flour', id: null },
      { name: 'Eggs', id: null },
      { name: 'Milk', id: 'mexcheese123' },
      { name: 'Salt', id: null },
      { name: 'Unsalted Butter', id: null },
      { name: 'Granulated Sugar', id: null },
      { name: 'Vanilla Extract', id: null }
    ],
    instructions: [
      'Melt some butter in the microwave or on the stove. Let it cool for a few minutes before using in the batter. (Otherwise you could scramble the eggs.)',
      "Add the cooled melted butter and all the remaining ingredients into a blender. A blender works WONDERFULLY to smooth out the batter because it cuts that flour perfectly into all the wet ingredients. If you don't have a blender, just use a mixing bowl and whisk.",
      "Chill the crepe batter for at least 30-60 minutes before cooking it. This time in the refrigerator is crucial to the taste, texture, and success of your crepes. Use this time to clean up and get your skillet ready. You can even chill the batter overnight so it's ready to cook the next day.",
      'Generously butter the pan and keep butter nearby for greasing the pan between each crepe too. Though professional chefs may use a specialty crepe pan.',
      "The longest part of this recipe is standing over the stove and cooking them one at a time over medium heat. Use only 3-4 Tablespoons of batter per crepe. (I usually use 3 Tablespoons.) The less you use and the larger you stretch the crepe, the thinner they'll be. As you can see in my video tutorial above, I twirl the pan so the batter stretches as far as it can go. If you don't do this, your crepes will be pretty thick and taste like tortillas. Still delicious, but very different. Flip the crepe over and cook the other side, too.",
      'I love serving them warm with cold whipped cream and fresh berries. Keep scrolling because I have lots of filling ideas listed for you below.'
    ],
    img: crepesImg,
    time: 75,
    rating: 5,
    difficulty: 4,
    category: 'Bread'
  },

  americanRecipe: {
    name: 'American Steak',
    culture: { name: 'American', id: 'na123' },
    description:
      'A steak is a meat generally sliced across the muscle fibers, potentially including a bone. It is normally grilled, though it can also be pan-fried. Steak can also be cooked in sauce, such as in steak and kidney pie, or minced and formed into patties, such as hamburgers.',
    source: 'https://www.food.com/recipe/american-sirloin-steaks-291758',
    ingredient: [
      { name: 'Olive oil', id: null },
      { name: 'Minced garlic', id: null },
      { name: 'Filet mignon', id: 'beef123' },
      { name: 'Dried rosemary', id: null },
      { name: 'Dried thyme', id: null },
      { name: 'Dried marjoram', id: null },
      { name: 'Salt', id: null },
      { name: 'Ground black pepper', id: null }
    ],
    instructions: [
      'In a covered microwave-safe bowl, heat the olive oil and garlic in the microwave for 50 to 60 seconds. Remove and allow to cool.',
      'Add the rosemary, thyme, and marjoram, and stir. Let sit for 5 minutes.',
      'Place the filet mignon into a shallow glass dish',
      'Pour the herb mixture over the steaks, turning them over to coat evenly. Cover and let marinate for 2 to 4 hours in the refrigerator.',
      'Preheat the grill for high heat. Right before placing steaks on the grill, oil the grate with a long pair of tongs, folded paper towels, and oil. Make 3 to 4 passes on the grate.',
      'Remove the steaks from the fridge, and season both sides with salt and pepper.',
      'Place the steaks on the grill and cook for 5 to 6 minutes per side, or to the desired doneness.',
      'Remove steaks from the heat and let rest for at least 5 minutes.'
    ],
    img: steakImg,
    time: 145,
    rating: 3,
    difficulty: 2,
    category: 'Beef'
  },

  actualTest: {
    cookTime: 50,
    cuisine: {
      id: 13,
      name: 'Greece'
    },
    description:
      'Greek Chicken Kebabs requires approximately <b>50 minutes</b> from start to finish. One portion of this dish contains about <b>9g of protein</b>, <b>7g of fat</b>, and a total of <b>123 calories</b>. This recipe serves 9 and costs 85 cents per serving. It works well as a cheap hor d\'oeuvre. It is brought to you by Cooking Classy. 16094 people have made this recipe and would make it again. If you have onion, oregano, salt and pepper, and a few other ingredients on hand, you can make it. Several people really liked this Mediterranean dish. It is a good option if you\'re following a <b>gluten free, dairy free, paleolithic, and primal</b> diet. Taking all factors into account, this recipe <b>earns a spoonacular score of 0%</b>, which is improvable. <a href="https://spoonacular.com/recipes/greek-chicken-kebabs-1101858">Greek Chicken Kebabs</a>, <a href="https://spoonacular.com/recipes/greek-chicken-kebabs-764281">Greek Chicken Kebabs</a>, and <a href="https://spoonacular.com/recipes/greek-chicken-kebabs-1013342">Greek Chicken Kebabs</a> are very similar to this recipe.',
    dishType: 'appetizer',
    id: 45,
    image: 'https://spoonacular.com/recipeImages/893017-556x370.jpg',
    ingredients: [
      {
        id: 34,
        name: 'chicken breast'
      },
      {
        id: 93,
        name: 'olive oil'
      },
      {
        id: 89,
        name: 'lemon juice'
      },
      {
        id: null,
        name: 'red wine vinegar'
      },
      {
        id: 57,
        name: 'garlic'
      },
      {
        id: null,
        name: 'oregano'
      },
      {
        id: 74,
        name: 'dried basil'
      },
      {
        id: null,
        name: 'dried thyme'
      },
      {
        id: null,
        name: 'ground coriander'
      },
      {
        id: 16,
        name: 'black pepper'
      },
      {
        id: null,
        name: 'red pepper'
      },
      {
        id: 69,
        name: 'zucchini'
      },
      {
        id: null,
        name: 'red onion'
      }
    ],
    instructions:
      'Instructions\n\nFor the kebabs:\n\nIn a bowl whisk together 1/4 cup olive oil, lemon juice, vinegar, garlic, 2 tsp oregano, 1 tsp basil, the thyme, coriander and season with salt and pepper to taste (I used 3/4 tsp salt 1/4 tsp pepper).\u00a0\n\nPlace chicken in a gallon size resealable bag, pour olive oil mixture over chicken and press chicken into marinade. Seal bag and refrigerate 45 minutes to 2 hours (no longer than 2 hours or the chicken will start to get mealy because of the acidic ingredients). If using wooden skewers soak them in water for 30 minutes.\n\nDrizzle and toss veggies with 2 Tbsp of the olive oil (I just left them on the cutting board and tossed them) and season with remaining 1/2 tsp oregano, 1/2 tsp basil and salt to taste.\n\nPreheat a grill over medium-high heat. Thread a red bell pepper, red onion, zucchini, 2 chicken pieces on to skewer and repeat twice.\n\nBrush grill lightly with olive oil, place skewers on grill and grill until chicken registers 165\u00b0F (73\u00b0C) in center, about 8 - 12 minutes, rotating once halfway through cooking. Garnish with parsley, serve warm with tzatziki sauce.',
    name: 'Greek Chicken Kebabs',
    rating: 4,
    source: 'https://www.cookingclassy.com/greek-chicken-kebabs-with-tzatziki-sauce/'
  }
};

// Test restaurant instance
export const restaurantInstances = [
  {
    name: 'Pho Phong Lu',
    id: 'pho123',
    location: '9200 N Lamar Blvd #104, Austin, TX 78753',
    photo: phoPhongLu,
    url: 'https://phophongluu.com/menu',
    description:
      'Unpretentious Vietnamese choice specializing in noodle soup, plus fried rice & spring rolls.',
    culture: 'Vietnamese',
    cost: 1,
    rating: 5,
    city: 'Austin, Texas',
    map: phoMap,
    lat: 30.395706788388132,
    lng: -97.74716542442351
  },
  {
    name: 'la Madeleine',
    id: 'french123',
    location: '9828 Great Hills Trail Suite 650 Ste 650, Austin, TX 78759',
    photo: laMadeleine,
    url: 'https://lamadeleine.com/locations/austin-arboretum',
    description:
      'Quaint French cafe chain serving rustic country fare, espresso & fresh-baked baguettes..',
    culture: 'French',
    cost: 3,
    rating: 4,
    city: 'Austin, Texas',
    map: laMadMap,
    lat: 30.395706788388132,
    lng: -97.74716542442351
  },
  {
    name: 'LongHorn Steakhouse',
    id: 'american123',
    location: '2702 Parker Dr Suite C, Round Rock, TX 78681',
    photo: longHornSteakHouse,
    url: 'https://www.longhornsteakhouse.com/home',
    description:
      'Casual steakhouse chain known for grilled beef & other American dishes in a ranch-style space.',
    culture: 'American',
    cost: 0,
    rating: 4,
    city: 'Round Rock, Texas',
    map: steakMap,
    lat: 30.395706788388132,
    lng: -97.74716542442351
  }
];

// Restaurant dictionary used for instance pages
export const restaurantDictionary = {
  pho123: {
    name: 'Pho Phong Lu',
    location: '9200 N Lamar Blvd #104, Austin, TX 78753',
    photo: phoPhongLu,
    url: 'https://phophongluu.com/menu',
    description:
      'Unpretentious Vietnamese choice specializing in noodle soup, plus fried rice & spring rolls.',
    phoneNumber: '(777)777-7777',
    culture: { name: 'Vietnamese', id: 'asia123' },
    similarRecipes: [{ name: 'Pho', id: 'vietRecipe2' }],
    cost: 1,
    rating: 5,
    city: 'Austin, Texas',
    map: phoMap,
    lat: 30.36126242254142,
    lng: -97.6986529130707,
    deliverable: false
  },
  french123: {
    name: 'la Madeleine',
    id: 'french123',
    location: '9828 Great Hills Trail Suite 650 Ste 650, Austin, TX 78759',
    photo: laMadeleine,
    url: 'https://lamadeleine.com/locations/austin-arboretum',
    description:
      'Quaint French cafe chain serving rustic country fare, espresso & fresh-baked baguettes..',
    phoneNumber: '(777)777-7777',
    culture: { name: 'French', id: 'euro123' },
    similarRecipes: [{ name: 'Crepes', id: 'frenchRecipe3' }],
    cost: 3,
    rating: 4,
    city: 'Austin, Texas',
    map: laMadMap,
    lat: 30.395706788388132,
    lng: -97.74716542442351,
    deliverable: true
  },
  american123: {
    name: 'LongHorn Steakhouse',
    id: 'american123',
    location: '2702 Parker Dr Suite C, Round Rock, TX 78681',
    photo: longHornSteakHouse,
    url: 'https://www.longhornsteakhouse.com/home',
    description:
      'Casual steakhouse chain known for grilled beef & other American dishes in a ranch-style space.',
    phoneNumber: '(777)777-7777',
    culture: { name: 'American', id: 'na123' },
    similarRecipes: [{ name: 'American Steak', id: 'americanRecipe' }],
    cost: 2,
    rating: 4,
    city: 'Round Rock, Texas',
    map: steakMap,
    lat: 30.480915213714002,
    lng: -97.68101511494893,
    deliverable: true
  },
  actualTest: {
    address: '2409 E 7th StAustin, TX 78702',
    city: 'Austin',
    cost: 2,
    culture: {
      id: 5,
      name: 'Colombia'
    },
    deliverable: true,
    description: ['1100-2100', '1100-2100', '1100-2100', '1100-2200', '1100-2200', '1100-2000', ''],
    id: 8,
    image: 'https://s3-media1.fl.yelpcdn.com/bphoto/TbR_4yu4YsKaz9JGnnS2dQ/o.jpg',
    mapLatitude: 30.2608,
    mapLongitude: -97.71482,
    name: 'Casa Colombia',
    phone: '+15124959425',
    rating: 4.0,
    review1:
      'Authentic food, friendly staff, small parking lot. \n\nCasa Columbia is an amazing restaurant owned by a wonderful family. Their location on east 7th street...',
    review2:
      "Authentic and delicious food! I can't say enough about this place. We must have texted about four different families about the food here while we were...",
    review3:
      'Had a great experience at Casa Colombia. Went on a Thursday night and was fairly busy. We sat in the patio and they were blasting Spanish music (first...',
    website:
      'https://www.yelp.com/biz/casa-colombia-austin-2?adjust_creative=CLvH_oejmW4isGEg-hCd2w&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=CLvH_oejmW4isGEg-hCd2w'
  }
};

// Test culture instances
export const cultureInstances = [
  {
    name: 'Vietnamese',
    id: 'asia123',
    country: 'Vietnam',
    subregion: 'Asia',
    language: 'Vietnamese',
    location: vietnamMap,
    flag: vietnamFlag,
    description:
      "Vietnamese cuisine encompasses the foods and beverages of Vietnam, and features a combination of five fundamental tastes (Vietnamese: ngũ vị) in overall meals. Each Vietnamese dish has a distinctive flavor which reflects one or more of these elements. Vietnamese recipes use lemongrass, ginger, mint, Vietnamese mint, long coriander, Saigon cinnamon, bird's eye chili, lime, and Thai basil leaves. Traditional Vietnamese cooking has often been characterised with using fresh ingredients, not much use of dairy nor oil, interesting textures, and the use of herbs and vegetables. A leading soy sauce manufacturer's research confirms that fish sauce (nước mắm) is the predominant table sauce in Vietnamese homes, where it captures over 70% of the market, while the market share for soy sauce is under 20%. It is also low in sugar and is almost always naturally gluten-free, as many of the dishes are made with rice noodles, rice papers and rice flour instead of wheat.",
    resources: ['Rice', 'Sugarcane', 'Cassave', 'Corn', 'Sweet Potato', 'Nuts'],
    restaurants: [{ name: 'Pho Phong Lu', id: 'pho123' }],
    recipes: [{ name: 'Pho', id: 'vietRecipe2' }],
    sampleRsetaurant: '',
    capital: 'Hanoi',
    currency: 'Dong'
  },
  {
    name: 'French',
    id: 'euro123',
    country: 'France',
    subregion: 'Europe',
    language: 'French',
    location: franceMap,
    flag: franceFlag,
    description:
      'French cuisine (French: Cuisine française) consists of the cooking traditions and practices from France. French cuisine developed throughout the centuries influenced by the many surrounding cultures of Spain, Italy, Switzerland, Germany and Belgium, in addition to its own food traditions on the long western coastlines of the Atlantic, the Channel and inland. Knowledge of French cooking has contributed significantly to Western cuisines. Its criteria are used widely in Western cookery school boards and culinary education.',
    resources: ['Wheat', 'Barley', 'Corn', 'Potatoes', 'Beef', 'Pork', 'Milk', 'Cheese'],
    restaurants: [{ name: 'la Madeleine', id: 'french123' }],
    recipes: [{ name: 'Crepes', id: 'frenchRecipe3' }],
    sampleRsetaurant: '',
    capital: 'Paris',
    currency: 'Euro'
  },
  {
    name: 'American',
    id: 'na123',
    country: 'The United States of America',
    subregion: 'North America',
    language: 'English',
    location: usaMap,
    flag: usaFlag,
    description:
      'American cuisine is the cooking style and traditional food dishes prepared in the United States. Primarily European in origin, it has been significantly influenced by indigenous Native Americans, African Americans, Asians, Pacific Islanders, and many other cultures and traditions, reflecting the diverse history of the United States. Though some of American cuisine is fusion cuisine reflecting global cuisine, such as Mexican-American, Greek-American cuisine, and American Chinese cuisines, many regional cuisines have their own deeply rooted ethnic heritages, including Cajun, Louisiana Creole, Native American, New England Algonquian, New Mexican, Pennsylvania Dutch, Soul food, Texan/Tex-Mex, Southern, and Tlingit. American cuisine is very diverse and has various styles of cuisine.',
    resources: [
      'Corn',
      'Soybeans',
      'Barley',
      'Wheat',
      'Oats',
      'Milk',
      'Potatoes',
      'Pork',
      'Beef',
      'Rice'
    ],
    restaurants: [{ name: 'LongHorn Steakhouse', id: 'american123' }],
    recipes: [{ name: 'American Steak', id: 'americanRecipe' }],
    sampleRsetaurant: '',
    capital: 'Washington D.C',
    currency: 'Dollar'
  }
];

// Culture dictionary to be used on instance pages
export const cultureDictionary = {
  asia123: {
    name: 'Vietnamese',
    id: 'asia123',
    country: 'Vietnam',
    subregion: 'Asia',
    language: 'Vietnamese',
    location: vietnamMap,
    flag: vietnamFlag,
    description:
      "Vietnamese cuisine encompasses the foods and beverages of Vietnam, and features a combination of five fundamental tastes (Vietnamese: ngũ vị) in overall meals. Each Vietnamese dish has a distinctive flavor which reflects one or more of these elements. Vietnamese recipes use lemongrass, ginger, mint, Vietnamese mint, long coriander, Saigon cinnamon, bird's eye chili, lime, and Thai basil leaves. Traditional Vietnamese cooking has often been characterised with using fresh ingredients, not much use of dairy nor oil, interesting textures, and the use of herbs and vegetables. A leading soy sauce manufacturer's research confirms that fish sauce (nước mắm) is the predominant table sauce in Vietnamese homes, where it captures over 70% of the market, while the market share for soy sauce is under 20%. It is also low in sugar and is almost always naturally gluten-free, as many of the dishes are made with rice noodles, rice papers and rice flour instead of wheat.",
    resources: ['Rice', 'Sugarcane', 'Cassave', 'Corn', 'Sweet Potato', 'Nuts'],
    restaurants: [{ name: 'Pho Phong Lu', id: 'pho123' }],
    recipes: [{ name: 'Pho', id: 'vietRecipe2' }],
    capital: 'Hanoi',
    currency: 'Dong',
    wiki: 'https://en.wikipedia.org/wiki/Vietnam',
    lat: 21.03420876981607,
    lng: 105.82264222685882
  },
  euro123: {
    name: 'French',
    id: 'euro123',
    country: 'France',
    subregion: 'Europe',
    language: 'French',
    location: franceMap,
    flag: franceFlag,
    description:
      'French cuisine (French: Cuisine française) consists of the cooking traditions and practices from France. French cuisine developed throughout the centuries influenced by the many surrounding cultures of Spain, Italy, Switzerland, Germany and Belgium, in addition to its own food traditions on the long western coastlines of the Atlantic, the Channel and inland. Knowledge of French cooking has contributed significantly to Western cuisines. Its criteria are used widely in Western cookery school boards and culinary education.',
    resources: ['Wheat', 'Barley', 'Corn', 'Potatoes', 'Beef', 'Pork', 'Milk', 'Cheese'],
    restaurants: [{ name: 'la Madeleine', id: 'french123' }],
    recipes: [{ name: 'Crepes', id: 'frenchRecipe3' }],
    capital: 'Paris',
    currency: 'Euro',
    wiki: 'https://en.wikipedia.org/wiki/France',
    lat: 46.71413646179622,
    lng: 2.480030491774965
  },
  na123: {
    name: 'American',
    id: 'na123',
    country: 'The United States of America',
    subregion: 'North America',
    language: 'English',
    location: usaMap,
    flag: usaFlag,
    description:
      'American cuisine is the cooking style and traditional food dishes prepared in the United States. Primarily European in origin, it has been significantly influenced by indigenous Native Americans, African Americans, Asians, Pacific Islanders, and many other cultures and traditions, reflecting the diverse history of the United States. Though some of American cuisine is fusion cuisine reflecting global cuisine, such as Mexican-American, Greek-American cuisine, and American Chinese cuisines, many regional cuisines have their own deeply rooted ethnic heritages, including Cajun, Louisiana Creole, Native American, New England Algonquian, New Mexican, Pennsylvania Dutch, Soul food, Texan/Tex-Mex, Southern, and Tlingit. American cuisine is very diverse and has various styles of cuisine.',
    resources: [
      'Corn',
      'Soybeans',
      'Barley',
      'Wheat',
      'Oats',
      'Milk',
      'Potatoes',
      'Pork',
      'Beef',
      'Rice'
    ],
    restaurants: [{ name: 'LongHorn Steakhouse', id: 'american123' }],
    recipes: [{ name: 'American Steak', id: 'americanRecipe' }],
    capital: 'Washington D.C',
    currency: 'Dollar',
    wiki: 'https://en.wikipedia.org/wiki/United_States',
    lat: 39.90271001905775,
    lng: -100.56742077362233
  },
  actualTest: {
    cusine: 'Latin American',
    currency: 'Argentine peso',
    description:
      "Besides many of the pasta, sausage and dessert dishes common to continental Europe, Argentines enjoy a wide variety of Indigenous and Criollo creations, including empanadas (a small stuffed pastry), locro (a mixture of corn, beans, meat, bacon, onion, and gourd), humita and mate.The country has the highest consumption of red meat in the world, traditionally prepared as asado, the Argentine barbecue. It is made with various types of meats, often including chorizo, sweetbread, chitterlings, and blood sausage.Common desserts include facturas (Viennese-style pastry), cakes and pancakes filled with dulce de leche (a sort of milk caramel jam), alfajores (shortbread cookies sandwiched together with chocolate, dulce de leche or a fruit paste), and tortas fritas (fried cakes)Argentine wine, one of the world's finest, is an integral part of the local menu. Malbec, Torront\u00e9s, Cabernet Sauvignon, Syrah and Chardonnay are some of the most sought-after varieties.",
    id: 1,
    image: 'https://commons.wikimedia.org/wiki/Special:FilePath/Flag_of_Argentina.svg',
    language: 'Spanish language',
    mapLatitude: 38.4161,
    mapLongitude: 63.6167,
    name: 'Argentina',
    num_recipe_connections: 3,
    num_restaurant_connections: 5,
    produce: ['beef', 'soybeans', 'wheat', 'maize', "cow's milk", 'apples', 'chicken'],
    recipe_connections: [
      {
        id: '30',
        name: 'Chimichurri Skirt Steak with Grilled Asparagus'
      },
      {
        id: '64',
        name: 'Pan Seared Lamb Loin With Chimichurri & Roasted Trio Squash Salad With Goat Cheese and Pinenuts'
      },
      {
        id: '77',
        name: 'Shrimp Ceviche'
      }
    ],
    restaurant_connections: [
      {
        id: '7',
        name: 'Buenos Aires Caf\u00e9 - Este'
      },
      {
        id: '44',
        name: 'Corrientes 348 Argentinian Steakhouse'
      },
      {
        id: '92',
        name: 'Morfi Argentino'
      },
      {
        id: '96',
        name: 'Patagonia Grill & Cafe'
      },
      {
        id: '121',
        name: 'La Milanesa Restaurant & Grill'
      }
    ],
    subRegion: 'South America',
    url: 'https://en.wikipedia.org/wiki/Argentina'
  }
};

// Test ingredient instances
export const ingredientInstances = [
  {
    name: 'Keystone All Natural Ground Beef, 28 Oz',
    id: 'beef123',
    img: keystone,
    description:
      'Keystone Ground Beef is a handy substitute versus cooking ground beef from raw when you are short on time. It has no artificial ingredients or water for high-quality flavor. The all-natural ground beef contains fully-cooked meat and sea salt. It is suitable for making meals such as chili, spaghetti, sloppy joes, tacos and casseroles. Keystone Ground Beef provides a good source of protein and iron and 70 calories per serving. It comes in a convenient 28-oz can.',
    badge: '/badges/Beef.png',
    tooltip: 'Contains beef',
    price: 21.72,
    calories: 70.0,
    fat: 3.5,
    fiber: 2.5,
    sugar: 1.2,
    calcium: 4.0,
    sodium: 7.3,
    protein: 12.0
  },
  {
    name: 'Great Value Organic Sweet Ginger Shaker, 2.8 oz',
    id: 'gingy123',
    img: sweetGinger,
    description:
      'Make mouth-watering meals with our delicious Great Value Organic Sweet Ginger Shaker. Our organic sweet ginger is made with only wholesome ingredients with no artificial flavors or preservatives to deliver great food you can trust. Generously shake our sweet ginger over chicken and vegetables for a delicious stir fry, use it as the secret ingredient in spiced baked goods, or simply sprinkle it over your favorite oatmeal, cereal, or yogurt. No spice rack should be without a Great Value Organic Sweet Ginger Shaker.',
    badge: '/badges/Vegan.png',
    tooltip: 'Vegan',
    price: 3.97,
    calories: 280.0,
    fat: 2.0,
    fiber: 1.3,
    sugar: 5.0,
    calcium: 2.1,
    sodium: 2.7,
    protein: 0.5
  },
  {
    name: 'El Mexicano Requeson Ricotta Style Mexican Cheese, 14 oz',
    id: 'mexcheese123',
    img: cheese,
    description:
      'A true delicacy specialty cheese, similar to ricotta cheese. This quality cheese pairs wonderfully with fresh fruit such as berries and/or stone fruit. It can also be used as a filling base for crepes or spicy Mexican lasagna! Always refer to the actual package for the most accurate information',
    badge: '/badges/Milk.png',
    tooltip: 'Contains milk',
    price: 4.77,
    calories: 120.0,
    fat: 8.0,
    fiber: 1.3,
    sugar: 2.0,
    calcium: 5.0,
    sodium: 8.0,
    protein: 6.0
  }
];

// Test ingredient dictionary
export const ingredientDictionary = {
  beef123: {
    name: 'Keystone All Natural Ground Beef, 28 Oz',
    id: 'beef123',
    img: keystone,
    description:
      'Keystone Ground Beef is a handy substitute versus cooking ground beef from raw when you are short on time. It has no artificial ingredients or water for high-quality flavor. The all-natural ground beef contains fully-cooked meat and sea salt. It is suitable for making meals such as chili, spaghetti, sloppy joes, tacos and casseroles. Keystone Ground Beef provides a good source of protein and iron and 70 calories per serving. It comes in a convenient 28-oz can.',
    badge: '/badges/Beef.png',
    tooltip: 'Contains beef',
    price: 21.72,
    calories: 70.0,
    fat: 3.5,
    fiber: 2.5,
    sugar: 1.2,
    calcium: 4.0,
    sodium: 7.3,
    protein: 12.0
  },
  gingy123: {
    name: 'Great Value Organic Sweet Ginger Shaker, 2.8 oz',
    id: 'gingy123',
    img: sweetGinger,
    description:
      'Make mouth-watering meals with our delicious Great Value Organic Sweet Ginger Shaker. Our organic sweet ginger is made with only wholesome ingredients with no artificial flavors or preservatives to deliver great food you can trust. Generously shake our sweet ginger over chicken and vegetables for a delicious stir fry, use it as the secret ingredient in spiced baked goods, or simply sprinkle it over your favorite oatmeal, cereal, or yogurt. No spice rack should be without a Great Value Organic Sweet Ginger Shaker.',
    badge: '/badges/Vegan.png',
    tooltip: 'Vegan',
    price: 3.97,
    calories: 280.0,
    fat: 2.0,
    fiber: 1.3,
    sugar: 5.0,
    calcium: 2.1,
    sodium: 2.7,
    protein: 0.5
  },
  mexcheese123: {
    name: 'El Mexicano Requeson Ricotta Style Mexican Cheese, 14 oz',
    id: 'mexcheese123',
    img: cheese,
    description:
      'A true delicacy specialty cheese, similar to ricotta cheese. This quality cheese pairs wonderfully with fresh fruit such as berries and/or stone fruit. It can also be used as a filling base for crepes or spicy Mexican lasagna! Always refer to the actual package for the most accurate information',
    badge: '/badges/Milk.png',
    tooltip: 'Contains milk',
    price: 4.77,
    calories: 120.0,
    fat: 8.0,
    fiber: 1.3,
    sugar: 2.0,
    calcium: 5.0,
    sodium: 8.0,
    protein: 6.0
  }
};
